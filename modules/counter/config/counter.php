<?php

use \WezomCms\Counter\Widgets;

return [
    'widgets' => [
    	'counter' => Widgets\CounterWidget::class
	]
];
