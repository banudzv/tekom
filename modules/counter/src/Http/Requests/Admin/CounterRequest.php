<?php

namespace WezomCms\Counter\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use WezomCms\Core\Http\Requests\ChangeStatus\RequiredIfMessageTrait;
use WezomCms\Core\Traits\LocalizedRequestTrait;

class CounterRequest extends FormRequest
{
	use LocalizedRequestTrait;
	use RequiredIfMessageTrait;

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return $this->localizeRules(
			[
				'name' => 'nullable|string|max:255',
			],
			[
				'start' => 'required|string',
				'end' => 'required|string',
				'step' => 'required|integer',
				'duration' => 'required|integer',
				'published' => 'nullable',
				'sort' => 'nullable|string',
			]
		);
	}

	/**
	 * Get custom attributes for validator errors.
	 *
	 * @return array
	 */
	public function attributes()
	{
		return $this->localizeAttributes(
			[
				'name' => __('cms-counter::admin.name'),
			],
			[
				'position' => __('cms-core::admin.layout.Position'),
				'active' => __('cms-core::admin.layout.Published'),
				'start' => __('cms-counter::admin.start'),
				'end' => __('cms-counter::admin.end'),
				'step' => __('cms-counter::admin.step'),
				'duration' => __('cms-counter::admin.duration'),
			]
		);
	}
}
