@extends('cms-core::admin.crud.index')

@php
	/**
	 * @var $result \WezomCms\Counter\Models\Counter[]
	 */
@endphp

@section('content')

	<div class="table-responsive">
		<table class="table table-striped table-hover">
			<thead>
			<tr>
				<th class="sortable-column"></th>
				<th width="1%">@massControl($routeName)</th>
				<th>@lang('cms-counter::admin.name')</th>
				<th>@lang('cms-counter::admin.start')</th>
				<th>@lang('cms-counter::admin.end')</th>
				<th>@lang('cms-counter::admin.step')</th>
				<th>@lang('cms-counter::admin.duration')</th>
				<th width="1%" class="text-center">@lang('cms-core::admin.layout.Manage')</th>
			</tr>
			</thead>
			<tbody class="js-sortable"
				   data-params="{{ json_encode(['model' => encrypt($model)]) }}">
			@foreach($result as $obj)
				<tr data-id="{{ $obj->id }}">
					<td>
						<div class="js-sortable-handle sortable-handle">
							<i class="fa fa-arrows"></i>
						</div>
					</td>
					<td>@massCheck($obj)</td>
					<td>
						{!! $obj->name !!}
					</td>
					<td>{{ $obj->start }}</td>
					<td>{{ $obj->end }}</td>
					<td>{{ $obj->step }}</td>
					<td>{{ $obj->duration }}</td>
					<td>
						<div class="btn-group list-control-buttons" role="group">
							@smallStatus($obj)
							@editResource($obj, false)
							@deleteResource($obj)
						</div>
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
@endsection
