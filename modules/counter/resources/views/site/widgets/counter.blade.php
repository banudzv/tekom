@php
	/**
	  * @var $models |\Illuminate\Database\Eloquent\Collection|\WezomCms\Counter\Models\Counter[]
	  * @var $classes | string|null
	  */
    $classes = $classes ?? '';
    $modification = $modification ?? '';
@endphp
<div class="section {{ $modification ? 'section--' . $modification : null }}">
    <div class="container">
        <div class="counter {{ $classes }}">
            <div class="_grid _justify-center">
                <div class="_cell _cell--12 _df:cell--10">
                    <div class="title title--size-h2 _text-center">@lang('cms-counter::site.title')</div>
                    <div class="_flex _flex-wrap">
                        @foreach($models ?? [] as $counter)
                            @include('cms-ui::widgets.why-item',[
                                  'to' => $counter->end,
                                   'text' => $counter->name,
                             ])
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

