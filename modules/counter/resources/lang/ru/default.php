<?php

use WezomCms\Core\Enums\TranslationSide;

return [
    TranslationSide::ADMIN => [
		'Counter' => 'Счетчик',
		'name' => 'Название',
		'start' => 'Старт',
		'end' => 'Конец',
		'step' => 'Шаг',
		'duration' => 'Задержка',
    ],
    TranslationSide::SITE => [
    	'title' => 'Почему Теком'
    ],
];
