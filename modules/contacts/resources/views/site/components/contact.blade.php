<div class="contact">
    <div class="accordion accordion--fullwidth js-import"
         data-accordion='{"ns": "contact"}'
    >
        <div class="contact__head">
            <div class="accordion__head">
                <div class="contact__head-grid _mb-df _md:mb-none">
                    <div class="contact__block">
                        <div class="contact__info contact__info--city">
                            {!! $branch->city->name !!}
                        </div>
                        <div class="contact__info contact__info--type">
                            {!! $branch->typeForFront() !!}
                        </div>
                    </div>
                    <div class="contact__block">
                        <div class="contact__info contact__info--location">
                            @svg('location', 15, 20, 'contact__icon')
                            {!! $branch->address !!}
                        </div>
                    </div>
                    <div class="contact__block">
                        @foreach($branch->phones ?? [] as $phone)
                            <div class="contact__info contact__info--phone">
                                @svg('phone', 15, 15, 'contact__icon')
                                <a href="tel:{{$phone}}" class="link link--theme-default">{{ $phone }}</a>
                            </div>
                        @endforeach
                        <div class="contact__info contact__info--email">
                            @svg('email', 15, 15, 'contact__icon')
                            <a href="mailto:{{$branch->email}}" class="link link--theme-default">{{ $branch->email }}</a>
                        </div>
                    </div>
                    <div class="contact__block">
                        <div class="contact__info contact__info--schedule">
                            @svg('clock-filled', 15, 15, 'contact__icon')

                            <div>
                                {!! $branch->time_working !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if(!$branch->is_main)
            <div class="accordion__button"
                 data-accordion-ns="contact"
                 data-accordion-button="{{ 'contact-' . $branch->id }}"
                 data-show-more-open="{{ __('cms-ui::site.show more open') }}"
                 data-show-more-close="{{ __('cms-ui::site.show more close') }}"
            >
            </div>
            @endif
        </div>
        @if(!$branch->is_main)
            <div class="accordion__body" style="display: none"
             data-accordion-ns="contact"
             data-accordion-block="{{ 'contact-' . $branch->id }}">
            <div class="contact__body"
            >
                <div>
                    <div class="_mb-sm _md:mb-df">
                        <div class="contact__title">@lang('cms-contacts::site.Separate subdivision name')</div>
                        <div class="contact__content"><p>{{ $branch->name }}</p></div>
                    </div>
                    <div class="_mb-sm _md:mb-df">
                        <div class="contact__title">@lang('cms-contacts::site.Date and number of the decision')</div>
                        <div class="contact__content"><p>{{ $branch->date_create }}</p></div>
                    </div>
                    <div class="_flex">
                        <div class="contact__title">@lang('cms-contacts::site.Code egrpou')</div>
                        <div class="contact__content _ml-xxs"><p>{{ $branch->code }}</p></div>
                    </div>
                </div>
                <div>
                    <div class="_mb-sm _md:mb-df">
                        <div class="contact__title">@lang('cms-contacts::site.Types of financial services that')</div>
                        <div class="contact__content">
                            <p>@lang('cms-contacts::site.Types of financial services that description')</p>
                            <ul class="list list--theme-default _mt-md">
                                @foreach($branch->services ?? [] as $service)
                                    <li class="list__item">{{ $service }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                @if($branch->employee)
                    <div>
                        <div class="_mb-sm _md:mb-df">
                            @if(!$branch->is_main)
                                <div class="contact__title _mb-sm _md:mb-df">@lang('cms-contacts::site.Surname, name, patronymic and')</div>
                            @else
                                <div class="contact__title _mb-sm _md:mb-df">@lang('cms-contacts::site.Surname, name')</div>

                            @endif
                            <div class="contact__content _flex _items-start">
                                <div class="contact__image">
                                    <img src="{{$branch->employee->getImage()}}" alt=""/>
                                </div>
                                <div class="contact__name _mt-sm">
                                    {{ $branch->employee->name }}
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        @endif
    </div>
</div>
