<?php

use WezomCms\Contacts\Dashboards;
use WezomCms\Contacts\Widgets;

return [
    'widgets' => [
        'social-links' => Widgets\SocialLinks::class,
        'contacts:hotline' => Widgets\Hotline::class,
    ],
    'dashboards' => [
        Dashboards\ContactsDashboard::class,
    ],
];
