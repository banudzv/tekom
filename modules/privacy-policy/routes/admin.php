<?php

Route::namespace('WezomCms\\PrivacyPolicy\\Http\\Controllers\\Admin')
    ->group(function () {
        Route::settings('privacy-policy', 'PrivacyPolicyController');
    });
