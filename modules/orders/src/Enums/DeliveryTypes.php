<?php

namespace WezomCms\Orders\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

final class DeliveryTypes extends Enum implements LocalizedEnum
{
    public const POSTAL = 'postal';
    public const COURIER = 'courier';

    /**
     * Get the default localization key
     *
     * @return string
     */
    public static function getLocalizationKey(): string
    {
        return 'cms-orders::' . app('side') . '.delivery_types';
    }
}
