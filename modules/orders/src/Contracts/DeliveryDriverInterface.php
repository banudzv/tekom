<?php

namespace WezomCms\Orders\Contracts;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

interface DeliveryDriverInterface
{
    /**
     * @param  array  $stepData
     * @param $deliveryType
     * @return mixed
     */
    public function renderFormInputs(array $stepData, $deliveryType);

    /**
     * Validate incoming data
     *
     * @param  Request  $request
     * @return array
     * @throws ValidationException
     */
    public function validateData(Request $request): array;

    /**
     * Prepare data for saving in order.
     *
     * @param  Request  $request
     * @return array
     */
    public function prepareStoreData(Request $request): array;
}
