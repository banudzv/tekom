<?php

namespace WezomCms\Orders\Http\Controllers\Site;

use Illuminate\Http\Request;
use WezomCms\Core\Foundation\JsResponse;
use WezomCms\Core\Http\Controllers\SiteController;
use WezomCms\Orders\Contracts\NovaPoshtaServiceInterface;

class NovaPoshtaController extends SiteController
{
    /**
     * @param  Request  $request
     * @param  NovaPoshtaServiceInterface  $novaPoshta
     * @return JsResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getCities(Request $request, NovaPoshtaServiceInterface $novaPoshta)
    {
        $this->validate(
            $request,
            ['query' => 'string|min:3'],
            [],
            ['query.min' => __('cms-orders::site.checkout.Enter at least 3 characters')]
        );

        $descriptionKey = app()->getLocale() === 'ru' ? 'DescriptionRu' : 'Description';
        $cities = [];
        foreach ($novaPoshta->getCities($request->get('query')) as $datum) {
            $cities[] = [
                'description' => $datum->{$descriptionKey},
                'ref' => $datum->Ref,
            ];
        }

        return JsResponse::make()->massAssigment(compact('cities'));
    }

    /**
     * @param  Request  $request
     * @param  NovaPoshtaServiceInterface  $novaPoshta
     * @return JsResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getCityWarehouses(Request $request, NovaPoshtaServiceInterface $novaPoshta)
    {
        $this->validate(
            $request,
            ['city_ref' => 'required|string'],
            [],
            ['city_ref.required' => __('cms-orders::site.checkout.Choose a locality from the list')]
        );

        $descriptionKey = app()->getLocale() === 'ru' ? 'DescriptionRu' : 'Description';
        $warehouses = [];
        foreach ($novaPoshta->getCityWarehouses($request->get('city_ref')) as $datum) {
            $warehouses[] = [
                'description' => $datum->{$descriptionKey},
                'ref' => $datum->Ref,
            ];
        }

        return JsResponse::make()->massAssigment(compact('warehouses'));
    }
}
