<?php

namespace WezomCms\Orders\Http\Controllers\Site;

use Auth;
use Illuminate\View\View;
use WezomCms\Orders\Models\Buck;
use WezomCms\Orders\Models\Order;
use WezomCms\Catalog\Models\Product;
use Illuminate\Contracts\View\Factory;
use WezomCms\Core\Foundation\JsResponse;
use WezomCms\Catalog\Models\ProductTranslation;
use WezomCms\Core\Http\Controllers\SiteController;
use WezomCms\Orders\Contracts\CartAdapterInterface;

class CabinetOrdersController extends SiteController
{
    /**
     * @return Factory|View
     */
    public function index()
    {
        // Meta
        $this->addBreadcrumb(settings('orders.site.name'));
        $this->seo()
            ->setPageName(settings('orders.site.name'))
            ->setTitle(settings('orders.site.title'))
            ->setH1(settings('orders.site.h1'));

        // Select orders
        $orders = Order::where('user_id', Auth::id())
            ->with('items', 'items.product', 'delivery', 'payment', 'client', 'recipient', 'status', 'statusHistory')
            ->latest('id')
            ->paginate(settings('orders.site.limit', 10));

        // Render
        return view('cms-orders::site.cabinet.orders', [
            'orders' => $orders,
        ]);
    }

    /**
     * @return Factory|View
     */
    public function cart()
    {
        // Meta
        $this->addBreadcrumb(settings('orders.cabinet_cart.name'));
        $this->seo()
            ->setTitle(settings('orders.cabinet_cart.title'))
            ->setH1(settings('orders.cabinet_cart.h1'));

        $carts = Buck::query()
                        ->where('user_id', Auth::id())
                        ->orderBy('created_at', 'desc')
                        ->get();
                      
        // Render
        return view('cms-orders::site.cabinet.cart', compact('carts', $carts));
    }
}
