<?php

namespace WezomCms\Orders\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use WezomCms\Orders\Enums\DeliveryTypes;

class CreateOrderRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status_id' => 'required|exists:order_statuses,id',
            'payment_id' => 'required|exists:payments,id',
            'payed' => 'required|bool',
            'delivery_type' => ['nullable', Rule::in(DeliveryTypes::getValues())],
            'delivery_id' => 'nullable|int|exists:deliveries,id',
            'delivery_address' => 'nullable|string|max:255',
            'ttn' => 'nullable|string|max:100',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'status_id' => __('cms-orders::admin.orders.Status'),
            'payment_id' => __('cms-orders::admin.orders.Payment method'),
            'payed' => __('cms-orders::admin.orders.Payed'),
            'delivery_type' => __('cms-orders::admin.orders.Delivery type'),
            'delivery_id' => __('cms-orders::admin.orders.Delivery method'),
            'delivery_address' => __('cms-orders::admin.orders.Address'),
            'ttn' => __('cms-orders::admin.orders.TTN'),
        ];
    }
}
