@php
    /**
     * @var $stepData array
     */
@endphp
<div>
    <input type="text" required="required" disabled="disabled" name="address"
           value="{{ array_get($stepData, 'address') }}" placeholder="@lang('cms-orders::site.Введите адрес')">
</div>
