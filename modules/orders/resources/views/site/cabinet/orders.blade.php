@extends('cms-users::site.layouts.cabinet')

@php
    /**
     * @var $orders \Illuminate\Database\Eloquent\Collection|\WezomCms\Orders\Models\Order[]|\Illuminate\Pagination\LengthAwarePaginator
     */
@endphp

@section('content')
    <div class="container">
        <h1>{{ SEO::getH1() }}</h1>
        <div>
            <div>
                @widget('cabinet-menu')
            </div>
            <div>
                @if($orders->isNotEmpty())
                    @foreach($orders as $order)
                        @php
                            $countItems = $order->items->count();
                        @endphp
                        <div class="js-import" data-accordion='{"type":"multiple","slideTime":150}'>
                            <div class="{{ $order->status->class }}" data-wstabs-ns="orders" data-wstabs-button="{{ $order->id }}">
                                <div>@lang('cms-orders::site.Заказ №:number', ['number' => $order->id])</div>
                                <div>
                                    {{ $order->created_at->format('d.m.Y') }}
                                    {{ $order->created_at->format('H:i') }}
                                </div>
                                <div>
                                    {{ $countItems . ' ' . trans_choice('cms-orders::site.товар|товара|товаров', $countItems) }}
                                    @lang('cms-orders::site.Полная информация о заказе')
                                </div>
                                <div>@money($order->whole_purchase_price, true)</div>
                                <div>
                                    @if($order->status)
                                        <div class="{{ $order->status->class }}">{{ $order->status->name }}</div>
                                    @endif
                                </div>
                            </div>
                            <div data-wstabs-ns="orders" data-wstabs-block="{{ $order->id }}">
                                <table>
                                    @if($order->client->name)
                                        <tr>
                                            <td>@lang('cms-orders::site.Получатель:')</td>
                                            <td>{{($order->client->name ? $order->client->name: '' ) . ' ' . ($order->client->surname ? $order->client->surname: '')}}</td>
                                        </tr>
                                    @endif
                                    @if($order->client->phone)
                                        <tr>
                                            <td>@lang('cms-orders::site.Номер телефона:')</td>
                                            <td>{{$order->client->phone}}</td>
                                        </tr>
                                    @endif
                                    @if($order->client->email)
                                        <tr>
                                            <td>@lang('cms-orders::site.Эл. почта:')</td>
                                            <td>{{$order->client->email}}</td>
                                        </tr>
                                    @endif
                                    @if($order->payment)
                                        <tr>
                                            <td>@lang('cms-orders::site.Способ оплаты:')</td>
                                            <td>{{ $order->payment->name }}</td>
                                        </tr>
                                    @endif
                                    <tr>
                                        <td>@lang('cms-orders::site.Способ доставки:')</td>
                                        <td>{{ $order->full_delivery_type }}</td>
                                    </tr>
                                    <tr>
                                        <td>@lang('cms-orders::site.Адресс доставки:')</td>
                                        <td>{{ $order->delivery_address }}</td>
                                    </tr>
                                    @if($order->ttn)
                                        <tr>
                                            <td>@lang('cms-orders::site.Номер ТТН')</td>
                                            <td>{{ $order->ttn }}</td>
                                        </tr>
                                    @endif
                                </table>

                                @foreach($order->items as $item)
                                    @if($item->product)
                                        <a href="{{ $item->getFrontUrl() }}">
                                            <img src="{{ $item->getImageUrl() }}" alt="{{ $item->name }}">
                                        </a>
                                    @else
                                        <img src="{{ $item->getImageUrl() }}" alt="{{ $item->name }}">
                                    @endif
                                    @if($item->product)
                                        <a href="{{ $item->getFrontUrl() }}">{{ $item->name }}</a>
                                    @else
                                        <span>{{ $item->name }}</span>
                                    @endif
                                    <div>{{ $item->quantity }} x {{ $item->price }}{{ money()->siteCurrencySymbol() }}</div>
                                @endforeach
                                <div>
                                    @lang('cms-orders::site.Итого:')
                                    {{ $order->whole_purchase_price }}{{ money()->siteCurrencySymbol() }}
                                </div>
                                <div>
                                    @lang('cms-orders::site.Статус заказа')
                                    <ul>
                                        @foreach($order->statusHistory as $status)
                                            <li title="@lang('cms-orders::site.Изменен :date', ['date' => $status->pivot->created_at->format('d.m.Y H:i:s')])">
                                                {{ $status->name }}
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div>
                                    @if($order->payed)
                                        <div title="{{ __('cms-orders::site.Оплачен') . ' ' . $order->payed_at->format('d.m.Y H:i:s') }}">@lang('cms-orders::site.Заказ оплачен')</div>
                                    @else
                                        <div>@lang('cms-orders::site.Заказ не оплачен')</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div>@lang('cms-orders::site.Вы еще ничего не заказывали')</div>
                    <a href="{{ route('home') }}">@lang('cms-orders::site.Вернуться на главную')</a>
                @endif
            </div>
            @if($orders->hasPages())
                <div>{!! $orders->links() !!}</div>
            @endif
        </div>
    </div>
@endsection
