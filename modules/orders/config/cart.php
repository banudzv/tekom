<?php

return [
    'storage' => 'database', // or 'session'
    'precision' => 2,
    'quantity_precision' => 2,
];
