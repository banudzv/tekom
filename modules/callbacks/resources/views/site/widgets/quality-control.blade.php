@php
/**
 * @var $services array
 */

//<div class="popup">
  //  <div>@lang('cms-callbacks::site.Quality control title')</div>
  //  <div>@lang('cms-callbacks::site.Quality control description')</div>
  //  <form class="js-import" method="post" action="{{ route('callbacks.quality-control') }}" enctype="multipart/form-data">
  //      @csrf
  //      <input type="text" name="name" placeholder="@lang('cms-callbacks::site.Name')">
  //      <input type="tel" name="phone" placeholder="@lang('cms-callbacks::site.Phone') *" required="required">
  //      <input type="text" name="contract_number" placeholder="@lang('cms-callbacks::site.Contract number')">
  //      <input type="text" name="message" placeholder="@lang('cms-callbacks::site.Message')">
  //      <select name="service_id">
  //          @foreach($services ?? [] as $id => $item)
  //              <option value="{{ $id }}">{{ $item }}</option>
  //          @endforeach
  //      </select>
  //      <input type="file" name="file" placeholder="@lang('cms-callbacks::site.File')">
    $accepted_files = 'application/msword, application/vnd.openxmlformats-officedocument.wordprocessing, application/pdf, image/jpeg';
    $max_file_size = 15;
@endphp

<div class="section section--quality-control">
    <div class="container">
        <div class="quality-control">
            <div class="_grid _justify-center _spacer _spacer--md">
                <div class="_cell _cell--12 _md:cell--10 _df:cell--8">
                    <div class="quality-control__content">
                        <div class="quality-control__title">
                            @lang('cms-callbacks::site.Quality control title')
                        </div>
                        <div class="quality-control__description">
                            @lang('cms-callbacks::site.Quality control description')
                        </div>
                        @component('cms-ui::components.form.form-ajax', [
                                'class' => 'quality-control__form',
                                'method' => 'POST',
                                'autocomplete' => 'off',
                                'url' => route('callbacks.quality-control'),
                                'id' => uniqid('consultation-'),
                                'attrs' => [
                                  'enctype' => 'multipart/form-data'
                                ],
                            ])
                            @csrf
                            <div class="_grid _spacer _spacer--md">
                                <div class="_mb-xs _pb-xs _cell--12 _md:cell--6">
                                    @include('cms-ui::components.form.input', [
                                        'name' => 'name',
                                        'attributes' => [ 'placeholder' => __('cms-callbacks::site.Name') ],
                                        'classes' => 'form-item--theme-default',
                                        'mode' => 'text'
                                    ])
                                </div>
                                <div class="_mb-xs _pb-xs _cell--12 _md:cell--6">
                                    @component('cms-ui::components.form.select2',
                                         [
                                             'selectOptions' => [
                                                 'Factory' => 'DefaultSelect2'
                                            ],
                                            'options' => $services,
                                            'name' => uniqid('select2'),
                                            'attrs' => ['name' => 'service_id']
                                        ]
                                    )
                                        @foreach($services ?? [] as $id => $item)
                                            <option value="{{ $id }}">
                                                {{ $item }}
                                            </option>
                                        @endforeach
                                    @endcomponent
                                </div>
                                <div class="_mb-xs _pb-xs _cell--12 _md:cell--6">
                                    @php
                                        $inputmaskConfig = json_encode((object)[
                                            'mask' => '+38(999)-99-99-999',
                                            'androidMask' => '+38(999)-99-99-999'
                                        ])
                                    @endphp
                                    @include('cms-ui::components.form.input', [
                                        'name' => 'phone',
                                        'attributes' => [
                                            'placeholder' => __('cms-callbacks::site.Phone'),
                                            'data-mask' => $inputmaskConfig,
                                            'class' => 'js-import form-item__control',
                                            'required',
                                            'data-rule-phone'
                                         ],
                                        'modificators' => [],
                                        'component' => 'input',
                                        'classes' => 'form-item--theme-default',
                                        'mode' => 'tel'
                                    ])
                                </div>
                                <div class="_mb-xs _pb-xs _cell--12 _md:cell--6">
                                    @include('cms-ui::components.form.input', [
                                        'name' => 'contract_number',
                                        'attributes' => [ 'placeholder' => __('cms-callbacks::site.Contract number')],
                                        'component' => 'input',
                                        'classes' => 'form-item--theme-default',
                                        'mode' => 'text'
                                    ])
                                </div>
                                <div class="_mb-xs _cell _cell--12">
                                    @include('cms-ui::components.form.input', [
                                        'name' => 'message',
                                        'attributes' => [ 'placeholder' => __('cms-callbacks::site.Message') ],
                                        'modificators' => [],
                                        'component' => 'textarea',
                                        'classes' => 'form-item--theme-default',
                                        'mode' => 'text',
                                    ])
                                </div>
                                <div class="_cell _cell--12">
                                    <label for="file" class="file-upload js-import" data-upload-file>
                                        <span class="file-upload__area" data-upload-file-area>
                                            <span class="file-upload__link">
                                                <span class="file-upload__icon">@svg('plus')</span>
                                                <span class="file-upload__text">@lang('cms-callbacks::site.File')</span>
                                            </span>
                                        </span>
                                        <input data-max-size="15"
                                               data-size-exceeded-message="@lang('cms-callbacks::site.File too big')"
                                               accept="{{ $accepted_files }}"
                                               data-upload-file-control
                                               class="file-upload__control"
                                               id="file"
                                               type="file"
                                               hidden
                                               name="file">
                                    </label>
                                    <div class="_flex _justify-center">
                                        <div class="quality-control__info">
                                            @lang('cms-callbacks::site.file extension')<br>
                                            @lang('cms-callbacks::site.file size', [ 'size' => 15])
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="_flex _justify-center _mt-df">
                                @widget('ui:button', [
                                        'component' => 'button',
                                        'text' => __('cms-callbacks::site.Send'),
                                        'classes' => '_text-center',
                                        'attrs' => ['type' => 'submit'],
                                        'modificators' => ['color-accent', 'size-def', 'uppercase', 'ls-def', 'bold']
                                    ])
                            </div>
                        @endcomponent
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
