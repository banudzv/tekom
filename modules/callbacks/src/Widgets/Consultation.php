<?php

namespace WezomCms\Callbacks\Widgets;

use WezomCms\Core\Foundation\Widgets\AbstractWidget;

class Consultation extends AbstractWidget
{
    /**
     * View name.
     *
     * @var string|null
     */
    protected $view = 'cms-callbacks::site.widgets.consultation';

    /**
     * @return array|null
     */
//    public function execute(): ?array
//    {
////        dd('q');
//
////        return compact('models');
//    }
}
