<?php

namespace WezomCms\Callbacks\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use WezomCms\Callbacks\Models\Callback;
use WezomCms\Callbacks\Models\Consultation;

class ConsultationNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var Consultation
     */
    private $consultation;

    /**
     * Create a new notification instance.
     *
     * @param  Consultation  $consultation
     */
    public function __construct(Consultation $consultation)
    {
        $this->consultation = $consultation;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            'mail',
            'database'
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->subject(__('cms-callbacks::admin.email.New consultations order'))
            ->markdown('cms-callbacks::admin.notifications.email', [
                'order' => $this->consultation,
                'name' => __('cms-callbacks::admin.email.New consultations order'),
                'urlToAdmin' => route('admin.callback-consultations.edit', $this->consultation->id),
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'route_name' => 'admin.callback-consultations.edit',
            'route_params' => $this->consultation->id,
            'icon' => 'fa-phone',
            'color' => 'success',
            'heading' => __('cms-callbacks::admin.Consultations'),
            'description' => implode('. ', array_filter([$this->consultation->phone])),
        ];
    }
}

