<?php

namespace WezomCms\Callbacks\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use WezomCms\Callbacks\Models\Callback;
use WezomCms\Callbacks\Models\Consultation;
use WezomCms\Callbacks\Models\QualityControl;
use WezomCms\Callbacks\Models\Request;

class CallbackRequestNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var Request
     */
    private $request;

    /**
     * Create a new notification instance.
     *
     * @param Request  $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            'mail',
            'database'
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->subject(__('cms-callbacks::admin.email.New order'))
            ->markdown('cms-callbacks::admin.notifications.email', [
                'order' => $this->request,
                'name' => __('cms-callbacks::admin.email.New order'),
                'urlToAdmin' => route('admin.callback-requests.edit', $this->request->id),
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'route_name' => 'admin.callback-requests.edit',
            'route_params' => $this->request->id,
            'icon' => 'fa-phone',
            'color' => 'success',
            'heading' => __('cms-callbacks::admin.Request'),
            'description' => implode('. ', array_filter([$this->request->phone])),
        ];
    }
}
