<?php

namespace WezomCms\Callbacks\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use WezomCms\Callbacks\Models\Callback;
use WezomCms\Callbacks\Models\Consultation;
use WezomCms\Callbacks\Models\QualityControl;

class QualityControlNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var QualityControl
     */
    private $qualityControl;

    /**
     * Create a new notification instance.
     *
     * @param  QualityControl  $qualityControl
     */
    public function __construct(QualityControl $qualityControl)
    {
        $this->qualityControl = $qualityControl;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            'mail',
            'database'
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->subject(__('cms-callbacks::admin.email.New quality controls order'))
            ->markdown('cms-callbacks::admin.notifications.email', [
                'order' => $this->qualityControl,
                'name' => __('cms-callbacks::admin.email.New quality controls order'),
                'urlToAdmin' => route('admin.callback-quality-controls.edit', $this->qualityControl->id),
           ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'route_name' => 'admin.callback-consultations.edit',
            'route_params' => $this->qualityControl->id,
            'icon' => 'fa-phone',
            'color' => 'success',
            'heading' => __('cms-callbacks::admin.Quality control'),
            'description' => implode('. ', array_filter([$this->qualityControl->phone])),
        ];
    }
}


