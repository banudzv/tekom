<?php

namespace WezomCms\Callbacks;

use SidebarMenu;
use WezomCms\Callbacks\Models\Callback;
use WezomCms\Callbacks\Models\Consultation;
use WezomCms\Callbacks\Models\Insurance;
use WezomCms\Callbacks\Models\QualityControl;
use WezomCms\Callbacks\Models\Request;
use WezomCms\Core\BaseServiceProvider;
use WezomCms\Core\Contracts\PermissionsContainerInterface;

class CallbacksServiceProvider extends BaseServiceProvider
{
    /**
     * All module widgets.
     *
     * @var array|string|null
     */
    protected $widgets = 'cms.callbacks.callbacks.widgets';

    /**
     * Dashboard widgets.
     *
     * @var array|string|null
     */
    protected $dashboard = 'cms.callbacks.callbacks.dashboards';

    /**
     * @param  PermissionsContainerInterface  $permissions
     */
    public function permissions(PermissionsContainerInterface $permissions)
    {
        $permissions->add('callbacks', __('cms-callbacks::admin.Callbacks'))->withEditSettings();
        $permissions->add('callback-consultations', __('cms-callbacks::admin.Consultations'));
        $permissions->add('callback-quality-controls', __('cms-callbacks::admin.Quality control'));
        $permissions->add('callback-requests', __('cms-callbacks::admin.Request'));
        $permissions->add('callback-insurances', __('cms-callbacks::admin.Insurances'));
    }

    public function adminMenu()
    {
        $countConsultation = Consultation::where('read', 0)->count();
        $countQualityControl = QualityControl::where('read', 0)->count();
        $countRequest = Request::where('read', 0)->count();
        $countInsurance = Insurance::where('read', 0)->count();

        $allCount = $countConsultation + $countQualityControl + $countRequest;

        $callback = SidebarMenu::add(__('cms-callbacks::admin.Callbacks'), route('admin.callbacks.index'))
            ->data('permission', 'callbacks.view')
            ->data('badge', $allCount)
            ->data('badge_type', 'warning')
            ->data('icon', 'fa-phone')
            ->data('position', 15);

        $callback
            ->add(__('cms-callbacks::admin.Consultations'), route('admin.callback-consultations.index'))
            ->data('permission', 'callbacks.view')
            ->data('icon', 'fa-phone')
            ->data('badge', $countConsultation)
            ->data('badge_type', 'warning')
            ->data('position', 1)
        ;

        $callback
            ->add(__('cms-callbacks::admin.Quality control'), route('admin.callback-quality-controls.index'))
            ->data('permission', 'callbacks.view')
            ->data('icon', 'fa-phone')
            ->data('badge', $countQualityControl)
            ->data('badge_type', 'warning')
            ->data('position', 2)
        ;

        $callback
            ->add(__('cms-callbacks::admin.Request'), route('admin.callback-requests.index'))
            ->data('permission', 'callbacks.view')
            ->data('icon', 'fa-phone')
            ->data('badge', $countRequest)
            ->data('badge_type', 'warning')
            ->data('position', 3)
        ;

        $callback
            ->add(__('cms-callbacks::admin.Insurances'), route('admin.callback-insurances.index'))
            ->data('permission', 'callbacks.view')
            ->data('icon', 'fa-phone')
            ->data('badge', $countInsurance)
            ->data('badge_type', 'warning')
            ->data('position', 4)
        ;
    }
}
