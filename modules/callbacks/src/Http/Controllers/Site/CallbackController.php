<?php

namespace WezomCms\Callbacks\Http\Controllers\Site;

use Notification;
use NotifyMessage;
use WezomCms\Callbacks\Http\Requests\Site\CallbackConsultationRequest;
use WezomCms\Callbacks\Http\Requests\Site\CallbackQualityControlRequest;
use WezomCms\Callbacks\Http\Requests\Site\CallbackRequest;
use WezomCms\Callbacks\Http\Requests\Site\InsuranceRequest;
use WezomCms\Callbacks\Models\Callback;
use WezomCms\Callbacks\Models\Consultation;
use WezomCms\Callbacks\Models\Insurance;
use WezomCms\Callbacks\Models\QualityControl;
use WezomCms\Callbacks\Models\Request;
use WezomCms\Callbacks\Notifications\CallbackNotification;
use WezomCms\Callbacks\Notifications\CallbackRequestNotification;
use WezomCms\Callbacks\Notifications\ConsultationNotification;
use WezomCms\Callbacks\Notifications\InsuranceNotification;
use WezomCms\Callbacks\Notifications\QualityControlNotification;
use WezomCms\Core\Foundation\JsResponse;
use WezomCms\Core\Http\Controllers\PopupController;
use WezomCms\Core\Models\Administrator;
use WezomCms\Services\Repositories\ServiceRepository;

class CallbackController extends PopupController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function popup()
    {
        $user = \Auth::user();
        $name = $user ? $user->full_name : null;
        $phone = $user ? $user->phone : null;

        return view('cms-callbacks::site.popups.callback', compact('name', 'phone'));
    }

    public function popupRequest()
    {

        $repository = resolve(ServiceRepository::class);

        $services = $repository->getBySelect(true, false, __('cms-callbacks::site.Service choice'));

        return view('cms-callbacks::site.widgets.request', compact('services'));
    }

    public function popupInsurance()
    {

        $repository = resolve(ServiceRepository::class);

        $services = $repository->getBySelect(true, false, __('cms-callbacks::site.Service choice'));

        return view('cms-callbacks::site.popups.insurance', compact('services'));
    }

    public function sendConsultation(CallbackConsultationRequest $request)
    {
        $data = [
            'name' => $request->get('name'),
            'phone' => $request->get('phone'),
            'time' => $request->get('time'),
        ];
        try {
            $consultation = new Consultation($data);

            if($consultation->save()){

                $administrators = Administrator::toNotifications('callback-consultations.edit')->get();
                Notification::send($administrators, new ConsultationNotification($consultation));

                return JsResponse::make()
                    ->reset()
                    ->notification(
                        NotifyMessage::success(__('cms-callbacks::site.Form successfully submitted!')));
            } else {
                return JsResponse::make()
                    ->reset()
                    ->notification(NotifyMessage::error(__('cms-callbacks::site.Error creating request!')));
            }

        } catch (\Exception $exception){
            return JsResponse::make()
                ->reset(false)
                ->notification(NotifyMessage::error($exception->getMessage()));
        }
    }

    public function sendQualityControl(CallbackQualityControlRequest $request)
    {
        try {
            if(isset($request['service_id']) && $request['service_id'] == 0){
                $request['service_id'] = null;
            }

            $qualityControl = new QualityControl($request->all());

            if($qualityControl->save()){

                if($request->file('file')){
                    $qualityControl->uploadFile($request->file('file'));
                }

                $administrators = Administrator::toNotifications('callback-quality-control.edit')->get();
                Notification::send($administrators, new QualityControlNotification($qualityControl));

                return JsResponse::make()
                    ->reset()
                    ->notification(
                        NotifyMessage::success(__('cms-callbacks::site.Form successfully submitted!')));
            } else {
                return JsResponse::make()
                    ->reset()
                    ->notification(NotifyMessage::error(__('cms-callbacks::site.Error creating request!')));
            }
        } catch (\Exception $exception){
            return JsResponse::make()
                ->reset(false)
                ->notification(NotifyMessage::error($exception->getMessage()));
        }
    }

    public function sendRequest(CallbackRequest $request)
    {
        try {
            if(isset($request['service_id']) && $request['service_id'] == 0){
                $request['service_id'] = null;
            }

            $callbackRequest = new Request($request->all());

            if($callbackRequest->save()){

                $administrators = Administrator::toNotifications('callback-request.edit')->get();
                Notification::send($administrators, new CallbackRequestNotification($callbackRequest));

                return JsResponse::make()
                    ->reset()
                    ->notification(
                        NotifyMessage::success(__('cms-callbacks::site.Form successfully submitted!')));
            } else {
                return JsResponse::make()
                    ->reset()
                    ->notification(NotifyMessage::error(__('cms-callbacks::site.Error creating request!')));
            }
        } catch (\Exception $exception){
            return JsResponse::make()
                ->reset(false)
                ->notification(NotifyMessage::error($exception->getMessage()));
        }
    }

    /**
     * @param  CallbackRequest  $request
     * @return JsResponse
     */
    public function sendForm(CallbackRequest $request)
    {
        try {
            $data = [
                'name' => $request->get('username'),
                'phone' => $request->get('phone'),
            ];

            $order = new Callback($data);

            if ($order->save()) {
                $administrators = Administrator::toNotifications('callbacks.edit')->get();
                Notification::send($administrators, new CallbackNotification($order));

                return JsResponse::make()
                    ->reset()
                    ->notification(
                        NotifyMessage::success(__('cms-callbacks::site.Form successfully submitted!')));
            } else {
                return JsResponse::make()
                    ->reset()
                    ->notification(NotifyMessage::error(__('cms-callbacks::site.Error creating request!')));
            }
        } catch (\Exception $exception){
            return JsResponse::make()
                ->reset(false)
                ->notification(NotifyMessage::error($exception->getMessage()));
        }
    }

    /**
     * @param  CallbackRequest  $request
     * @return JsResponse
     */
    public function sendInsurance(InsuranceRequest $request)
    {
        try {

            $data = [
                'name' => $request->get('name'),
                'phone' => $request->get('phone'),
                'time' => $request->get('text'),
                'service_id' => $request->get('service_id'),
            ];

            if(isset($request['service_id']) && $request['service_id'] == 0){
                $data['service_id'] = null;
            };

            $insurance = new Insurance($data);

            if ($insurance->save()) {
                $administrators = Administrator::toNotifications('callback-insurances.edit')->get();
                Notification::send($administrators, new InsuranceNotification($insurance));

                return JsResponse::make()
                    ->reset()
                    ->notification(
                        NotifyMessage::success(__('cms-callbacks::site.Form successfully submitted!')));
            } else {
                return JsResponse::make()
                    ->reset()
                    ->notification(NotifyMessage::error(__('cms-callbacks::site.Error creating request!')));
            }
        } catch (\Exception $exception){
            return JsResponse::make()
                ->reset(false)
                ->notification(NotifyMessage::error($exception->getMessage()));
        }
    }
}
