<?php

namespace WezomCms\Callbacks\Http\Controllers\Admin;

use WezomCms\Callbacks\Http\Requests\Admin\ConsultationRequest;
use WezomCms\Callbacks\Models\Consultation;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;

class ConsultationController extends AbstractCRUDController
{
    /**
     * Model name.
     *
     * @var string
     */
    protected $model = Consultation::class;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-callbacks::admin.consultation';

    /**
     * Resource route name.
     *
     * @var string
     */
    protected $routeName = 'admin.callback-consultations';

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = ConsultationRequest::class;

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-callbacks::admin.Consultations');
    }
}

