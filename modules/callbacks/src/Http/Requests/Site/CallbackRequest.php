<?php

namespace WezomCms\Callbacks\Http\Requests\Site;

use Illuminate\Foundation\Http\FormRequest;

class CallbackRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|string|max:255',
            'phone' => 'required|string|max:255|regex:/^\+?[\d\s\(\)-]+$/',
            'message' => 'nullable|string',
            'service_id' => 'nullable|integer',
            'time' => 'nullable|string',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name' => __('cms-callbacks::site.Name'),
            'phone' => __('cms-callbacks::site.Phone'),
            'message' => __('cms-callbacks::site.Message'),
            'service_id' => __('cms-callbacks::site.Service'),
            'time' => __('cms-callbacks::site.Time'),
        ];
    }
}
