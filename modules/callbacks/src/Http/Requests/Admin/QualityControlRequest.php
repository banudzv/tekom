<?php

namespace WezomCms\Callbacks\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class QualityControlRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|string|max:255',
            'phone' => 'required|string|max:255|regex:/^\+?[\d\s\(\)-]+$/',
            'contract_number' => 'nullable|string',
            'message' => 'nullable|string',
            'service_id' => 'nullable|integer',
            'file' => ['nullable', 'file'],
            'read' => 'required',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name' => __('cms-callbacks::site.Name'),
            'phone' => __('cms-callbacks::site.Phone'),
            'contract_number' => __('cms-callbacks::site.Contract number'),
            'message' => __('cms-callbacks::site.Message'),
            'file' => __('cms-callbacks::site.File'),
            'service_id' => __('cms-callbacks::site.Service'),
            'read' => __('cms-callbacks::admin.Read'),
        ];
    }
}


