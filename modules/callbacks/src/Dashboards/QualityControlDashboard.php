<?php

namespace WezomCms\Callbacks\Dashboards;

use WezomCms\Callbacks\Models\Callback;
use WezomCms\Callbacks\Models\Consultation;
use WezomCms\Callbacks\Models\QualityControl;
use WezomCms\Core\Foundation\Dashboard\AbstractValueDashboard;

class QualityControlDashboard extends AbstractValueDashboard
{
    /**
     * @var null|int - cache time in minutes.
     */
    protected $cacheTime = 5;

    /**
     * @var null|string - permission for link
     */
    protected $ability = 'callback-quality-controls.view';

    /**
     * @return int
     */
    public function value(): int
    {
        return QualityControl::count();
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return __('cms-callbacks::admin.Quality control');
    }

    /**
     * @return string
     */
    public function icon(): string
    {
        return 'fa-phone';
    }

    /**
     * @return string
     */
    public function iconColorClass(): string
    {
        return 'color-success';
    }

    /**
     * @return null|string
     */
    public function url(): ?string
    {
        return route('admin.callback-quality-controls.index');
    }
}


