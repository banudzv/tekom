<?php

namespace WezomCms\Users\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use WezomCms\Core\Rules\Phone;
use WezomCms\Users\Models\User;
use WezomCms\Users\Rules\EmailOrPhone;

class RegisterRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            User::PHONE => ['nullable', 'string', new Phone()],
            'email' => ['required', 'string', 'unique:users'],
            'password' => [
                'required',
                'string',
                'min:' . config('cms.users.users.password_min_length'),
                'max:255',
                'confirmed'
            ],
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name' => __('cms-users::site.cabinet.Username'),
            User::EMAIL => __('cms-users::site.cabinet.E-mail'),
            User::PHONE => __('cms-users::site.cabinet.Phone'),
            'login' => __('cms-users::site.cabinet.Login'),
            'password' => __('cms-users::site.cabinet.Password'),
            'agree' => __('cms-users::site.auth.Agree'),
        ];
    }
}
