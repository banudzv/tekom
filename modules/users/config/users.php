<?php

use WezomCms\Users\Dashboard;
use WezomCms\Users\Widgets;

return [
    'sms_service' => 'esputnik', // Support: turbosms, esputnik
    'password_min_length' => 8,
    'supported_socials' => [
        'facebook',
        'google',
        'twitter',
    ],
    'socials' => [
        'facebook' => [
            'scopes' => ['email'],
            'fields' => ['first_name', 'last_name', 'email'],
        ],
    ],
    'widgets' => [
        'cabinet-button' => Widgets\CabinetButton::class,
        'cabinet-menu' => Widgets\CabinetMenu::class,
        'cabinet-submenu' => Widgets\CabinetSubMenu::class,
        'cabinet-socials' => Widgets\CabinetSocials::class,
        'cabinet-auth-socials' => Widgets\CabinetAuthSocials::class,
    ],
    'dashboards' => [
        Dashboard\UsersDashboard::class,
        //Dashboard\ActiveUsersDashboard::class
    ]
];
