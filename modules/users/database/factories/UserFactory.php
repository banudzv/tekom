<?php

use Faker\Generator as Faker;
use WezomCms\Users\Models\User;

/** @var $factory \Illuminate\Database\Eloquent\Factory */
$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName,
        'surname' => $faker->lastName,
        'phone' => $faker->numerify('+38 (0##) ### ## ##'),
        'email' => $faker->email,
        'registered_through' => User::EMAIL,
        'password' => '',
        'active' => $faker->boolean,
    ];
});
