<?php

use WezomCms\Core\Enums\TranslationSide;

return [
    TranslationSide::SITE => [
        'Partners' => 'Партнери',
    ]
];
