<?php

use WezomCms\Core\Enums\TranslationSide;

return [
    TranslationSide::ADMIN => [
        'Partners' => 'Партнеры',
        'Partners Category' => 'Категории для партнеров',
        'Partners list' => 'Список партнеров',
        'Name' => 'Название',
        'Image' => 'Изображение',
        'No image' => 'Нет изображения',
        'Partners limit at page' => 'Кол-во отображаемых элементов на странице',
        'Description' => 'Вид деятельности',
        'Text' => 'Текстовое описание страницы',
        'Category' => 'Категории',
    ],
    TranslationSide::SITE => [
        'Partners' => 'Партнеры',
        'title' => 'Наши партнеры'
    ]
];
