@extends('cms-ui::layouts.main')

@php
    /**
     * @var $result \WezomCms\Partners\Models\Partner[]|\Illuminate\Pagination\LengthAwarePaginator
     */
@endphp

@section('content')
    <div class="container">
        @widget('ui:breadcrumbs')
        <h1>{{ SEO::getH1() }}</h1>
        <div class="wysiwyg js-import" data-wrap-media data-draggable-table>
            {!! SEO::getSeoText() !!}
        </div>
        <div>
            @forelse($result as $item)
                <div>
                    <div>{{ $item->name }}</div>
                    <div>{{ $item->description }}</div>
                    <img class="lozad js-import" src="{{ url('assets/images/loader.gif') }}"
                         data-lozad="{{ $item->getImageUrl() }}" alt="{{ $item->name }}">
                </div>
            @empty
                <div>
                    @emptyResult
                </div>
            @endforelse
        </div>

        @if($result->hasPages())
            <div>{!! $result->links() !!}</div>
        @endif
    </div>
@endsection
