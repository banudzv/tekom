@extends('cms-core::admin.crud.index')

@section('content')
    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th class="sortable-column"></th>
                <th width="1%">@massControl($routeName)</th>
                <th>@lang('cms-partners::admin.Image')</th>
                <th>@lang('cms-partners::admin.Name')</th>
                <th>@lang('cms-partners::admin.Category')</th>
                <th width="1%" class="text-center">@lang('cms-core::admin.layout.Manage')</th>
            </tr>
            </thead>
            <tbody class="js-sortable"
                   data-params="{{ json_encode(['model' => encrypt($model), 'page' => $result->currentPage(), 'limit' => $result->perPage()]) }}">
            @foreach($result as $obj)
                <tr data-id="{{ $obj->id }}">
                    <td>
                        <div class="js-sortable-handle sortable-handle">
                            <i class="fa fa-arrows"></i>
                        </div>
                    </td>
                    <td>@massCheck($obj)</td>
                    <td>
                        <a href="{{ url($obj->getImageUrl()) }}" data-fancybox>
                            <img src="{{ url($obj->getImageUrl()) }}" alt="{{ $obj->name }}" height="50">
                        </a>
{{--                        @if($obj->imageExists())--}}
{{--                            <a href="{{ url($obj->getImageUrl()) }}" data-fancybox>--}}
{{--                                <img src="{{ url($obj->getImageUrl()) }}" alt="{{ $obj->name }}" height="50">--}}
{{--                            </a>--}}
{{--                        @else--}}
{{--                            <span class="text-info">@lang('cms-partners::admin.No image')</span>--}}
{{--                        @endif--}}
                    </td>
                    <td>@editResource($obj)</td>
                    <td>
                        @editResource(['obj'=> $obj->category, 'text' => $obj->category->name, 'ability' => 'partner-categories.edit', 'target' => '_blank'])
                    </td>
                    <td>
                        <div class="btn-group list-control-buttons" role="group">
                            @smallStatus($obj)
                            @editResource($obj, false)
                            @deleteResource($obj)
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
