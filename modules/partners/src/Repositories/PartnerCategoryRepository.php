<?php

namespace WezomCms\Partners\Repositories;

use Illuminate\Database\Eloquent\Collection;
use WezomCms\Core\Repositories\AbstractRepository;
use WezomCms\Partners\Models\Category;

class PartnerCategoryRepository extends AbstractRepository
{
    /**
     * @inheritDoc
     */
    protected function model()
    {
        return Category::class;
    }

    public function getByFront(): Collection
    {
        return $this->getModelQuery()
            ->published()
            ->with([
                'translations',
                'partners' => function($query){
                    $query->where('published', true)->orderBy('sort');
                }
            ])
            ->whereHas('partners', function($query){
                $query->where('published', true);
            })
            ->orderBy('sort')
            ->get()
            ;
    }
}
