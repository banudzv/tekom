<?php

namespace WezomCms\Partners\Http\Controllers\Admin;

use WezomCms\Core\Http\Controllers\AbstractCRUDController;
use WezomCms\Partners\Http\Requests\Admin\CategoryRequest;
use WezomCms\Partners\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class CategoryController extends AbstractCRUDController
{
    /**
     * Model name.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-partners::admin.category';

    /**
     * Resource route name.
     *
     * @var string
     */
    protected $routeName = 'admin.partner-categories';

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = CategoryRequest::class;

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-partners::admin.Partners Category');
    }

    /**
     * @param  Builder  $query
     * @param  Request  $request
     */
    protected function selectionIndexResult($query, Request $request)
    {
        $query->orderBy('sort');
    }
}
