<?php

namespace WezomCms\Partners\Http\Controllers\Site;

use WezomCms\Core\Http\Controllers\SiteController;
use WezomCms\Partners\Models\Partner;

class PartnersController extends SiteController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $settings = settings('partners.site', []);

        // Selection
        $result = Partner::published()
            ->orderBy('sort')
            ->latest('id')
            ->paginate(array_get($settings, 'limit', 10));

        // Breadcrumbs
        $this->addBreadcrumb(array_get($settings, 'name'), route('partners'));

        // SEO
        $this->seo()
            ->setPageName(array_get($settings, 'name'))
            ->setTitle(array_get($settings, 'title'))
            ->setH1(array_get($settings, 'h1'))
            ->setSeoText(array_get($settings, 'text'))
            ->setDescription(array_get($settings, 'description'))
            ->metatags()
            ->setKeywords(array_get($settings, 'keywords'))
            ->setNext($result->nextPageUrl())
            ->setPrev($result->previousPageUrl());

        // Render
        return view('cms-partners::site.index', compact('result'));
    }
}
