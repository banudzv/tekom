<?php

namespace WezomCms\Partners\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * \WezomCms\Partners\Models\PartnerTranslation
 *
 * @property int $id
 * @property int $partner_id
 * @property string $name
 * @property string|null $description
 * @property string $locale
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\PartnerTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\PartnerTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\PartnerTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\PartnerTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\PartnerTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\PartnerTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\PartnerTranslation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Partners\Models\PartnerTranslation wherePartnerId($value)
 * @mixin \Eloquent
 */
class PartnerTranslation extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];
}
