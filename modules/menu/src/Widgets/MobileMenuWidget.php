<?php

namespace WezomCms\Menu\Widgets;

use WezomCms\Core\Foundation\Helpers;

class MobileMenuWidget extends MenuWidget
{
    /**
     * @return array|null
     */
    public function execute(): ?array
    {
        $menu = $this->getMenu('mobile');

        $menu = Helpers::groupByParentId($menu);

        $maxDepth = config('cms.menu.menu.groups.mobile.depth', 10);

        return compact('menu', 'maxDepth');
    }
}
