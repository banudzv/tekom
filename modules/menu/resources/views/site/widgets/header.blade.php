@php
    /**
     * @var $menu array|\WezomCms\Menu\Models\Menu[][]
     * @var $maxDepth int
     */
@endphp
<nav class="menu" data-dropdown-menu="burger">
    <div class="container container--theme-dropdown-menu">
        <div class="_grid">
            <div class="_cell _cell--8">
                <ul class="menu__list">
                    @if(isset($menu[null]))
                        @foreach($menu[null] as $item)
                            <li class="menu__item">
                                @if($mode = $item->activeMode())
                                    @if($mode === \WezomCms\Menu\Models\Menu::MODE_SPAN)
                                        <span class="menu__link is-active"><span>{{ $item->name }}</span></span>
                                    @else
                                        <a href="{{ url($item->url) }}" class="menu__link is-active"><span>{{ $item->name }}</span></a>
                                    @endif
                                @else
                                    <a href="{{ url($item->url) }}" class="menu__link"><span>{{ $item->name }}</span></a>
                                @endif
                                {{-- @todo тут вывод списка ссылок в категории --}}
                                {{--@if($loop->last)--}}
                                    {{--<ul class="menu__sublist">--}}
                                        {{--<li class="menu__subitem">--}}
                                            {{--<a href="#" class="menu__sublink is-active"><span>{{ $item->name }}</span></a>--}}
                                        {{--</li>--}}
                                        {{--<li class="menu__subitem">--}}
                                            {{--<a href="#" class="menu__sublink is-active"><span>{{ $item->name }}</span></a>--}}
                                        {{--</li>--}}
                                        {{--<li class="menu__subitem">--}}
                                            {{--<a href="#" class="menu__sublink is-active"><span>{{ $item->name }}</span></a>--}}
                                        {{--</li>--}}
                                        {{--<li class="menu__subitem">--}}
                                            {{--<a href="#" class="menu__sublink is-active"><span>{{ $item->name }}</span></a>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}
                                {{--@endif--}}
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
            <div class="_cell _cell--4">
                <div class="contacts-block contacts-block--header">
                    @widget('contacts:hotline', ['icon' => true, 'classes' => 'hotline--theme-menu'])
                    @widget('ui:button', ['component' => 'button', 'classes' => '_mt-md _mb-sm js-import', 'attrs' => ['type' => 'button', 'data-mfp' => 'ajax', 'data-mfp-src' => route('callbacks.popup')], 'text' => 'обратная связь', 'modificators' => ['color-light-transparent', 'size-def', 'uppercase', 'ls-def', 'bold']])
                    @widget('branch:main-contact')
                </div>
            </div>
        </div>
    </div>
</nav>
