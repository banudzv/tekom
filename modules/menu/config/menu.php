<?php

use WezomCms\Menu\Widgets;

return [
    'widgets' => [
        'menu' => Widgets\MenuWidget::class,
        'mobile-menu' => Widgets\MobileMenuWidget::class,
    ],
    'groups' => [
        'header' => [
            'name' => 'cms-menu::admin.Header',
            'depth' => 3,
        ],
        'footer' => [
            'name' => 'cms-menu::admin.Footer',
            'depth' => 3,
        ],
        'mobile' => [
            'name' => 'cms-menu::admin.Mobile',
            'depth' => 10,
        ],
    ],
];
