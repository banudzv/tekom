<?php

use Faker\Generator as Faker;

$factory->define(\WezomCms\Menu\Models\Menu::class, function (Faker $faker, array $attributes = []) {
    return [
        'name' => $faker->word,
        'url' => $faker->url,
        'published' => $faker->boolean(80),
        'sort' => $faker->randomNumber(5),
        'group' => array_get($attributes, 'group', array_rand(config('cms.menu.menu.groups', []))),
        'parent_id' => array_get($attributes, 'parent_id'),
    ];
});
