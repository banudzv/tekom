<?php

use Faker\Generator as Faker;
use WezomCms\Catalog\Models\Specifications\Specification;
use WezomCms\Catalog\Models\Specifications\SpecValue;

$factory->define(SpecValue::class, function (Faker $faker) {
    return [
        'published' => true,
        'sort' => $faker->randomNumber(),
        'specification_id' => Specification::inRandomOrder()->first()->id,
        'name' => $faker->realText(),
    ];
});
