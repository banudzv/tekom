<?php

namespace WezomCms\Catalog\Traits;

use Illuminate\Support\Collection as SupportCollection;
use WezomCms\Catalog\Models\Product;

/**
 * Trait ProductFlagsTrait
 * @package WezomCms\Catalog\Traits
 * @mixin Product
 */
trait ProductFlagsTrait
{

    /**
     * @return bool
     */
    public function getHasFlagAttribute(): bool
    {
        return $this->novelty || $this->sale || $this->popular;
    }

    /**
     * @return string
     */
    public function getFlagColorAttribute(): string
    {
        if ($this->sale) {
            return 'sale';
        } elseif ($this->popular) {
            return 'top';
        } elseif ($this->novelty) {
            return 'new';
        } else {
            return '';
        }
    }

    /**
     * @return string
     */
    public function getFlagTextAttribute(): string
    {
        if ($this->sale) {
            return 'sale';
        } elseif ($this->popular) {
            return 'top';
        } elseif ($this->novelty) {
            return 'new';
        } else {
            return '';
        }
    }

    /**
     * @return SupportCollection
     */
    public function getFlagsAttribute(): SupportCollection
    {
        $result = collect();

        if ($this->sale) {
            $result->put('sale', ['color' => 'sale', 'text' => $this->getSaleText()]);
        }
        if ($this->popular) {
            $result->put('popular', ['color' => 'top', 'text' => __('cms-catalog::site.catalog.Popular')]);
        }
        if ($this->novelty) {
            $result->put('novelty', ['color' => 'new', 'text' => __('cms-catalog::site.catalog.Novelty')]);
        }

        return $result;
    }

    /**
     * @return array|string|null
     */
    private function getSaleText()
    {
        return __(
            'cms-catalog::site.catalog.Sale:percent',
            ['percent' => $this->discount_percentage ? " -{$this->discount_percentage}%" : '']
        );
    }
}
