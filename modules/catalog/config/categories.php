<?php

return [
    'max_depth' => 3,
    'image' => [
        'directory' => 'categories/images',
        'default' => 'medium',
        'sizes' => [
            'medium' => [
                'width' => 256,
                'height' => 204,
                'mode' => 'fit',
            ],
        ],
    ],
    'icon' => [
        'directory' => 'categories/icons',
        'default' => 'medium',
        'sizes' => [
            'medium' => [
                'width' => 256,
                'height' => 204,
                'mode' => 'fit',
            ],
        ],
    ],
];
