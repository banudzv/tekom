@php
    /**
     * @var $product \WezomCms\Catalog\Models\Product
     * @var $variations \Illuminate\Support\Collection|\WezomCms\Catalog\Models\Product[]
     */
@endphp
<div>
    <h1>{{ SEO::getH1() }}</h1>
    <div>@lang('cms-catalog::site.Код товара'): {{ $product->id }}</div>
    @if($product->availableForPurchase())
        <div>@lang('cms-catalog::site.Есть в наличии')!</div>
    @else
        <div>@lang('cms-catalog::site.Нет в наличии')!</div>
    @endif
    <div>
        <div>@money($product->cost, true)</div>
        @if($product->old_cost)
            <span>@money($product->old_cost)</span>
            <small>{{ money()->siteCurrencySymbol() }}</small>
        @endif
    </div>
    @widget('product-labels', compact('product'))
    <div>
        @if($variations->isNotEmpty())
            <div>@lang('cms-catalog::site.Цвет'):</div>
            @foreach($variations as $variation)
                @if($product->id === $variation->id)
                    <span title="{{ $variation->color->name }}" style="width: 20px; height:20px; display:block; background-color: {{ $variation->color->color }}"></span>
                @else
                    <a href="{{ $variation->getFrontUrl() }}" title="{{ $variation->color->name }}"
                       style="width: 20px; height:20px; display:block; background-color: {{ $variation->color->color }}"></a>
                @endif
            @endforeach
        @endif

        @if($product->availableForPurchase())
            <div>@lang('cms-catalog::site.Количество:')</div>
            <div>
                @svg('common', 'minus', 17)
                <input type="number" name="input-quantity"
                       value="{{ $product->minCountForPurchase() }}"
                       min="{{ $product->minCountForPurchase() }}"
                       max="9999"
                       step="{{ $product->stepForPurchase() }}">
                @svg('common', 'plus', 17)
            </div>
        @endif
    </div>
    <div>
        @widget('comparison:product-button', ['product' => $product])

        @if($product->availableForPurchase())
            <button class="{{ $product->in_cart ? 'in-cart' : '' }}"
                    data-cart-button="{{ $product->id }}"
                    title="{{ $product->in_cart ? __('cms-catalog::site.Открыть корзину') : __('cms-catalog::site.Добавить в корзину') }}"
                    data-cart-target="{{ json_encode(['action' => 'add', 'id' => $product->id, 'count' => $product->minCountForPurchase()]) }}">
                {{ $product->in_cart ? __('cms-catalog::site.в корзине') : __('cms-catalog::site.купить') }}
            </button>
        @else
            <button disabled="disabled">@lang('cms-catalog::site.Нет в наличии')</button>
        @endif

{{--        @widget('favorites:product-button', ['favorable' => $product])--}}

        @if($product->availableForPurchase())
            @widget('buy-one-click:button', compact('product'))
        @endif
    </div>
</div>
