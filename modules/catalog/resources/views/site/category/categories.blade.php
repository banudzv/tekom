@php
    /**
     * @var $result \Illuminate\Pagination\LengthAwarePaginator|\WezomCms\Catalog\Models\Category[]
     **/
@endphp

@extends('cms-ui::layouts.main')

@section('content')
    <div class="container">
        <h1>{{ SEO::getH1() }}</h1>
        <div>
            @forelse($result as $category)
                <a href="{{ $category->getFrontUrl() }}">
                    <img src="{{ $category->getImageUrl() }}" alt="{{ $category->name }}">
                    <span>{{ $category->name }}</span>
                </a>
            @empty
                @emptyResult
            @endforelse

            @if($result->hasPages())
                {!! $result->links() !!}
            @endif
        </div>
    </div>

    @widget('catalog:popular')
@endsection

@section('hideH1', true)
