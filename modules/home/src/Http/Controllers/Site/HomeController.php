<?php

namespace WezomCms\Home\Http\Controllers\Site;

use WezomCms\Core\Http\Controllers\SiteController;

class HomeController extends SiteController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $siteSettings = settings('home.site', []);
        // Seo
        $this->seo()
            ->setPageName(array_get($siteSettings, 'name'))
            ->setTitle(array_get($siteSettings, 'title'))
            ->setDescription(array_get($siteSettings, 'description'))
            ->setH1(array_get($siteSettings, 'h1'))
            ->setSeoText(array_get($siteSettings, 'text'))
            ->metatags()
            ->setKeywords(array_get($siteSettings, 'keywords'));

        return view('cms-home::site.home',[
            'seo_text' => array_get($siteSettings, 'text')
        ]);
    }
}
