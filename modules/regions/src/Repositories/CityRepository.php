<?php

namespace WezomCms\Regions\Repositories;

use WezomCms\Core\Repositories\AbstractRepository;
use WezomCms\Regions\Models\City;

class CityRepository extends AbstractRepository
{
    protected function model()
    {
        return City::class;
    }

    public function getBySelect()
    {
        $data = [];

        $regions = resolve(RegionsRepository::class)->getBySelect();

        $cities = $this->getModelQuery()
            ->published()
            ->with([
                'translations' => function ($query) {
                    $query->orderBy('name');
                }
            ])
            ->orderBy('sort')
            ->get()
            ->toArray();

        foreach ($regions as $id => $name){
            $cityForRegion = [];
            foreach ($cities as $city){
                if($id == $city['region_id']){
                    $cityForRegion[$city['id']] = $city['name'];
                }
            }
            $data[$name] = $cityForRegion;
        }

        return $data;
    }
}
