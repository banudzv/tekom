<?php

namespace WezomCms\Core\Foundation;

use Exception;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use WezomCms\Core\Facades\NotifyMessage;
use WezomCms\Core\Foundation\Notifications\NotifyDriverInterface;

class JsResponse implements Arrayable, Jsonable, Responsable
{
    private $response = [
        'success' => true,
        'reset' => true,
        'reload' => false,
    ];

    private $statusCode = 200;

    private $actions = [];

    /**
     * JsFeedback constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->massAssigment($data);
    }

    /**
     * @param array $data
     * @return JsResponse
     */
    public function massAssigment(array $data): JsResponse
    {
        foreach ($data as $key => $value) {
            $this->set($key, $value);
        }

        return $this;
    }

    /**
     * @param string $key
     * @param $value
     * @return JsResponse
     */
    public function set(string $key, $value): JsResponse
    {
        array_set($this->response, $key, $value);

        return $this;
    }

    /**
     * @param array $data
     * @return JsResponse
     */
    public static function make(array $data = [])
    {
        return new static($data);
    }

    /**
     * @param bool $success
     * @return JsResponse
     */
    public function success(bool $success): JsResponse
    {
        $this->response['success'] = $success;

        if ($success === false) {
            $this->response['reset'] = false;
        }

        return $this;
    }

    /**
     * @param int $code
     * @return $this
     */
    public function code(int $code)
    {
        $this->statusCode = $code;

        return $this;
    }

    /**
     * @param string|NotifyDriverInterface $text
     * @param string $type
     * @param int $timeout
     * @return JsResponse
     */
    public function notification($text, string $type = 'success', int $timeout = 1): JsResponse
    {
        if ($text instanceof NotifyDriverInterface) {
            $notification = $text;
        } else {
            $notification = $this->buildNotification($text, $type, $timeout);
        }

        $this->addAction('popupShow', $this->getNotificationValue($notification, $type), [
            'timeout' => $timeout
        ]);

        return $this;
    }

    /**
     * @param $text
     * @param string $type
     * @param int $timeout
     * @return NotifyDriverInterface|mixed
     */
    protected function buildNotification($text, string $type = 'success', int $timeout = 0)
    {
        return NotifyMessage::$type($text, $timeout);
    }

    public function addAction(string $name, $value = null, ?array $props = null)
    {
        array_push($this->actions, [
            'name' => $name,
            'value' => $value,
            'props' => $props
        ]);

        return $this;
    }

    protected function getNotificationValue(NotifyDriverInterface $notification, string $type = 'success')
    {
        $notification = $notification->toArray();
        return view("cms-ui::partials.popups.$type", [
            'title' => $notification['title'],
            'text' => $notification['text'] ?? null
        ])->render();
    }

    public function replaceHtml($value = null)
    {
        $this->addAction('replaceHtml', $value);
        return $this;
    }

    /**
     * @param $url
     * @return JsResponse
     */
    public function redirect($url): JsResponse
    {
        return $this->set('redirect', $url);
    }

    /**
     * @param bool $reload
     * @return JsResponse
     */
    public function reload($reload = true): JsResponse
    {
        return $this->set('reload', $reload);
    }

    /**
     * @param array $errors
     * @return JsResponse
     */
    public function errors(array $errors = []): JsResponse
    {
        return $this->set('errors', $errors);
    }

    /**
     * @param bool $reset
     * @return JsResponse
     */
    public function reset($reset = true): JsResponse
    {
        return $this->addAction('reset', $reset);
    }

    /**
     * @param array|string $options
     * @return JsResponse
     */
    public function magnific($options): JsResponse
    {
        return $this->set('magnific', $options);
    }

    /**
     * Convert the object to its JSON representation.
     *
     * @param int $options
     * @return string
     */
    public function toJson($options = 0)
    {
        return json_encode($this->toArray(), $options);
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray(): array
    {
        $response = $this->response;
        $response['actions'] = $this->actions;

        return $response;
    }

    /**
     * Create an HTTP response that represents the object.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function toResponse($request)
    {
        return response()->json($this->toArray(), $this->statusCode);
    }

    /**
     * @throws Exception|JsResponseException|mixed
     */
    public function throwException()
    {
        $exception = new JsResponseException();
        $exception->setResponse($this);

        throw $exception;
    }
}
