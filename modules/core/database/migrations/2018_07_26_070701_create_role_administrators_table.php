<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoleAdministratorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_administrators', function (Blueprint $table) {
            $table->unsignedBigInteger('administrator_id');
            $table->unsignedBigInteger('role_id');
            $table->timestamps();

            $table->unique(['administrator_id', 'role_id']);
            $table->foreign('administrator_id')->references('id')->on('administrators')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('role_administrators');
    }
}
