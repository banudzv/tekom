<?php

namespace WezomCms\Reports\Dashboards;

use WezomCms\Core\Foundation\Dashboard\AbstractValueDashboard;
use WezomCms\Reports\Models\Report;

class ReportDashboard extends AbstractValueDashboard
{
    /**
     * @var null|int - cache time in minutes.
     */
    protected $cacheTime = 5;

    /**
     * @var null|string - permission for link
     */
    protected $ability = 'reports.view';

    /**
     * @return int
     */
    public function value(): int
    {
        return Report::count();
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return __('cms-reports::admin.Reports');
    }

    /**
     * @return string
     */
    public function icon(): string
    {
        return 'fa-file';
    }

    /**
     * @return string
     */
    public function iconColorClass(): string
    {
        return 'color-warning';
    }

    /**
     * @return null|string
     */
    public function url(): ?string
    {
        return route('admin.reports.index');
    }
}
