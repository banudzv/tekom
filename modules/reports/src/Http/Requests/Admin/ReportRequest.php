<?php

namespace WezomCms\Reports\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use WezomCms\Core\Http\Requests\ChangeStatus\RequiredIfMessageTrait;
use WezomCms\Core\Traits\LocalizedRequestTrait;

class ReportRequest extends FormRequest
{
	use LocalizedRequestTrait;
	use RequiredIfMessageTrait;

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return $this->localizeRules(
			[
				'name' => 'required|string|max:255',
			],
			[
				'published' => 'nullable',
				'sort' => 'nullable|integer',
				'published_at' => 'required|date',
				'time' => 'required',
				'category_id' => 'required',
                'file' => ['nullable', 'file', 'min:3', 'max:15000'],
			]
		);
	}

	/**
	 * Get custom attributes for validator errors.
	 *
	 * @return array
	 */
	public function attributes()
	{
		return $this->localizeAttributes(
			[
				'name' => __('cms-core::admin.layout.Name'),
			],
			[
				'sort' => __('cms-core::admin.layout.Position'),
				'published' => __('cms-core::admin.layout.Published'),
				'published_at' => __('cms-reports::admin.Published at'),
				'category_id' => __('cms-reports::admin.Categories report'),
                'file' => __('cms-reports::admin.File'),
                'time' => __('cms-reports::admin.Time'),
			]
		);
	}
}
