<?php

namespace WezomCms\Reports\Http\Controllers\Admin;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;
use WezomCms\Core\Settings\AdminLimit;
use WezomCms\Core\Settings\Fields\Textarea;
use WezomCms\Core\Settings\MetaFields\SeoFields;
use WezomCms\Reports\Http\Requests\Admin\ReportRequest;
use WezomCms\Reports\Models\Category;
use WezomCms\Reports\Models\Report;
use WezomCms\Core\Settings\RenderSettings;
use WezomCms\Core\Settings\Tab;
use WezomCms\Core\Traits\SettingControllerTrait;
use WezomCms\Core\Settings\Fields\Image;

class ReportController extends AbstractCRUDController
{
    use SettingControllerTrait;

	/**
	 * Model name.
	 *
	 * @var string
	 */
	protected $model = Report::class;

	/**
	 * Indicates whether to use pagination.
	 *
	 * @var bool
	 */
	protected $paginate = false;

	/**
	 * Base view path name.
	 *
	 * @var string
	 */
	protected $view = 'cms-reports::admin.reports';

	/**
	 * Resource route name.
	 *
	 * @var string
	 */
	protected $routeName = 'admin.reports';

	/**
	 * Form request class name.
	 *
	 * @var string
	 */
	protected $request = ReportRequest::class;

	public function __construct()
	{
		parent::__construct();

	}

	/**
	 * Resource name for breadcrumbs and title.
	 *
	 * @return string
	 */
	protected function title(): string
	{
		return __('cms-reports::admin.Reports');
	}

	/**
	 * @param  Builder  $query
	 * @param  Request  $request
	 */
	protected function selectionIndexResult($query, Request $request)
	{
		$query->orderBy('sort');
	}

    /**
     * @param  Report  $model
     * @param  FormRequest  $request
     * @return array
     */
    protected function fill($model, FormRequest $request): array
    {
        $data = parent::fill($model, $request);

        $data['published_at'] = strtotime($request->get('published_at') . ' ' . $data['time']);
        $data['year'] = last(explode('.', $request->get('published_at')));

        return $data;
    }

    /**
     * @param Report $model
     * @param array $viewData
     * @return array
     */
    protected function createViewData($model, array $viewData): array
    {
        return [
            'categories' => Category::getForSelect(),
        ];
    }

    /**
     * @param Report $model
     * @param array $viewData
     * @return array
     */
    protected function editViewData($model, array $viewData): array
    {
        return [
            'categories' => Category::getForSelect(),
        ];
    }

    protected function settings()
    {
        $subTitle = Textarea::make()
            ->setKey('sub_title')
            ->setName(__('cms-reports::admin.Sub title'))
            ->setIsMultilingual()
            ->setRows(3)
            ->setSort(5);

        $result = [
            SeoFields::make('Reports', [$subTitle]),
            AdminLimit::make()
        ];

        $tabs = new RenderSettings(
            new Tab('page', __('cms-core::admin.tabs.site banner'), 1, 'fa-folder-o')
        );

        $result[] = Image::make($tabs)
            ->setSettings([
                'images' => [
                    'directory' => 'settings',
                    'default' => 'medium', // For admin image preview
                    'sizes' => [
                        'big' => [
                            'width' => 1920,
                            'height' => 700,
                            'mode' => 'resize',
                        ],
                    ],
                ],
            ])
            ->setName(__('cms-reports::admin.image-size', ['width' => 1920, 'height' => 700]))
            ->setSort(1)
            ->setKey('image')
            ->setRules('nullable|file');

        return $result;
    }
}

