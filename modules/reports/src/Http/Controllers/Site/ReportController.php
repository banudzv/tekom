<?php

namespace WezomCms\Reports\Http\Controllers\Site;

use Illuminate\Database\Eloquent\Collection;
use WezomCms\Core\Http\Controllers\SiteController;
use WezomCms\Core\Traits\ImageFromSettings;
use WezomCms\Reports\Helpers\ReportQueryParams;
use WezomCms\Reports\Models\Category;
use WezomCms\Reports\Repositories\CategoryRepository;
use WezomCms\Reports\Repositories\ReportRepository;

class ReportController extends SiteController
{
    use ImageFromSettings;

    private $settings;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var ReportRepository
     */
    private $reportRepository;

    private $year;
    private $category_slug;
    private $sub_category_slug;

    public function __construct(
        CategoryRepository $categoryRepository,
        ReportRepository $reportRepository
    )
    {
        $this->settings = settings('report.site', []);
        $this->categoryRepository = $categoryRepository;
        $this->reportRepository = $reportRepository;

        $this->year = request(ReportQueryParams::YEAR);
        $this->category_slug = request(ReportQueryParams::CATEGORY);
        $this->sub_category_slug = request(ReportQueryParams::SUB_CATEGORY);
    }

    public function index()
    {
        $pageName = array_get($this->settings, 'name');

        // Breadcrumbs
        $this->addBreadcrumb($pageName, route('reports'));

        $mainCategories = $this->categoryRepository->getMainCategory();

        $category_slug = $this->category_slug ? $this->category_slug : key($mainCategories);
        /** @var $subCategories Category[]*/
        $subCategories = null;
        $reports = null;
        if($category_slug){

            $subCategories = resolve(CategoryRepository::class)->getSubCategory($category_slug);

            //отчеты
            if($subCategories->isNotEmpty()){
                if($this->sub_category_slug){
                    $reports = resolve(CategoryRepository::class)
                        ->getBySlug($this->sub_category_slug)
                        ->load(['reports' => function($q){
                            if($this->year){
                                $q->where('year', $this->year);
                            }
                        }]);
                } else {
                    $reports = $subCategories->load(['reports' => function($q){
                        if($this->year){
                            $q->where('year', $this->year);
                        }
                    }]);
                }
            } else {
                $reports = resolve(CategoryRepository::class)
                    ->getBySlug($category_slug)
                    ->load(['reports' => function($q){
                        if($this->year){
                            $q->where('year', $this->year);
                        }
                    }]);
            }
        }

        // SEO
        $this->seo()
            ->setCanonical(route('reports'))
            ->setTitle(array_get($this->settings, 'title'))
            ->setPageName($pageName)
            ->setH1(array_get($this->settings, 'h1'))
            ->setDescription(array_get($this->settings, 'description'))
            ->metatags()
            ->setKeywords(array_get($this->settings, 'keywords'));

        return view('cms-reports::site.reports.index', [
            'banner' => $this->getBannerUrl('report.page'),
            'subTitle' => array_get($this->settings, 'sub_title'),
            'mainCategories' => $mainCategories,
            'years' => $this->reportRepository->getYears(),
            'subCategories' => $subCategories,
            'reports' => $reports,
            'selectedCategory' => $category_slug,
            'selectedSubCategory' => $this->sub_category_slug,
            'selectedYear' => $this->year
        ]);
    }

    public function category($slug)
    {
        $year = null;
        if(request('year')){
            $year = request('year');
        }

        $pageName = array_get($this->settings, 'name');

        // Breadcrumbs
        $this->addBreadcrumb($pageName, route('reports'));

        $categories = $this->categoryRepository->getByFrontForSelect();

        $category = resolve(CategoryRepository::class)->getBySlugForFront($slug,[
            'translations',
            'children',
            'children.reports' => function($q) use ($year){
                if($year){
                    $q->where('year', $year);
                }
            },
            'reports' => function($q) use ($year){
                if($year){
                    $q->where('year', $year);
                }
            }
        ]);

        // SEO
        $this->seo()
            ->setCanonical(route('reports'))
            ->setTitle(array_get($this->settings, 'title'))
            ->setPageName($pageName)
            ->setH1(array_get($this->settings, 'h1'))
            ->setDescription(array_get($this->settings, 'description'))
            ->metatags()
            ->setKeywords(array_get($this->settings, 'keywords'));

        return view('cms-reports::site.reports.index', [
            'banner' => $this->getBannerUrl('articles.page'),
            'subTitle' => array_get($this->settings, 'sub_title'),
            'categories' => $categories,
            'category' => $category,
            'years' => $this->reportRepository->getYears()
        ]);
    }
}
