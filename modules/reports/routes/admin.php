<?php

use WezomCms\Reports\Http\Controllers\Admin;

Route::adminResource('reports', Admin\ReportController::class)->settings();
Route::adminResource('report-categories', Admin\CategoryController::class);
