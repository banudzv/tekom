@php
    /**
     * @var $models \Illuminate\Database\Eloquent\Collection|\WezomCms\Reports\Models\Category[]
     */
@endphp
<div class="reports-banner">
    <div class="reports-banner__content">
        <div class="reports-banner__title">@lang('cms-reports::site.report title')</div>
        <div class="reports-banner__description">@lang('cms-reports::site.report description')</div>
        <div class="reports-banner__links">
            @foreach($models ?? [] as $model)
                <a class="reports-banner__link" href="{{ $model->getFrontUrl() }}">{{ $model->name }}</a>
            @endforeach
        </div>
    </div>
    <div class="_flex _justify-center">
        @widget('ui:button', [
                    'component' => 'a',
                    'text' => __('cms-reports::site.report button'),
                    'classes' => '_text-center',
                    'attrs' => [
                        'href' => route('reports')
                        ],
                    'modificators' => ['color-accent', 'size-def', 'uppercase', 'ls-def', 'bold']])
    </div>
</div>
