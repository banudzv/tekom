@extends('cms-ui::layouts.main')

@php
use WezomCms\Reports\Helpers\ReportQueryParams;

    /**
     * @var $banner string|null
     * @var $subTitle string|null
     * @var $categories array
     * @var $years array|null
     * @var $mainCategories \WezomCms\Reports\Models\Category | null
     * @var $subCategories \WezomCms\Reports\Models\Category | null
     */
@endphp

@section('content')
    @widget('ui:heading-section', [
    'bannerImage' => $banner,
    'title' => SEO::getH1(),
    'text' => $subTitle,
    'classes' => 'heading-section--reports'
    ])
    <div class="section">
        <div class="nav-block nav-block--reports">
            <div class="container container--theme-default">
                <div class="nav-block__inner">
                    <div class="nav-block__group">
                        <div class="nav-block__text">
                            @lang('cms-reports::site.Choice category')
                        </div>
                        <div class="nav-block__select">
                            @component('cms-ui::components.form.select2',
                                 [
                                     'selectOptions' => [
                                         'Factory' => 'SelectWithLinks'
                                    ],
                                    'class' => 'form-item--dark',
                                    'options' => $mainCategories,
                                    'name' => uniqid('select2'),
                                    'attrs' => ['name' => 'district_id']
                                ]
                            )
                                @foreach($mainCategories ?? [] as $slug => $name)
                                    <option
                                        value="{{ $loop->iteration }}"
                                        {{ ($selectedCategory == $slug) ? 'selected' : null }}
                                        data-url-to-go="{{ route('reports', [ ReportQueryParams::CATEGORY => $slug]) }}">
                                        {{ $name }}
                                    </option>
                                @endforeach
                            @endcomponent
                        </div>
                    </div>
                    @if($subCategories && $subCategories->isNotEmpty())
                        <div class="nav-block__group">
                            <div class="nav-block__text">
                                @lang('cms-reports::site.Choice sub-category')
                            </div>
                            <div class="nav-block__select">
                                @component('cms-ui::components.form.select2',
                                     [
                                         'selectOptions' => [
                                             'Factory' => 'SelectWithLinks'
                                        ],
                                        'class' => 'form-item--dark',
                                        'options' => $subCategories,
                                        'name' => uniqid('select2'),
                                        'attrs' => ['name' => 'district_id']
                                    ]
                                )
                                    @foreach($subCategories ?? [] as $sub_category)
                                        <option
                                            value="{{ $loop->iteration }}"
                                            {{ ($selectedSubCategory == $sub_category->slug) ? 'selected' : null }}
                                            data-url-to-go="{{ route('reports', [
                                        ReportQueryParams::CATEGORY => $sub_category->parent->slug,
                                        ReportQueryParams::SUB_CATEGORY => $sub_category->slug
                                    ]) }}">
                                            {{ $sub_category->name }}
                                        </option>
                                    @endforeach
                                @endcomponent
                            </div>
                        </div>
                    @endif
                    @if(!\WezomAgency\Browserizr::detect()->isDesktop())
                        <div class="nav-block__group _df:hide">
                            <div class="nav-block__text">
                                @lang('cms-reports::site.Choice year')
                            </div>
                            <div class="nav-block__select">
                                @component('cms-ui::components.form.select2',
                                     [
                                         'selectOptions' => [
                                             'Factory' => 'SelectWithLinks'
                                        ],
                                        'class' => 'form-item--dark',
                                        'options' => $subCategories,
                                        'name' => uniqid('select2'),
                                        'attrs' => ['name' => 'district_id']
                                    ]
                                )
                                    @foreach($years as $year)
                                        @php
                                            $query = [];
                                            if(request(ReportQueryParams::CATEGORY)){
                                               $query[ReportQueryParams::CATEGORY] = request(ReportQueryParams::CATEGORY);
                                            }
                                            if(request(ReportQueryParams::SUB_CATEGORY)){
                                               $query[ReportQueryParams::SUB_CATEGORY] = request(ReportQueryParams::SUB_CATEGORY);
                                            }
                                            $query[ReportQueryParams::YEAR] = $year;
                                        @endphp
                                        {{--todo активному элементу добавлять класс is-active --}}
                                        <option
                                            value="{{ $loop->iteration }}"
                                            class="{{ ($selectedYear == $year) ? 'is-active' : null }}"
                                            data-url-to-go="{{ route('reports', $query) }}">
                                            {{ $year }}
                                        </option>
                                    @endforeach
                                @endcomponent
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="section section--reports section--bg-grey">
        <div class="container container--theme-default">
            @if($reports)
                @if(isset($years))
                    <div class="_flex _flex-wrap _mb-xl _df:show">
                        @foreach($years as $year)
                            @php
                                $query = [];
                                if(request(ReportQueryParams::CATEGORY)){
                                   $query[ReportQueryParams::CATEGORY] = request(ReportQueryParams::CATEGORY);
                                }
                                if(request(ReportQueryParams::SUB_CATEGORY)){
                                   $query[ReportQueryParams::SUB_CATEGORY] = request(ReportQueryParams::SUB_CATEGORY);
                                }
                                $query[ReportQueryParams::YEAR] = $year;
                            @endphp
                            {{--todo активному элементу добавлять класс is-active --}}
                            <a class="button button--button button--link button--size-sm _mr-xs {{ ($selectedYear == $year) ? 'is-active' : null }}"
                               href="{{route('reports', $query)}}">
                                {{ $year }}
                            </a>
                        @endforeach
                    </div>
                @endif
                @foreach($reports ?? [] as $category)
                    <div class="{{ !$loop->last ? '_mb-df' : null }}">
                        @if($category->reports->isNotEmpty())
                            <div class="title title--size-h4">{{ $category->name }}</div>
                        @endif
                        <div>
                            @foreach($category->reports as $report)
                                <div class="report report--theme-date">
                                    <div class="report__name">{{ $report->name }}</div>

                                    @if($report->fileExists())
                                        <div class="_ml-auto">
                                            <a class="report__icon" href="{{ url($report->getFileUrl()) }}" download target="_blank" >
                                                @svg('pdf-file', 24, 30)
                                            </a>
                                        </div>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
    @include('cms-ui::widgets.feedback-form')
@endsection

