<div class="row">
	<div class="col-lg-7">
		<div class="card mb-3">
			<div class="card-header">
				<h5 class="py-2"><strong>@lang('cms-core::admin.layout.Main data')</strong></h5>
			</div>
            <div class="card-body">
                @langTabs
                <div class="form-group">
                    {!! Form::label($locale . '[name]', __('cms-core::admin.layout.Name')) !!}
                    {!! Form::text($locale . '[name]', old($locale . '.name', $obj->translateOrNew($locale)->name)) !!}
                </div>
                @endLangTabs
            </div>
		</div>
	</div>

	<div class="col-lg-5">
		<div class="card">
            <div class="card-body">
                <div class="form-group">
                    {!! Form::label(str_slug('published'), __('cms-core::admin.layout.Published')) !!}
                    {!! Form::status('published', old('published', $obj->exists ? $obj->published : true))  !!}
                </div>
                <div class="form-group">
                    {!! Form::label('published_at', __('cms-reports::admin.Published at')) !!}
                    {!! Form::text('published_at', old('published_at', $obj->published_at ? $obj->published_at->format('d.m.Y') : null),
                        [
                            'class' => 'js-datepicker',
                            'placeholder' => __('cms-reports::admin.Published at')
                            ])
                    !!}
                </div>
                <div class="form-group">
                    {!! Form::label('time', __('cms-reports::admin.Time')) !!}
                    {!! Form::text('time', old('published_at', $obj->published_at ? $obj->published_at->format('HH:ii') : \Illuminate\Support\Carbon::now('Europe/Kiev')->format('H:i')),
                        [
                            'class' => 'js-datetime-picker',
                        ])
                    !!}
                </div>
                <div class="form-group">
                    {!! Form::label('category_id', __('cms-reports::admin.Categories report')) !!}
                    <select name="category_id" id="category_id" class="form-control js-select2">
                        <option value="">@lang('cms-core::admin.layout.Not set')</option>
                        @foreach($categories as $key => $category)
                            <option value="{{ $key }}" {{ $key == old('category_id', $obj->category_id ?: request()->get('category_id')) ? 'selected': null }}
                                {{ $category['disabled'] ?? false ? 'disabled' : null }}>{!! $category['name'] !!}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    {!! Form::label('file', __('cms-reports::admin.File')) !!}
                    {!! Form::fileUploader('file', $obj, route($routeName . '.delete-file', [$obj->id, 'file']), ['accept' => '.pdf']) !!}
                </div>
            </div>
		</div>
	</div>
</div>
