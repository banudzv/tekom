## Installation

#### Use console
```
composer require wezom-cms/seo-redirects
```
#### Or edit composer.json
```
"require": {
    ...
    "wezom-cms/seo-redirects": "^7.0"
}
```
#### Update dependencies:
```
composer update
```
#### Run migrations
```
php artisan migrate
```

## Publish
#### Views
```
php artisan vendor:publish --provider="WezomCms\SeoRedirects\SeoRedirectsServiceProvider" --tag="views"
```
#### Lang
```
php artisan vendor:publish --provider="WezomCms\SeoRedirects\SeoRedirectsServiceProvider" --tag="lang"
```
## Issues
If your get error 
```
file_exists(): open_basedir restriction in effect. File(/tmp) laravel excel
```
Your need publish LaravelExcel config to config/excel.php & set 'local_path'  => storage_path(), 
