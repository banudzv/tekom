@php
	/**
	* @var $models \WezomCms\Benefits\Models\Benefits[]
	* @var $settings array
	*/
@endphp

<div class="section section--benefits js-import lozad" data-background-image="{{ asset('/static/images/benefits-bg.jpg') }}">
    <div class="container">
        <div class="benefits js-import" data-inview>
            <div class="_grid _justify-center _df:justify-between _spacer _spacer--df">
                <div class="_cell _cell--12 _sm:cell--10 _md:cell--9 _df:cell--7 _xl:cell--5 _flex _flex-column _df:items-start _items-center">
                    @if(isset($settings['name']))
                        <div class="benefits__title">{!! $settings['name'] ?? null !!}</div>
                    @endif
                    @if(isset($settings['text']))
                        <div class="benefits__description">{!! $settings['text'] ?? null !!}</div>
                    @endif
                </div>
                <div class="_cell _cell--12 _sm:cell--10 _md:cell--9 _df:cell--5 _xl:cell--4">
                    @foreach($models ?? [] as $benefit)
                        <div class="benefit">
                            <div class="benefit__content">
                                <div class="benefit__title" style="animation: scale-from-opacity 0.5s ease {{ $loop->index + 0.5 }}s forwards">{!! $benefit->name !!}</div>
                                <div class="benefit__icon-wrapper">
                                    <div class="benefit__decor benefit__decor--prev" style="animation: benefits-expand 0.5s ease forwards {{ $loop->index === 0 ? 0 : $loop->index }}s"></div>
                                    <div class="benefit__icon" style="animation: fade-in 0.5s ease {{ $loop->index + 0.5 }}s forwards">
                                        @svg($benefit->icon, null, null)
                                    </div>
                                    @if($loop->last)
                                        <div class="benefit__decor benefit__decor--next" style="animation: benefits-expand 0.5s ease forwards {{ $loop->index === 0 ? 0 : $loop->index + 1 }}s"></div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
