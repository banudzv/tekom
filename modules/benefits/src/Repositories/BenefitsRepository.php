<?php

namespace WezomCms\Benefits\Repositories;

use WezomCms\Benefits\Models\Benefits;
use WezomCms\Core\Repositories\AbstractRepository;

class BenefitsRepository extends AbstractRepository
{
	protected function model()
	{
		return Benefits::class;
	}

	public function getByFront()
	{
		return $this->getModelQuery()
			->published()
			->with([
				'translations'
			])
			->where('for_main', true)
			->orderBy('sort')
			->get();
	}
//
//	public function getBenefitsBySelect()
//	{
//		return $this->query()
//			->with([
//				'translations' => function ($q) {
//					$q->orderBy('name');
//				}
//			])
//			->get()->pluck('name', 'id')->toArray();
//	}

}
