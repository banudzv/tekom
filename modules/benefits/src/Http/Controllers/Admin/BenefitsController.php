<?php

namespace WezomCms\Benefits\Http\Controllers\Admin;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;
use WezomCms\Benefits\Http\Requests\Admin\BenefitsRequest;
use WezomCms\Benefits\Models\Benefits;
use WezomCms\Core\Settings\Fields\AbstractField;
use WezomCms\Core\Settings\Fields\Wysiwyg;
use WezomCms\Core\Settings\MetaFields\Title;
use WezomCms\Core\Settings\MultilingualGroup;
use WezomCms\Core\Settings\RenderSettings;
use WezomCms\Core\Settings\Tab;
use WezomCms\Core\Traits\SettingControllerTrait;

class BenefitsController extends AbstractCRUDController
{
    use SettingControllerTrait;

	/**
	 * Model name.
	 *
	 * @var string
	 */
	protected $model = Benefits::class;

	/**
	 * Indicates whether to use pagination.
	 *
	 * @var bool
	 */
	protected $paginate = false;

	/**
	 * Base view path name.
	 *
	 * @var string
	 */
	protected $view = 'cms-benefits::admin.benefits';

	/**
	 * Resource route name.
	 *
	 * @var string
	 */
	protected $routeName = 'admin.benefits';

	/**
	 * Form request class name.
	 *
	 * @var string
	 */
	protected $request = BenefitsRequest::class;

	public function __construct()
	{
		parent::__construct();

	}

	/**
	 * Resource name for breadcrumbs and title.
	 *
	 * @return string
	 */
	protected function title(): string
	{
		return __('cms-benefits::admin.Benefits');
	}

	/**
	 * @param  Builder  $query
	 * @param  Request  $request
	 */
	protected function selectionIndexResult($query, Request $request)
	{
		$query->orderBy('sort');
	}

    /**
     * @return array|AbstractField[]|MultilingualGroup[]
     * @throws \Exception
     */
    protected function settings(): array
    {
        $result = [];

        $tabs = new RenderSettings(
            new Tab('page', __('cms-benefits::admin.module site data'), 1, 'fa-folder-o')
        );

        $items = [
            Title::make()
                ->setName(__('cms-core::admin.layout.Name'))
                ->setSort(1)
                ->setIsMultilingual()
                ->setKey('name')
                ->setRules('nullable|string'),
            Wysiwyg::make()
                ->setName(__('cms-core::admin.layout.Text'))
                ->setSort(2)
                ->setIsMultilingual()
                ->setKey('text')
                ->setRules('nullable|string')
        ];

        $result[] = new MultilingualGroup($tabs, $items);

        return $result;
    }
}

