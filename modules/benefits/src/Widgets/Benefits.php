<?php

namespace WezomCms\Benefits\Widgets;

use WezomCms\Benefits\Repositories\BenefitsRepository;
use WezomCms\Core\Foundation\Widgets\AbstractWidget;
use WezomCms\Counter\Models\Counter;
use WezomCms\Counter\Repositories\CounterRepository;

class Benefits extends AbstractWidget
{
    /**
     * View name.
     *
     * @var string|null
     */
    protected $view = 'cms-benefits::site.widgets.benefits';

	/**
	 * @return array|null
	 */
    public function execute(): ?array
	{
        /** @var $models \WezomCms\Benefits\Models\Benefits[] */
        $models = app(BenefitsRepository::class)->getByFront();

        if ($models->isEmpty()) {
            return null;
        }

        $settings = settings('benefits.page', []);

        return compact('models', 'settings');
	}
}
