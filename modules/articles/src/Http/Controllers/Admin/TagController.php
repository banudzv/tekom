<?php

namespace WezomCms\Articles\Http\Controllers\Admin;

use WezomCms\Articles\Http\Requests\Admin\TagRequest;
use WezomCms\Articles\Models\ArticleTag;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;
use WezomCms\Core\Traits\AjaxResponseStatusTrait;

class TagController extends AbstractCRUDController
{
    use AjaxResponseStatusTrait;

    /**
     * Model name.
     *
     * @var string
     */
    protected $model = ArticleTag::class;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-articles::admin.tags';

    /**
     * Resource route name.
     *
     * @var string
     */
    protected $routeName = 'admin.article-tags';

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = TagRequest::class;

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-articles::admin.Tags');
    }
}
