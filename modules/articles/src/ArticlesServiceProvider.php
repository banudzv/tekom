<?php

namespace WezomCms\Articles;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use WezomCms\Articles\Models\Article;
use WezomCms\Articles\Models\ArticleGroup;
use WezomCms\Articles\Repositories\ArticleGroupRepository;
use WezomCms\Core\BaseServiceProvider;
use WezomCms\Core\Contracts\PermissionsContainerInterface;
use WezomCms\Core\Contracts\SitemapXmlGeneratorInterface;
use WezomCms\Core\Traits\SidebarMenuGroupsTrait;
use WezomCms\Services\Models\Service;
use WezomCms\Services\Models\ServiceGroup;
use WezomCms\Services\Repositories\ServiceGroupRepository;

class ArticlesServiceProvider extends BaseServiceProvider
{
    use SidebarMenuGroupsTrait;

    /**
     * All module widgets.
     *
     * @var array|string|null
     */
    protected $widgets = 'cms.articles.articles.widgets';

    /**
     * Dashboard widgets.
     *
     * @var array|string|null
     */
    protected $dashboard = 'cms.articles.articles.dashboards';

    /**
     * @param  PermissionsContainerInterface  $permissions
     */
    public function permissions(PermissionsContainerInterface $permissions)
    {
        $permissions->add('articles', __('cms-articles::admin.Articles'))->withEditSettings();

        if (config('cms.articles.articles.use_groups')) {
            $permissions->add('article-groups', __('cms-articles::admin.Article groups'))->withEditSettings();
        }

        if (config('cms.articles.articles.use_tags')) {
            $permissions->add('article-tags', __('cms-articles::admin.Tags'));
        }
    }

    /**
     * Register all admin sidebar menu links.
     */
    public function adminMenu()
    {
        $group = $this->contentGroup()
            ->add(__('cms-articles::admin.Articles'), route('admin.articles.index'))
            ->data('icon', 'fa-briefcase')
            ->nickname('articles');

        if (config('cms.articles.articles.use_groups')) {
            $group->add(__('cms-articles::admin.Articles'), route('admin.articles.index'))
                ->data('icon', 'fa-list')
                ->data('permission', 'articles.view')
                ->data('position', 1);

            $group->add(__('cms-articles::admin.Groups'), route('admin.article-groups.index'))
                ->data('icon', 'fa-th-large')
                ->data('permission', 'article-groups.view')
                ->data('position', 2);

            $group->add(__('cms-articles::admin.Tags'), route('admin.article-tags.index'))
                ->data('icon', 'fa-tags')
                ->data('permission', 'article-tags.view')
                ->data('position', 3);
        } else {
            $group->data('permission', 'articles.view');
        }
    }

    /**
     * @return array
     */
    public function sitemap()
    {
        $data = [];
        $articles = \App::make(ArticleGroupRepository::class)->getByFrontForMain();

        foreach($articles ?? [] as $k => $group){
            /** @var $group ArticleGroup */
            $data[$k]['id'] = $group->id;
            $data[$k]['sort'] = -40;
            $data[$k]['name'] = $group->name;
            $data[$k]['url'] = $group->getFrontUrl();

            foreach($group->articles ?? [] as $key => $article){
                /** @var $article Article */
                $data[$k]['parent'][$key]['id'] = $article->id;
                $data[$k]['parent'][$key]['sort'] = $article->id;
                $data[$k]['parent'][$key]['name'] = $article->name;
                $data[$k]['parent'][$key]['url'] = $article->getFrontUrl();
            }
        }

        return $data;
    }

    /**
     * @param  SitemapXmlGeneratorInterface  $sitemap
     * @throws \ErrorException
     */
    public function sitemapXml(SitemapXmlGeneratorInterface $sitemap)
    {
        $sitemap->addLocalizedRoute('blog');
        $articles = \App::make(ArticleGroupRepository::class)->getByFrontForMain();
        foreach($articles ?? [] as $k => $group){
            /** @var $group ArticleGroup */
            $sitemap->addLocalizedRoute('article-groups', $group->slug);
            foreach($group->articles ?? [] as $ke => $article){
                /** @var $article Article */
                $sitemap->addLocalizedRoute('articles.inner', $article->slug);
            }
        }
    }
}
