<?php

namespace WezomCms\Articles\Repositories;

use WezomCms\Articles\Models\ArticleGroup;
use WezomCms\Core\Repositories\AbstractRepository;

class ArticleGroupRepository extends AbstractRepository
{
    protected function model()
    {
        return ArticleGroup::class;
    }

    public function getByFrontForMain()
    {
        return $this->getModelQuery()
            ->published()
            ->with([
                'translations',
                'articles' => function($query){
                    $query->published()
                        ->where('for_main', true)
                        ->orderByDesc('published_at')
                        ->latest('id')
                    ;
                }
            ])
            ->orderBy('sort')
            ->get();
    }
}
