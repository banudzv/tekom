<?php

Route::namespace('WezomCms\\Articles\\Http\\Controllers\\Site')
    ->group(function () {

        Route::get('blog', 'ArticlesController@index')->name('blog');
        Route::get('blog/{slug}', 'ArticleGroupsController@inner')->name('article-groups');
        Route::get('blog/tag/{slug}', 'ArticlesController@tag')->name('articles-tag');
        Route::get('blog/article/{slug}', 'ArticlesController@inner')->name('articles.inner');

//        Route::get('article-groups', 'ArticleGroupsController@index')->name('article-groups');
        Route::get('articles', 'ArticlesController@index')->name('articles');
    });
