<?php

use WezomCms\Articles\Dashboards;
use WezomCms\Articles\Widgets;

return [
    'use_groups' => true,
    'use_tags' => true,
    'images' => [
        'directory' => 'articles',
        'default' => 'big', // For admin image preview
        'sizes' => [
            'big' => [
                'width' => 1920,
                'height' => 700,
                'mode' => 'resize',
            ]
        ],
    ],
    'images_preview' => [
        'directory' => 'articles',
        'default' => 'medium', // For admin image preview
        'sizes' => [
            'medium' => [
                'width' => 237,
                'height' => 237,
                'mode' => 'resize',
            ]
        ],
    ],
    'sitemap' => [
        'articles' => true, // Enable/disable render links to all published articles in sitemap page.
    ],
    'widgets' => [
        'articles:latest' => Widgets\Latest::class,
        'articles:blog' => Widgets\Blog::class,
        'articles:card' => Widgets\Card::class,
    ],
    'dashboards' => [
        Dashboards\ArticlesDashboard::class,
    ]
];
