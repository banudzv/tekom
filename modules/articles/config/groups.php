<?php

return [
    'images' => [
        'directory' => 'article_groups',
        'default' => 'medium', // For admin image preview
        'sizes' => [
            'medium' => [
                'width' => 430,
                'height' => 276,
                'mode' => 'fit'
            ],
        ],
    ],
];
