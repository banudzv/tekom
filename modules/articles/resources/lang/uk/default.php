<?php

use WezomCms\Core\Enums\TranslationSide;

return [
    TranslationSide::SITE => [
        'Articles' => 'Статті',
        'Previous article' => 'Попередня стаття',
        'Next article' => 'Наступна стаття',
        'Learn more' => 'Дізнатися більше',
        'Read more' => 'Читати далі',
        'Back to the articles' => 'Назад до всіх статей',
    ],
];
