<?php

use WezomCms\Core\Enums\TranslationSide;

return [
    TranslationSide::ADMIN => [
        'Articles' => 'Статьи',
        'List' => 'Список',
        'Add' => 'Добавить',
        'Articles list' => 'Список статей',
        'Name' => 'Название',
        'Published at' => 'Опубликовано',
        'Image' => 'Изображение',
        'Cover' => 'Обложка',
        'Text' => 'Текст',
        'Site articles limit at page' => 'Количество отображаемых статей на странице',
        'Group' => 'Группа',
        'Groups' => 'Группы',
        'Article groups' => 'Группы статей',
        'Groups list' => 'Список групп',
        'Groups slug' => 'Алиас групп',
        'Short description' => 'Краткое описание',
        'Site article groups limit at page' => 'Колличество отображаемых групп статей на странице',
        'For main' => 'Для главной',
        'Tag' => 'Тег',
        'Tags' => 'Теги',
        'image-size' => 'Рекомендуемый размер :width x :height px',
        'Sub title' => 'Подзаголовок',
        'Image preview' => 'Превью',
        'Search title' => 'Заголовок для стр. поиска',
    ],
    TranslationSide::SITE => [
        'Articles' => 'Статьи',
        'Previous article' => 'Предыдущая новость',
        'Next article' => 'Следующая новость',
        'Learn more' => 'Узнать больше',
        'Read more' => 'Читать дальше',
        'Back to the articles' => 'Назад к списку статей',
        'Blog' => 'Блог',
        'Other articles' => 'Вам может быть интерестно',
        'More links article' => 'Все :name',
        'Empty result' => 'К сожалению, в данной категории ничего нет',
    ],
];
