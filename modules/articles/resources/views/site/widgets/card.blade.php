@php
    /**
     * @var $article object
     */
    $large = $large ?? false;
    $classes = $classes ?? null;
@endphp

@if($large)
    <div class="blog-card blog-card--size-lg {{ $classes }}">
        <a href="{{ $article->getFrontUrl() }}" class="blog-card__main">
            <span class="blog-card__content">
                <span class="blog-card__title">
                    {{ $article->name }}
                </span>
                <span class="blog-card__published">
                    @svg('clock')
                    <span>{{ $article->publishedHuman }}</span>
                </span>
            </span>
        </a>
        <span class="blog-card__image">
            <img class="js-import lozad"
                 src="{{ url('/images/empty.gif') }}"
                 data-src="{{ $article->getPreview() }}"
                 alt="">
        </span>
        <div class="blog-card__tags">
            @foreach($article->tags ?? [] as $tag)
                <a title="{{ $tag->name }}"
                   class="blog-card__tag"
                   href="{{ $tag->getFrontUrl() }}">
                    {{ $tag->name }}
                </a>
            @endforeach
        </div>
    </div>
@else
    <div class="blog-card blog-card--size-sm {{ $classes }}">
        <div class="blog-card__main">
            <div class="blog-card__image-container">
                <a href="{{ $article->getFrontUrl() }}" class="blog-card__image">
                    <img class="js-import lozad"
                         src="{{ url('/images/empty.gif') }}"
                         data-src="{{ $article->getPreview() }}"
                         alt="">
                </a>
                <div class="blog-card__tags">
                    @foreach($article->tags ?? [] as $tag)
                        <a title="{{ $tag->name }}"
                           class="blog-card__tag"
                           href="{{ $tag->getFrontUrl() }}">
                            {{ $tag->name }}
                        </a>
                    @endforeach
                </div>
            </div>
            <div class="blog-card__content">
                <a class="blog-card__title js-import"
                   title="{{ $article->name }}"
                   data-truncate
                   href="{{ $article->getFrontUrl() }}">
                    {{ $article->name }}
                </a>
                <div class="blog-card__published">
                    @svg('clock')
                    <span>
                        {{ $article->publishedHuman }}
                    </span>
                </div>
            </div>
        </div>
    </div>
@endif
