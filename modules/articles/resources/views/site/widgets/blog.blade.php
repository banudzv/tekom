@php
    /**
     * @var $models []
     */
@endphp

<div class="section section--blog-widget">
    <div class="container">
        <div class="title title--size-h2 _text-center _mb-none">@lang('cms-articles::site.Blog')</div>
        <div class="tabs tabs--blog tabs--slider">
            <div class="_grid _mb-xl _mt-md">
            @foreach($models ?? [] as $group)
                    @widget('ui:button', [
                    'component' => 'button',
                    'attrs' => ['data-wstabs-ns' => 'carouselBlogs', 'data-wstabs-button' => $group['name']],
                    'text' => $group['name'],
                    'modificators' => ['button button--tab button--size-sm _mr-xs']
                    ])
            @endforeach
            </div>
            <div class="tabs__body">
                @foreach($models ?? [] as $group)
                    <div class="wstabs-block" data-wstabs-ns="carouselBlogs" data-wstabs-block="{{$group['name']}}">
                        <a href="{{ $group['linkMore'] }}" class="link link--watch-all">
                             <span class="link__text">
                                @lang('cms-articles::site.More links article', ['name' => $group['name']])
                            </span>
                            <span class="link__icon">
                               @svg('arrow-right-type-2', 16, 16, 'link__icon')
                            </span>
                        </a>
                        <div class="tabs__carousel js-import" data-slick-carousel='{!! r2d2()->attrJsonEncode([
                            'factory' => 'InfoSlickCarousel',
                            'props' => [
                                'countOfId' => ''
                                ]]) !!}'>
                                <div class="tabs__carousel-slider" data-slick-carousel-list>
                                    @foreach($group['articles'] ?? [] as $article)
                                        @include('cms-ui::widgets.blog-item',[
                                          'img' => $article['image'],
                                          'text' => $article['name'],
                                          'date' => $article['published_at'],
                                          'href' =>  $article['link']
                                     ])
                                    @endforeach
                                </div>
                                <div class="tabs__carousel-arrows">
                                    <div class="carousel-arrow" data-slick-carousel-prev-arrow>
                                        @svg('arrow-left', 22, 23, '')
                                    </div>
                                    <div class="carousel-arrow" data-slick-carousel-next-arrow>
                                        @svg('arrow-right', 22, 23, '')
                                    </div>
                                </div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
</div>
</div>
