<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAboutInformationTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_information_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('information_id');
            $table->string('locale')->index();
            $table->string('name')->nullable();
            $table->text('text')->nullable();

            $table->unique(['information_id', 'locale']);
            $table->foreign('information_id')
                ->references('id')
                ->on('about_informations')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_information_translations');
    }
}
