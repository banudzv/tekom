<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAboutRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_rules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('published')->default(true);
            $table->unsignedBigInteger('rule_group_id')->nullable();
            $table->tinyInteger('sort')->default(0);
            $table->string('file')->nullable();
            $table->timestamps();


            $table->foreign('rule_group_id')
                ->references('id')
                ->on('about_rule_groups')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_rules');
    }
}
