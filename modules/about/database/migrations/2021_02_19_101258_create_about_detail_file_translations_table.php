<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAboutDetailFileTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_detail_file_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('detail_file_id');
            $table->string('locale')->index();
            $table->string('name')->nullable();

            $table->unique(['detail_file_id', 'locale']);
            $table->foreign('detail_file_id')
                ->references('id')
                ->on('about_detail_files')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_detail_file_translations');
    }
}
