<?php

Route::namespace('WezomCms\\About\\Http\\Controllers\\Site')
    ->group(function () {
        Route::get('about/about-us', 'AboutController@index')->name('about.about-us');
        Route::get('about/information', 'InformationController@index')->name('about.information');
        Route::get('about/agent', 'AgentsController@index')->name('about.agents');
        Route::get('about/details', 'DetailsController@index')->name('about.details');
        Route::get('about/rule-insurance', 'RuleController@index')->name('about.rule');
        Route::get('about/insurance-rate', 'InsuranceRateController@index')->name('about.insurance-rate');
        Route::get('about/licenses', 'LicensesController@index')->name('about.licenses');
        Route::get('about/contracts', 'ContractsController@index')->name('about.contracts');
        Route::get('about/ownership', 'OwnershipController@index')->name('ownership.contracts');
    });
