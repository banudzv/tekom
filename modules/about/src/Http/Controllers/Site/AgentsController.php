<?php

namespace WezomCms\About\Http\Controllers\Site;

use WezomCms\About\Repositories\AgentRepository;
use WezomCms\Core\Http\Controllers\SiteController;
use WezomCms\Core\Traits\ImageFromSettings;

class AgentsController extends SiteController
{
    use ImageFromSettings;

    private $settings;

    private AgentRepository $agentRepository;

    public function __construct(AgentRepository $agentRepository)
    {
        $this->agentRepository = $agentRepository;
        $this->settings = settings('about.site', []);
    }

    public function index()
    {
        $localSetting = settings('agent.site', []);

        $pageName = array_get($this->settings, 'name');
        // Breadcrumbs
        $this->addBreadcrumb($pageName, route('about.about-us'));
        $this->seo()
            ->setTitle(array_get($localSetting, 'title'))
            ->setH1(array_get($this->settings, 'h1'))
            ->setPageName($pageName)
            ->setDescription(array_get($localSetting, 'description'))
            ->metatags()
            ->setKeywords(array_get($localSetting, 'keywords'));

        $agents = $this->agentRepository->getByFront();

        return view('cms-about::site.agents', [
            'banner' => $this->getBannerUrl('about.page'),
            'description' => array_get($this->settings, 'description'),
            'agents' => $agents
        ]);
    }
}
