<?php

namespace WezomCms\About\Http\Controllers\Site;

use WezomCms\About\Repositories\RuleGroupRepository;
use WezomCms\Core\Http\Controllers\SiteController;
use WezomCms\Core\Traits\ImageFromSettings;

class RuleController extends SiteController
{
    use ImageFromSettings;

    private $settings;

    private RuleGroupRepository $groupRepository;

    public function __construct(RuleGroupRepository $groupRepository)
    {
        $this->groupRepository = $groupRepository;

        $this->settings = settings('about.site', []);
    }

    public function index()
    {
        $localSetting = settings('about.about_rule', []);

        $pageName = array_get($this->settings, 'name');
        // Breadcrumbs
        $this->addBreadcrumb($pageName, route('about.about-us'));
        $this->seo()
            ->setTitle(array_get($localSetting, 'title'))
            ->setH1(array_get($this->settings, 'h1'))
            ->setPageName($pageName)
            ->setDescription(array_get($localSetting, 'description'))
            ->metatags()
            ->setKeywords(array_get($localSetting, 'keywords'));

        $groups = $this->groupRepository->getByFront();

        return view('cms-about::site.rule', [
            'banner' => $this->getBannerUrl('about.page'),
            'description' => array_get($this->settings, 'description'),
            'groups' => $groups
        ]);
    }
}
