<?php

namespace WezomCms\About\Http\Controllers\Site;

use WezomCms\About\Repositories\DetailFileRepository;
use WezomCms\About\Repositories\DetailRepository;
use WezomCms\Core\Http\Controllers\SiteController;
use WezomCms\Core\Traits\ImageFromSettings;

class DetailsController extends SiteController
{
    use ImageFromSettings;

    private $settings;

    private DetailRepository $detailRepository;
    private DetailFileRepository $detailFileRepository;

    public function __construct(
        DetailRepository $detailRepository,
        DetailFileRepository $detailFileRepository
    )
    {
        $this->settings = settings('about.site', []);

        $this->detailRepository = $detailRepository;
        $this->detailFileRepository = $detailFileRepository;
    }

    public function index()
    {
        $localSetting = settings('about.about_details', []);

        $pageName = array_get($this->settings, 'name');
        // Breadcrumbs
        $this->addBreadcrumb($pageName, route('about.about-us'));
        $this->seo()
            ->setTitle(array_get($localSetting, 'title'))
            ->setH1(array_get($this->settings, 'h1'))
            ->setPageName($pageName)
            ->setDescription(array_get($localSetting, 'description'))
            ->metatags()
            ->setKeywords(array_get($localSetting, 'keywords'));

        $details = $this->detailRepository->getByFront();
        $detailFiles = $this->detailFileRepository->getByFront();

        return view('cms-about::site.details', [
            'banner' => $this->getBannerUrl('about.page'),
            'description' => array_get($this->settings, 'description'),
            'details' => $details,
            'detailFiles' => $detailFiles,
        ]);
    }
}
