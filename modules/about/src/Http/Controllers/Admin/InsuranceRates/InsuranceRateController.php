<?php

namespace WezomCms\About\Http\Controllers\Admin\InsuranceRates;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use WezomCms\About\Http\Requests\Admin\InsuranceRates\InsuranceRateRequest;
use WezomCms\About\Models\InsuranceRates\InsuranceRate;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;

class InsuranceRateController extends AbstractCRUDController
{
    /**
     * Model name.
     *
     * @var string
     */
    protected $model = InsuranceRate::class;

    /**
     * Indicates whether to use pagination.
     *
     * @var bool
     */
    protected $paginate = false;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-about::admin.insurance_rates';

    /**
     * Resource route name.
     *
     * @var string
     */
    protected $routeName = 'admin.insurance-rates';

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = InsuranceRateRequest::class;

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-about::admin.InsuranceRates');
    }

    /**
     * @param  Builder  $query
     * @param  Request  $request
     */
    protected function selectionIndexResult($query, Request $request)
    {
        $query->orderBy('sort');
    }
}

