<?php

namespace WezomCms\About\Http\Controllers\Admin\AboutUs;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use WezomCms\About\Http\Requests\Admin\AboutUs\EventRequest;
use WezomCms\About\Models\AboutUs\Event;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;
use WezomCms\About\Repositories\EventRepository;

class EventController extends AbstractCRUDController
{
    /**
     * Model name.
     *
     * @var string
     */
    protected $model = Event::class;

    /**
     * Indicates whether to use pagination.
     *
     * @var bool
     */
    protected $paginate = false;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-about::admin.events';

    /**
     * Resource route name.
     *
     * @var string
     */
    protected $routeName = 'admin.about-events';

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = EventRequest::class;
    /**
     * @var EventRepository
     */
    private $eventRepository;

    public function __construct(EventRepository $eventRepository)
    {
        parent::__construct();
        $this->eventRepository = $eventRepository;
    }

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-about::admin.Events');
    }

    /**
     * @param  Builder  $query
     * @param  Request  $request
     */
    protected function selectionIndexResult($query, Request $request)
    {
        $query->orderBy('year', 'desc');
    }
}

