<?php

namespace WezomCms\About\Http\Controllers\Admin\Ownership;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use WezomCms\About\Http\Requests\Admin\Contracts\ContractRequest;
use WezomCms\About\Http\Requests\Admin\Rules\GroupRequest;
use WezomCms\About\Http\Requests\Admin\Rules\RuleRequest;
use WezomCms\About\Models\Ownership\Contract;
use WezomCms\About\Models\Rules\Group;
use WezomCms\About\Models\Rules\Rule;
use WezomCms\About\Repositories\ContractGroupRepository;
use WezomCms\About\Repositories\OwnershipGroupRepository;
use WezomCms\About\Repositories\RuleGroupRepository;
use WezomCms\About\Repositories\RuleRepository;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;

class ContractsController extends AbstractCRUDController
{
    /**
     * Model name.
     *
     * @var string
     */
    protected $model = Contract::class;

    /**
     * Indicates whether to use pagination.
     *
     * @var bool
     */
    protected $paginate = false;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-about::admin.ownership.contract';

    /**
     * Resource route name.
     *
     * @var string
     */
    protected $routeName = 'admin.about-contracts-ownership';

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = ContractRequest::class;
    /**
     * @var ContractGroupRepository
     */
    private $contractGroupRepository;

    public function __construct(OwnershipGroupRepository $contractGroupRepository)
    {
        parent::__construct();
        $this->contractGroupRepository = $contractGroupRepository;
    }

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-about::admin.Contracts');
    }

    /**
     * @param  Builder  $query
     * @param  Request  $request
     */
    protected function selectionIndexResult($query, Request $request)
    {
        $query->orderBy('sort');
    }

    /**
     * @param  Rule  $model
     * @param  array  $viewData
     * @return array
     */
    protected function formData($model, array $viewData): array
    {
        return [
            'groups' => $this->contractGroupRepository->getBySelect(),
        ];
    }

    /**
     * @return null|string
     */
    protected function abilityPrefix(): ?string
    {
        return 'about-contracts-ownership';
    }

}

