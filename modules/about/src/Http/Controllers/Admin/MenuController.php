<?php

namespace WezomCms\About\Http\Controllers\Admin;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use WezomCms\About\Http\Requests\Admin\MenuRequest;
use WezomCms\About\Models\Menu;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;

class MenuController extends AbstractCRUDController
{
    /**
     * Model name.
     *
     * @var string
     */
    protected $model = Menu::class;

    /**
     * Indicates whether to use pagination.
     *
     * @var bool
     */
    protected $paginate = false;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-about::admin.menu';

    /**
     * Resource route name.
     *
     * @var string
     */
    protected $routeName = 'admin.about-menu';

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = MenuRequest::class;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-about::admin.Menu');
    }

    /**
     * @param  Builder  $query
     * @param  Request  $request
     */
    protected function selectionIndexResult($query, Request $request)
    {
        $query->orderBy('sort');
    }
}


