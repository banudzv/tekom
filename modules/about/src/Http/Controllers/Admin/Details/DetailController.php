<?php

namespace WezomCms\About\Http\Controllers\Admin\Details;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use WezomCms\About\Http\Requests\Admin\Details\DetailRequest;
use WezomCms\About\Http\Requests\Admin\Rules\GroupRequest;
use WezomCms\About\Http\Requests\Admin\Rules\RuleRequest;
use WezomCms\About\Models\Details\Detail;
use WezomCms\About\Models\Rules\Group;
use WezomCms\About\Models\Rules\Rule;
use WezomCms\About\Repositories\RuleGroupRepository;
use WezomCms\About\Repositories\RuleRepository;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;

class DetailController extends AbstractCRUDController
{
    /**
     * Model name.
     *
     * @var string
     */
    protected $model = Detail::class;

    /**
     * Indicates whether to use pagination.
     *
     * @var bool
     */
    protected $paginate = false;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-about::admin.details.details';

    /**
     * Resource route name.
     *
     * @var string
     */
    protected $routeName = 'admin.about-details';

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = DetailRequest::class;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-about::admin.Details');
    }

    /**
     * @param  Builder  $query
     * @param  Request  $request
     */
    protected function selectionIndexResult($query, Request $request)
    {
        $query->orderBy('sort');
    }
}

