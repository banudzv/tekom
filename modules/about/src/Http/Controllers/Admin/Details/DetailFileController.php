<?php

namespace WezomCms\About\Http\Controllers\Admin\Details;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use WezomCms\About\Http\Requests\Admin\Details\DetailFileRequest;
use WezomCms\About\Models\Details\DetailFile;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;

class DetailFileController extends AbstractCRUDController
{
    /**
     * Model name.
     *
     * @var string
     */
    protected $model = DetailFile::class;

    /**
     * Indicates whether to use pagination.
     *
     * @var bool
     */
    protected $paginate = false;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-about::admin.details.files';

    /**
     * Resource route name.
     *
     * @var string
     */
    protected $routeName = 'admin.about-detail-files';

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = DetailFileRequest::class;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-about::admin.Detail file');
    }

    /**
     * @param  Builder  $query
     * @param  Request  $request
     */
    protected function selectionIndexResult($query, Request $request)
    {
        $query->orderBy('sort');
    }
}

