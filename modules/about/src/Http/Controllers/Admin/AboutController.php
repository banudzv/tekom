<?php

namespace WezomCms\About\Http\Controllers\Admin;

use WezomCms\Core\Http\Controllers\SingleSettingsController;
use WezomCms\Core\Settings\Fields\AbstractField;
use WezomCms\Core\Settings\Fields\Image;
use WezomCms\Core\Settings\Fields\Input;
use WezomCms\Core\Settings\Fields\Textarea;
use WezomCms\Core\Settings\Fields\Wysiwyg;
use WezomCms\Core\Settings\MetaFields\SeoFields;
use WezomCms\Core\Settings\MetaFields\SeoText;
use WezomCms\Core\Settings\MultilingualGroup;
use WezomCms\Core\Settings\RenderSettings;
use WezomCms\Core\Settings\Tab;

class AboutController extends SingleSettingsController
{
    /**
     * @return null|string
     */
    protected function abilityPrefix(): ?string
    {
        return 'about-page';
    }

    /**
     * Page title.
     *
     * @return string|null
     */
    protected function title(): ?string
    {
        return __('cms-about::admin.About company');
    }

    /**
     * @return array|AbstractField[]|MultilingualGroup[]
     * @throws \Exception
     */
    protected function settings(): array
    {
        $pageDesc = Wysiwyg::make()
            ->setName(__('cms-about::admin.Text fo description'))
            ->setSort(2)
            ->setIsMultilingual()
            ->setKey('text')
            ->setRules('nullable|string');

        $result = [
            SeoFields::make('About company'),
        ];

        $tabs = new RenderSettings(
            new Tab('page', __('cms-core::admin.tabs.site banner'), 1, 'fa-folder-o')
        );

        $tabs_about_us = new RenderSettings(
            new Tab('about_us', __('cms-about::admin.tab about us page'), 2, 'fa-folder-o')
        );
        $result[] = SeoFields::make('About us', [$pageDesc], $tabs_about_us);

        $tabs_about_details = new RenderSettings(
            new Tab('about_details', __('cms-about::admin.tab about details page'), 3, 'fa-folder-o')
        );
        $result[] = SeoFields::make('About details', [], $tabs_about_details);

        $tabs_about_rule = new RenderSettings(
            new Tab('about_rule', __('cms-about::admin.tab about rule page'), 4, 'fa-folder-o')
        );
        $result[] = SeoFields::make('About rule', [], $tabs_about_rule);

        $tabs_about_contracts = new RenderSettings(
            new Tab('about_contracts', __('cms-about::admin.tab about contracts page'), 5, 'fa-folder-o')
        );
        $result[] = SeoFields::make('About contracts', [], $tabs_about_contracts);

        $tabs_about_license = new RenderSettings(
            new Tab('about_license', __('cms-about::admin.tab about license page'), 6, 'fa-folder-o')
        );
        $result[] = SeoFields::make('About license', [], $tabs_about_license);

        $tabs_insurance_rate = new RenderSettings(
            new Tab('insurance_rate', __('cms-about::admin.tab about insurance rate page'), 6, 'fa-folder-o')
        );
        $result[] = SeoFields::make('Insurance rate', [], $tabs_insurance_rate);

        $result[] = Image::make($tabs)
            ->setSettings([
                'images' => [
                    'directory' => 'settings',
                    'default' => 'medium', // For admin image preview
                    'sizes' => [
                        'big' => [
                            'width' => 1920,
                            'height' => 700,
                            'mode' => 'resize',
                        ],
                    ],
                ],
            ])
            ->setName(__('cms-about::admin.image-size', ['width' => 1920, 'height' => 700]))
            ->setSort(1)
            ->setKey('image')
            ->setRules('nullable|file');

        return $result;
    }
}

