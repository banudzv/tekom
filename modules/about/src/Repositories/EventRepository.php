<?php

namespace WezomCms\About\Repositories;

use WezomCms\About\Models\AboutUs\Event;
use WezomCms\Core\Repositories\AbstractRepository;

class EventRepository extends AbstractRepository
{
    protected function model()
    {
        return Event::class;
    }

    public function getByFront()
    {
        return $this->getModelQuery()
            ->published()
            ->with(['translations'])
            ->orderBy('year')
            ->get();
    }

    public function existsYear($year, $exceptId = null)
    {
        $query = $this->getModelQuery()->where('year', $year);

        if($exceptId){
            $query->where('id', '!=', $exceptId);
        }

        return $query->exists();
    }
}
