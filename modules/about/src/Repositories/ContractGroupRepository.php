<?php

namespace WezomCms\About\Repositories;

use WezomCms\About\Models\Contracts\Group;
use WezomCms\Core\Repositories\AbstractRepository;

class ContractGroupRepository extends AbstractRepository
{
    protected function model()
    {
        return Group::class;
    }

    public function getByFront()
    {
        return $this->getModelQuery()
            ->published()
            ->with(['contracts'])
            ->orderBy('sort')
            ->get();
    }
}
