<?php

namespace WezomCms\About\Models\License;

use Illuminate\Database\Eloquent\Model;
use WezomCms\Core\ExtendPackage\Translatable;
use WezomCms\Core\Traits\Model\FileAttachable;
use WezomCms\Core\Traits\Model\PublishedTrait;

/**
 *
 * @property int $id
 * @property bool $published
 * @property int $sort
 * @property string|null $file
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Menu\Models\Menu withTranslation()
 * @mixin \Eloquent
 * @mixin LicenseTranslation
 */
class License extends Model
{
    use Translatable;
    use PublishedTrait;
    use FileAttachable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'about_licenses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['published', 'sort', 'group_id'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'published' => 'bool',
    ];

    /**
     * Names of the fields being translated in the "Translation" model.
     *
     * @var array
     */
    protected $translatedAttributes = ['name'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    public function fileSettings(): array
    {
        return ['file' => ['directory' => 'licenses']];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(Group::class, 'group_id');
    }
}

