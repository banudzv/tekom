@extends('cms-ui::layouts.main')

@php
    /**
     * @var $banner string|null
     * @var $description string|null
     * @var $details WezomCms\About\Models\Details\Detail[]
     * @var $detailFiles WezomCms\About\Models\Details\DetailFile[]
     */

@endphp

@section('content')
    @widget('ui:heading-section', [
        'bannerImage' => $banner,
        'title' => SEO::getH1(),
        'text' => $description,
        'classes' => 'heading-section--contacts'
    ])

    @widget('about:menu')

    <div class="section section--information section--bg-grey">
        <div class="container">
            <div class="_grid _justify-center">
                <div class="_cell _cell--12 _lg:cell--10">
                    <div class="table table--theme-details">
                        @foreach($details as $detail)
                            <div class="table__row">
                                <div class="table__cell table__cell--left">{{ $detail->name }}</div>
                                <div class="table__cell table__cell--right">{!! $detail->text !!}</div>
                            </div>
                        @endforeach
                    </div>

                    @foreach($detailFiles as $file)
                        @include('cms-about::site.partials.row-file', ['item' => $file])
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
