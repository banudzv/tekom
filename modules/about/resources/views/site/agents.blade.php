@extends('cms-ui::layouts.main')

@php
    /**
     * @var $banner string|null
     * @var $description string|null
     * @var $agents \WezomCms\About\Models\Agent[]
     */

@endphp

@section('content')
    @widget('ui:heading-section', [
        'bannerImage' => $banner,
        'title' => SEO::getH1(),
        'text' => $description,
        'classes' => 'heading-section--contacts'
    ])

    @widget('about:menu')

    <div class="section section--information section--bg-grey">
        <div class="container">
            <div class="_grid _justify-center">
                <div class="_cell _cell--12 _lg:cell--10">
                    <div class="text text--size-lg text--color-grey _mb-xl">
                        @lang('cms-about::site.about-agent.title')
                    </div>
                    <div class="table table--theme-agents">
                        <div class="table__row">
                            <div class="table__cell table__cell--left">
                                @lang('cms-about::site.about-agent.name')
                            </div>
                            <div class="table__cell table__cell--right">
                                @lang('cms-about::site.about-agent.contact information')
                            </div>
                        </div>
                        @foreach($agents as $agent)
                            <div class="table__row">
                                <div class="table__cell table__cell--left">{{ $agent->name }}</div>
                                <div class="table__cell table__cell--right">{!! $agent->text !!}</div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
