@extends('cms-ui::layouts.main')

@php
    /**
     * @var $banner string|null
     * @var $description string|null
     * @var $text string|null
     * @var $awards \WezomCms\About\Models\AboutUs\Award[]
     * @var $events \WezomCms\About\Models\AboutUs\Event[]
     * @var $employees \WezomCms\OurTeam\Models\Employee[]
     * @var $menu \WezomCms\About\Models\Menu[]
     */

@endphp

@section('content')
    @widget('ui:heading-section', [
        'bannerImage' => $banner,
        'title' => SEO::getH1(),
        'text' => $description,
        'classes' => 'heading-section--contacts'
    ])

    @widget('about:menu')

    <div class="section section--brand section--bg-grey">
        <div class="container">
            <div class="title title--size-h2 _text-center">
                @lang('cms-about::site.about-us.title for desc')
            </div>
            <div class="_grid _lg:justify-end _pt-xxl">
                <div class="_cell _cell--12 _lg:cell--11 _px-sm _sm:px-df  _lg:px-none">
                    <div class="_grid _grid--1 _md:grid--2">
                        <div class="_cell _lg:pl-xs _lg:pr-hg">
                            <div class="wysiwyg wysiwyg--theme-about js-import" data-wrap-media data-draggable-table>
                                {!! $text !!}
                            </div>
                        </div>
                        <div class="_cell _pt-xl _px-sm _sm:px-df  _lg:px-none">
                            <img class="lozad js-import fade-in" src="{{ url('/images/empty.gif') }}"
                                 data-src="{{ asset('/static/images/about-tekom.png') }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section section--history">
        <div class="container">
            <div class="_grid _justify-center">
                <div class="_cell _cell--12 _lg:cell--10">
                    <div class="title title--size-h2 _text-center _mb-xxl">
                        @lang('cms-about::site.about-us.chronology of the company')
                    </div>
                    <div class="chronology-slider _lg:pb-xl _mb-hg" data-slider-root>
                        <div class="chronology-slider__arrows">
                            <div class="carousel-arrow carousel-arrow--prev" data-slick-carousel-prev-arrow style="display: none;">
                                @svg('arrow-left-thick', 22, 23, '')
                            </div>
                            <div class="carousel-arrow carousel-arrow--next" data-slick-carousel-next-arrow>
                                @svg('arrow-right-thick', 22, 23, '')
                            </div>
                        </div>
                        <div class="chronology-slider__slider chronology-slider__slider--years js-import"
                             data-slick-carousel='{!! r2d2()->attrJsonEncode([
                        'factory' => 'ChronologySliderYears'
                        ])
                    !!}'
                        >
                            <div class="chronology-slider__list" data-slick-carousel-list>
                                @foreach($events->reverse() as $event)
                                    <div data-chronology-slider-slide="{{ $loop->index }}">
                                        <div class="chronology-slider__year {{ $loop->first ? 'is-active' : null }}" data-chronology-slider-year>
                                            {{ $event->year }}
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="chronology-slider__slider chronology-slider__slider--info js-import"
                             data-slick-carousel='{!! r2d2()->attrJsonEncode([
                        'factory' => 'ChronologySliderInfo'
                        ])
                    !!}'
                        >
                            <div class="chronology-slider__list" data-slick-carousel-list>
                                @foreach($events->reverse() as $event)
                                    <div data-chronology-slider-slide="{{ $loop->index }}">
                                        <div class="_grid _grid--1 _md:grid--2 _justify-center _px-sm _sm:px-df  _lg:px-none">
                                            <div class="_cell _pt-df _lg:pt-hg">
                                                <div class="_mb-df title title--size-h5 title--weight-regular">
                                                    {{ $event->name }}
                                                </div>
                                                <div class="wysiwyg wysiwyg--theme-default js-import" data-wrap-media data-draggable-table>
                                                    {!! $event->text !!}
                                                </div>
                                            </div>
                                            <div class="_cell _flex _justify-end _pt-md _lg:pt-xxs _lg:px-xxl">
                                                <div class="chronology-slider__content">
                                                    <div class="chronology-slider__img">
                                                        <div class="chronology-slider__img-decor"></div>
                                                        <img src="{{ url('/images/empty.gif') }}" class="lozad js-import fade-in" data-src="{{ $event->getImage() }}" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="blockquote">
                        <div class="blockquote__text">
                            @lang('cms-about::site.about-us.our mission')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section section--our-goals">
        <div class="container">
            <div class="_grid _justify-center _spacer _spacer--md">
                <div class="_cell _cell--12 _md:cell--6">
                    <div class="reports-banner reports-banner--theme-about reports-banner--theme-green">
                        <div class="reports-banner__content">
                            <div class="reports-banner__icon">
                                @svg('guard', 53, 60)
                            </div>
                            <div class="reports-banner__title">@lang('cms-about::site.about-us.our task.title')</div>
                            <div class="reports-banner__description">@lang('cms-about::site.about-us.our task.sub-title')</div>
                            <div class="reports-banner__text">@lang('cms-about::site.about-us.our task.description')</div>
                        </div>
                    </div>
                </div>
                <div class="_cell _cell--12 _md:cell--6">
                    <div class="reports-banner reports-banner--theme-about">
                        <div class="reports-banner__content">
                            <div class="reports-banner__icon">
                                @svg('guard', 53, 60)
                            </div>
                            <div class="reports-banner__title">@lang('cms-about::site.about-us.our target.title')</div>
                            <div class="reports-banner__description">@lang('cms-about::site.about-us.our target.sub-title')</div>
                            <div class="reports-banner__text">@lang('cms-about::site.about-us.our target.description')</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @widget('counter', ['modification' => 'why'])
    <div class="section section--our-awards">
        <div class="container">
            <div class="title title--size-h2 _text-center _mb-df _lg:mb-hg">@lang('cms-about::site.about-us.our awards')</div>
            <div class="awards-slider _pt-sm js-import"
                 data-slick-carousel='{!! r2d2()->attrJsonEncode([
                        'factory' => 'AwardsSlider'
                        ])
                    !!}'
                >
                <div class="awards-slider__list" data-slick-carousel-list>
                    @foreach($awards as $award)
                        <div class="award">
                            <div class="award__img">
                                <div class="award__img-inner">
                                    <img class="lozad js-import fade-in" src="{{ url('/images/empty.gif') }}"
                                         data-src="{{ $award->getImage() }}" alt="">
                                </div>
                            </div>
                            <div class="award__name">
                                {{ $award->name }}
                            </div>
                            <div class="award__text">
                                {!! $award->text !!}
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="awards-slider__arrows">
                    <div class="carousel-arrow carousel-arrow--prev" data-slick-carousel-prev-arrow style="display: none;">
                        @svg('arrow-left-thick', 22, 23, '')
                    </div>
                    <div class="carousel-arrow carousel-arrow--next" data-slick-carousel-next-arrow>
                        @svg('arrow-right-thick', 22, 23, '')
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section section--our-team section--bg-grey">
        <div class="container">
            <div class="_grid _justify-center _mb-df _lg:mb-hg">
                <div class="_cell _cell--12 _md:cell--8 _lg:cell--6">
                    <div class="title title--size-h2 _text-center _mb-df">
                        @lang('cms-about::site.about-us.employee title')
                    </div>
                    <div class="title title--size-h5 title--weight-regular _text-center">
                        @lang('cms-about::site.about-us.employee description')
                    </div>
                </div>
            </div>
            <div class="person-slider js-import"
                 data-slick-carousel='{!! r2d2()->attrJsonEncode([
                        'factory' => 'PersonSlider'
                        ])
                    !!}'
            >
                <div class="person-slider__list" data-slick-carousel-list>
                    @foreach($employees as $employee)
                        <div class="person-card accordion js-import"
                             data-accordion='{"ns": "{{ 'person' . $employee->id }}"}'
                        >
                            <div class="person-card__main-content">
                                <div class="person-card__img">
                                    <img class="lozad js-import fade-in" src="{{ url('/images/empty.gif') }}"
                                         data-src="{{ $employee->getImage() }}" alt="">
                                </div>
                                <div class="person-card__text person-card__text--name">{{ $employee->name }}</div>
                                <div class="person-card__text person-card__text--position">{{ $employee->position }}</div>
                                <div class="person-card__separator"></div>
                                <div class="person-card__text person-card__text--experience">{{ $employee->experience_description }}</div>
                            </div>
                            <div class="person-card__accordion"
                                 style="{{ $employee->education && $employee->work_description && $employee->birthday ? null : 'visibility:hidden;' }}"
                            >
                                <div class="person-card__accordion-button accordion__button"
                                     data-accordion-ns="{{ 'person' . $employee->id }}"
                                     data-accordion-button="{{ 'person' . $employee->id }}"
                                     data-show-more-open="{{ __('cms-ui::site.show more open') }}"
                                     data-show-more-close="{{ __('cms-ui::site.show more close') }}"
                                ></div>
                                <div class="person-card__accordion-content"
                                     data-accordion-ns="{{ 'person' . $employee->id }}"
                                     data-accordion-block="{{ 'person' . $employee->id }}"
                                     style="display: none"
                                >
                                    @if($employee->education)
                                        <div class="person-card__text person-card__text--education">{{ $employee->education }}</div>
                                    @endif
                                    @if($employee->work_description)
                                        <div class="person-card__text person-card__text--work">{{ $employee->work_description }}</div>
                                    @endif
                                    @if($employee->birthday)
                                        <div class="person-card__text person-card__text--birthday">
                                            <span>@lang('cms-about::site.about-us.birthday')</span>
                                            <strong>{{ $employee->birthday ? $employee->birthday->format('Y') : null}}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="person-slider__arrows">
                    <div class="carousel-arrow carousel-arrow--prev" data-slick-carousel-prev-arrow style="display: none;">
                        @svg('arrow-left-thick', 22, 23, '')
                    </div>
                    <div class="carousel-arrow carousel-arrow--next" data-slick-carousel-next-arrow>
                        @svg('arrow-right-thick', 22, 23, '')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

