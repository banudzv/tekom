<?php

return [
    'widgets' => [
        'about:menu' => \WezomCms\About\Widgets\AboutMenu::class
    ],
    'images' => [
        'awards' => [
            'directory' => 'awards',
            'default' => 'medium', // For admin image preview
            'sizes' => [
                'medium' => [
                    'width' => 199,
                    'mode' => 'resize'
                ],
            ],
        ],
        'events' => [
            'directory' => 'events',
            'default' => 'medium', // For admin image preview
            'sizes' => [
                'medium' => [
                    'width' => 368,
                    'mode' => 'resize'
                ],
            ],
        ]
    ],
];
