<?php

$screenBreaks = [
	'xxs' => 375,
	'xs' => 480,
	'sm' => 568,
	'ms' => 640,
	'md' => 768,
	'def' => 1023,
	'lg' => 1280,
	'xl' => 1366
];

return [
	'heroSlider' => [
		'factory' => 'HeroSlider',
		'factoryExtend' => [
            'lazyLoad' => 'progressive',
			'loop' => false,
			'slidesToShow' => '1',
			'dots' => true,
			'speed' => 1000,
			'fade' => true,
            'autoplay'=> true,
            'autoplaySpeed' => 6000,
            'infinite' => false
		]
	],
	'servicesSlider' => [
		'factory' => 'ServicesSlider',
		'factoryExtend' => [
			'loop' => false,
			'slidesToShow' => '1',
			'dots' => true,
			'speed' => 1000,
			'autoplaySpeed' => 4000,
			'fade' => true,
			'infinite' => false,
			'touchMove' => false,
			'swipe' => false,
			'adaptiveHeight' => true
		]
	],
];
