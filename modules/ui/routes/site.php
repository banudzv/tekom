<?php

use WezomCms\Ui\Http\Controllers\Site\NotFoundController;

Route::fallback(NotFoundController::class);

Route::get('/ui-page', [WezomCms\Ui\Http\Controllers\Site\UiPageController::class, 'index'])->name('ui-page');
//Route::get('/sitemap', [WezomCms\Ui\Http\Controllers\Site\UiPageController::class, 'sitemap'])->name('sitemap');
//Route::get('/search-results', [WezomCms\Ui\Http\Controllers\Site\UiPageController::class, 'searchResults'])->name('search-results');

Route::get('/form-demo', function () {
	return response()->json(['status' => 'success']);
})->name('demo.form');
