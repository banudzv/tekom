@extends('cms-ui::layouts.base')

@section('main')
    <div class="section _pt-hd">
        <div class="container">
            <hr class="_mb-def" />
            <div class="title title--size-xxl _mb-def">
                Title XXL
            </div>
            <div class="title title--size-h1 _mb-def">
                Title h1
            </div>
            <div class="title title--size-h2 _mb-def">
                Title h2
            </div>
            <div class="title title--size-h3 _mb-def">
                Title h3
            </div>
            <div class="title title--size-h4 _mb-def">
                Title h4
            </div>
        </div>
		@widget('ui:up-button')
	</div>
	<div class="section">
		<div class="container">
			<div>
				{{-- colors --}}
				<hr class="_mtb-def">
				<div class="title title--size-h1 _pb-lg">
					Colors
				</div>
				<div class="_grid _grid--2 _xs:grid--4 _md:grid--6 _def:grid--8 _lg:grid--10 _nml-xs" style="background-color: rgba(34, 123, 120, .2)">
					<div class="_cell _pl-xs _mb-xs color-block" style="background-color: #E5F4EF;">
						$color-bg-light
					</div>
					<div class="_cell _pl-xs _mb-xs color-block" style="background-color: #A7D8C9;">
						$color-bg-lighter
					</div>
					<div class="_cell _pl-xs _mb-xs color-block" style="background-color: #5C9796;">
						$color-bg-dark
					</div>
					<div class="_cell _pl-xs _mb-xs color-block" style="background-color: rgba(243, 244, 247, 0.6);">
						$color-bg-grey-lighter
					</div>
					<div class="_cell _pl-xs _mb-xs color-block" style="background-color: #227B78;">
						$color-main
					</div>
					<div class="_cell _pl-xs _mb-xs color-block" style="background-color: #67CBAD;">
						$color-accent
					</div>
					<div class="_cell _pl-xs _mb-xs color-block" style="background-color: #1A7A78;">
						$color-accent-darker
					</div>
					<div class="_cell _pl-xs _mb-xs color-block" style="background-color: #FC737B;">
						$color-accent-2
					</div>
					<div class="_cell _pl-xs _mb-xs color-block" style="background-color: #E2575C;">
						$color-accent-2-darker
					</div>
					<div class="_cell _pl-xs _mb-xs color-block" style="background-color: #FFD500;">
						$color-accent-3
					</div>
					<div class="_cell _pl-xs _mb-xs color-block" style="background-color: rgba(42, 55, 73, .6);">
						$color-text-1
					</div>
					<div class="_cell _pl-xs _mb-xs color-block" style="background-color: rgba(255, 255, 255, .6);">
						$color-text-2
					</div>
					<div class="_cell _pl-xs _mb-xs color-block" style="background-color: #A8A8A8;">
						$color-grey
					</div>
					<div class="_cell _pl-xs _mb-xs color-block" style="background-color: #E5E5E5;">
						$color-bg
					</div>
					<div class="_cell _pl-xs _mb-xs color-block" style="background-color: #F0F3F1;">
						$color-bg-dark
					</div>
					<div class="_cell _pl-xs _mb-xs color-block" style="background-color: #EEF1EB;">
						$color-bg-dark-2
					</div>
					<div class="_cell _pl-xs _mb-xs color-block" style="background-color: #FEFEFE;">
						$color-text-light
					</div>
					<div class="_cell _pl-xs _mb-xs color-block" style="background-color: #F2F2F2;">
						$color-light
					</div>
					<div class="_cell _pl-xs _mb-xs color-block" style="background-color: #4F4F4F; color: #fff;">
						$color-grey-dark
					</div>
				</div>
				{{-- buttons --}}
				<hr class="_mtb-def">
				<div class="title title--size-h1">
					Buttons
				</div>
				<div class="_grid _pt-xl _spacer _spacer--df">
					<div class="_cell _cell--auto">
						@widget('ui:button', ['text' => 'заказать услугу', 'modificators' => ['color-accent', 'size-def', 'uppercase', 'ls-def', 'bold']])
					</div>
					<div class="_cell _cell--auto">
						@widget('ui:button', ['text' => 'Страховой случай', 'modificators' => ['color-accent-2', 'size-md', 'uppercase', 'ls-def', 'bold']])
					</div>
					<div class="_cell _cell--auto">
						@widget('ui:button', ['text' => 'Оплатить онлайн', 'modificators' => ['color-light', 'size-md', 'uppercase', 'ls-def', 'bold']])
					</div>
					<div class="_cell _cell--auto">
						@widget('ui:button', ['text' => 'заказать консультацию', 'modificators' => ['color-light-transparent', 'size-def', 'uppercase', 'ls-def', 'bold']])
					</div>
					<div class="_cell _cell--auto _spacer _spacer--md">
						<div style="background-color: rgba(34, 123, 120, .2); padding: 0 1rem;">
							@widget('ui:button', ['text' => 'Одесса', 'modificators' => ['color-accent-3', 'size-sm']])
						</div>
					</div>
					<div class="_cell _cell--auto _spacer _spacer--md">
						<div style="background-color: rgba(34, 123, 120, .2); padding: 0 1rem;">
							@widget('ui:button', ['text' => 'Одесса', 'modificators' => ['color-accent-3-light', 'size-sm']])
						</div>
					</div>
					<div class="_cell _cell--auto _flex _spacer _spacer--md">
						<div>
							@widget('ui:button', ['component' => 'a', 'attrs' => ['href' => '#'], 'icon' => ['facebook', 9, 18], 'modificators' => ['theme-social-media-outlined']])
						</div>
						<div>
							@widget('ui:button', ['component' => 'a', 'attrs' => ['href' => '#'], 'icon' => ['telegram', 14, 12, false, '_nml-xxs'], 'modificators' => ['theme-social-media']])
						</div>
					</div>
				</div>
			</div>
			<div class="_pt-lg">
				<hr class="_mtb-def">
				<div class="title title--size-h1">
					Form elements
				</div>
				<div class="_grid _spacer _spacer--lg _pt-xl" style="background-color: #333;">
					<div class="_cell _cell--6">
						@component('cms-ui::components.form.form-ajax', ['route' => 'demo.form', 'id' => uniqid('form-')])
							@include('cms-ui::components.form.input', [
								'name' => uniqid('email-'),
								'attributes' => [ 'placeholder' => 'Enter your email*', 'required' => 'email' ],
								'modificators' => ['inactive'],
								'classes' => 'form-item--theme-default',
								'mode' => 'email',
							])
						@endcomponent
					</div>
					<div class="_cell _cell--6">
						@php
							$options = [
								'Біла Церква',
								'Бориспіль',
								'Житомир',
								'Каменец-Подільский',
								'Ковель',
								(object)[
									'text' => 'Київ',
									'selected' => true
								],
								'Конотоп',
								'Конотоп',
								'Конотоп',
								'Конотоп',
								'Конотоп',
								'Луцьк'
							];
						@endphp
						@component('cms-ui::components.form.select2',
							 [
								 'selectOptions' => [
									 'Factory' => 'DefaultSelect2'
								],
								'options' => $options,
								'name' => uniqid('select2'),
								 'attrs' => ['name' => uniqid('select2') . '-select']
							]
						)
							@foreach($options ?? [] as $option)
								<option
									{{ $option->selected ?? false ? 'selected' : '' }}
									value="{{ isset($option->text) ? $option->text : $option }}"
								>
									{{ isset($option->text) ? $option->text : $option }}
								</option>
							@endforeach
						@endcomponent
					</div>
					<div class="_cell _cell--6">
						@component('cms-ui::components.form.form-ajax', ['route' => 'demo.form', 'id' => uniqid('form-')])
							@include('cms-ui::components.form.input', [
								'name' => uniqid('email-'),
								'attributes' => [ 'placeholder' => 'Enter your email*', 'required' => 'email' ],
								'modificators' => [],
								'classes' => 'form-item--theme-default',
								'mode' => 'email',
							])
						@endcomponent
					</div>
					<div class="_cell _cell--6">
						@component('cms-ui::components.form.form-ajax', ['route' => 'demo.form', 'id' => uniqid('form-')])
							@include('cms-ui::components.form.input', [
								'name' => uniqid('email-'),
								'attributes' => [ 'placeholder' => 'Enter your email' ],
								'modificators' => [],
								'classes' => 'form-item--theme-default',
								'mode' => 'email',
							])
						@endcomponent
					</div>
					<div class="_cell _cell--6">
						@component('cms-ui::components.form.form-ajax', ['route' => 'demo.form', 'id' => uniqid('form-')])
							@include('cms-ui::components.form.input', [
								'name' => uniqid('email-'),
								'attributes' => [ 'placeholder' => 'Enter your email', 'required' => 'required' ],
								'modificators' => [],
								'component' => 'textarea',
								'classes' => 'form-item--theme-default',
								'mode' => 'email',
							])
						@endcomponent
					</div>
					<div class="_cell _cell--6">
						@component('cms-ui::components.form.form-ajax', ['route' => 'demo.form', 'id' => uniqid('form-')])
							@include('cms-ui::components.form.input', [
								'name' => uniqid('email-'),
								'attributes' => [ 'placeholder' => 'Enter your email', 'required' => 'required' ],
								'modificators' => ['inactive'],
								'component' => 'textarea',
								'classes' => 'form-item--theme-default',
								'mode' => 'email',
							])
						@endcomponent
					</div>
					<div class="_cell _cell--6">
						@php
							$inputmaskConfig = json_encode((object)[
								'mask' => '+38(999)-99-99-999',
								'androidMask' => '+38(999)-99-99-999'
							])
						@endphp
						@component('cms-ui::components.form.form-ajax', ['route' => 'demo.form', 'id' => uniqid('form-')])
							@include('cms-ui::components.form.input', [
								'name' => uniqid('email-'),
								'attributes' => [ 'placeholder' => 'Enter your phone number*', 'data-mask' => $inputmaskConfig, 'class' => 'js-import form-item__control', 'required' => 'phone' ],
								'modificators' => [],
								'component' => 'input',
								'classes' => 'form-item--theme-default',
								'mode' => 'tel'
							])
						@endcomponent
					</div>
				</div>
			</div>
			{{-- svg sprites --}}
			<div class="_pt-lg">
				<hr class="_mtb-def">
				<div class="title title--size-h1">
					SVG
				</div>
				<div>
					<div class="title title--size-h3">
						Common
					</div>
					@include('cms-ui::partials.spritemap-print', [
						'content' => file_get_contents(config('cms.ui.ui.svg.sprite')),
						'source' => asset(config('cms.ui.ui.svg.sprite')),
						'sprite' => 'sprite'
					])
				</div>
                <div>
                    <div class="title title--size-h3">
                        accident_insurance
                    </div>
                    @include('cms-ui::partials.spritemap-print', [
                        'content' => file_get_contents(config('cms.ui.ui.svg.accident_insurance')),
						'source' => asset(config('cms.ui.ui.svg.accident_insurance')),
						'sprite' => 'accident_insurance'
                    ])
                </div>
                <div>
                    <div class="title title--size-h3">
                        agricultural_insurance
                    </div>
                    @include('cms-ui::partials.spritemap-print', [
                        'content' => file_get_contents(config('cms.ui.ui.svg.agricultural_insurance')),
						'source' => asset(config('cms.ui.ui.svg.agricultural_insurance')),
						'sprite' => 'agricultural_insurance'
                    ])
                </div>
                <div>
                    <div class="title title--size-h3">
                        air_transport_insurance
                    </div>
                    @include('cms-ui::partials.spritemap-print', [
                        'content' => file_get_contents(config('cms.ui.ui.svg.air_transport_insurance')),
						'source' => asset(config('cms.ui.ui.svg.air_transport_insurance')),
						'sprite' => 'air_transport_insurance'
                    ])
                </div>
                <div>
                    <div class="title title--size-h3">
                        animal_insurance
                    </div>
                    @include('cms-ui::partials.spritemap-print', [
                        'content' => file_get_contents(config('cms.ui.ui.svg.animal_insurance')),
						'source' => asset(config('cms.ui.ui.svg.animal_insurance')),
						'sprite' => 'animal_insurance'
                    ])
                </div>
                <div>
                    <div class="title title--size-h3">
                        avtograzhdanka
                    </div>
                    @include('cms-ui::partials.spritemap-print', [
                        'content' => file_get_contents(config('cms.ui.ui.svg.avtograzhdanka')),
						'source' => asset(config('cms.ui.ui.svg.avtograzhdanka')),
						'sprite' => 'avtograzhdanka'
                    ])
                </div>
                <div>
                    <div class="title title--size-h3">
                        car_insurance
                    </div>
                    @include('cms-ui::partials.spritemap-print', [
                        'content' => file_get_contents(config('cms.ui.ui.svg.car_insurance')),
						'source' => asset(config('cms.ui.ui.svg.car_insurance')),
						'sprite' => 'car_insurance'
                    ])
                </div>
                <div>
                    <div class="title title--size-h3">
                        cargo_insurance
                    </div>
                    @include('cms-ui::partials.spritemap-print', [
                        'content' => file_get_contents(config('cms.ui.ui.svg.cargo_insurance')),
						'source' => asset(config('cms.ui.ui.svg.cargo_insurance')),
						'sprite' => 'cargo_insurance'
                    ])
                </div>
                <div>
                    <div class="title title--size-h3">
                        construction_and_installation_risks_insurance
                    </div>
                    @include('cms-ui::partials.spritemap-print', [
                        'content' => file_get_contents(config('cms.ui.ui.svg.construction_and_installation_risks_insurance')),
						'source' => asset(config('cms.ui.ui.svg.construction_and_installation_risks_insurance')),
						'sprite' => 'construction_and_installation_risks_insurance'
                    ])
                </div>
                <div>
                    <div class="title title--size-h3">
                        health_insurance
                    </div>
                    @include('cms-ui::partials.spritemap-print', [
                        'content' => file_get_contents(config('cms.ui.ui.svg.health_insurance')),
						'source' => asset(config('cms.ui.ui.svg.health_insurance')),
						'sprite' => 'health_insurance'
                    ])
                </div>
                <div>
                    <div class="title title--size-h3">
                        insurance_of_vegetables_and_fruit
                    </div>
                    @include('cms-ui::partials.spritemap-print', [
                        'content' => file_get_contents(config('cms.ui.ui.svg.insurance_of_vegetables_and_fruit')),
						'source' => asset(config('cms.ui.ui.svg.insurance_of_vegetables_and_fruit')),
						'sprite' => 'insurance_of_vegetables_and_fruit'
                    ])
                </div>
                <div>
                    <div class="title title--size-h3">
                        liability_insurance
                    </div>
                    @include('cms-ui::partials.spritemap-print', [
                        'content' => file_get_contents(config('cms.ui.ui.svg.liability_insurance')),
						'source' => asset(config('cms.ui.ui.svg.liability_insurance')),
						'sprite' => 'liability_insurance'
                    ])
                </div>
                <div>
                    <div class="title title--size-h3">
                        property_insurance
                    </div>
                    @include('cms-ui::partials.spritemap-print', [
                        'content' => file_get_contents(config('cms.ui.ui.svg.property_insurance')),
						'source' => asset(config('cms.ui.ui.svg.property_insurance')),
						'sprite' => 'property_insurance'
                    ])
                </div>
                <div>
                    <div class="title title--size-h3">
                        railway_transport_insurance
                    </div>
                    @include('cms-ui::partials.spritemap-print', [
                        'content' => file_get_contents(config('cms.ui.ui.svg.railway_transport_insurance')),
						'source' => asset(config('cms.ui.ui.svg.railway_transport_insurance')),
						'sprite' => 'railway_transport_insurance'
                    ])
                </div>
                <div>
                    <div class="title title--size-h3">
                        travel_insurance
                    </div>
                    @include('cms-ui::partials.spritemap-print', [
                        'content' => file_get_contents(config('cms.ui.ui.svg.travel_insurance')),
						'source' => asset(config('cms.ui.ui.svg.travel_insurance')),
						'sprite' => 'travel_insurance'
                    ])
                </div>
                <div>
                    <div class="title title--size-h3">
                        water_transport_insurance
                    </div>
                    @include('cms-ui::partials.spritemap-print', [
                        'content' => file_get_contents(config('cms.ui.ui.svg.water_transport_insurance')),
						'source' => asset(config('cms.ui.ui.svg.water_transport_insurance')),
						'sprite' => 'water_transport_insurance'
                    ])
                </div>
			</div>
		</div>
	</div>
	<style>
		.color-block {
			display: flex;
			align-items: safe center;
			justify-content: center;
			height: 5rem;
			text-align: center;
		}
	</style>
    <style>
        ._svg-spritemap {
            margin: 1.6rem 0;
        }

        ._svg-spritemap__cell:hover {
            position: relative;
            z-index: 5;
        }

        ._svg-spritemap__item {
            padding: .5rem;
            fill: currentColor;
        }

        ._svg-spritemap__item:hover {
            background-color: #eee;
            box-shadow: 0 2px 4px rgba(0, 0, 0, .2);
            fill: #000;
        }

        ._svg-spritemap__head {
            border: 1px dashed rgba(0, 0, 0, .15);
            margin-bottom: .75rem;
            background: #fff;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        ._svg-spritemap__name {
            font-style: italic;
            white-space: nowrap;
            text-overflow: ellipsis;
            overflow: hidden;
            display: block;
        }

        ._svg-spritemap__copy {
            color: #7474f9;
            cursor: pointer;
        }

        ._svg-spritemap__copy::after {
            content: '\21b5';
        }

        ._svg-spritemap__copy:hover {
            text-decoration: underline;
            color: #9c9cf5;
        }
    </style>
    <script src="https://cdn.jsdelivr.net/npm/clipboard@2/dist/clipboard.min.js"></script>
    <script>(function (ClipboardJS) {
			if (ClipboardJS) {
				new ClipboardJS('.js-spritemap-clipboard')
			}
		})(window.ClipboardJS)</script>
@endsection
