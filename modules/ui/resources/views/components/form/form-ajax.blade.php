@php

    /**
     * @var string $id
     * @var string $route
     * @var string $class
     * @var string|null $method
     * @var array|null $attrs
     */

    $_method = $method ?? 'post';
    $_classes = implode(' ', array_filter([
    	'form js-import',
    	isset($class) ? $class : false,
    	$_method !== 'get' ? 'ajax-form' : false
    ]));

    $options = array_merge([
        'id' => $id,
        'method' => $_method,
        'class' => $_classes,
    ], $attrs ?? []);

    if (isset($route)) {
        $options['route'] = $route;
    }

    if (isset($url)) {
        $options['url'] = $url;
    }

@endphp

{!! Form::open($options) !!}
    <div class="form__body form__body--preload">
        {!! $slot !!}
    </div>
{!! Form::close() !!}
