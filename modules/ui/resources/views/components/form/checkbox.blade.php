@php

	/**
	* @var array|null $attrs
	* @var array|null $modificators
	* @var string|null $label
	* @var string $name
	* @var string|null $value
	* @var string|null $type
	*/

	$_mainClass = 'checkbox';

	$attributes = array_merge([
		'class' => $_mainClass . '__control',
		'type' => $type ?? 'checkbox',
		'name' => $name,
		'id' => $name,
		'value' => $value ?? null
	], $attributes ?? []);

 	$_bemModifier = '';

	foreach ($modificators ?? [] as $modificator) {
		$_bemModifier = $_bemModifier . ' ' . $_mainClass . '--' . $modificator;
	}

@endphp


<div class="checkbox {{ $_bemModifier }}">
	<input {!! Html::attributes($attributes) !!}>
	<label class="checkbox__label" for="{{ $name }}">
		<span>{{ $label ?? '' }}</span>
		@svg('check', 20, 20, 'checkbox__check')
	</label>
</div>
