@extends('cms-ui::layouts.base', ['lightHeader' => true])

@section('main')
    <div class="error-section">
        <div class="container">
            <div class="_grid _items-center _spacer _spacer--md _justify-center _md:justify-end">
                <div class="_cell _cell--12 _sm:cell--9 _md:cell--6 _df:cell--5 _lg:cell--4 _pb-md">
                    <div class="error-section__title">@yield('code', 404)</div>
                    <div class="error-section__message">@yield('message')</div>
                    @if(starts_with($__env->yieldContent('code', 404), '4'))
                        {{-- @todo перевод--}}
                        <div class="error-section__text">@yield('text', __('cms-ui::site.text come back'))</div>
                        <div class="_flex _md:justify-start _justify-center">
                            <a class="error-section__link" href="{{ route('home') }}">
                                @lang('cms-ui::site.to main page')
                            </a>
                        </div>
                    @endif
                </div>
                <div class="_cell _cell--12 _sm:cell--9 _md:cell--6 _df:cell--7">
                    <div class="error-section__image-wrapper">
                        <div class="error-section__image">
                            <img class="js-import lozad" data-src="{{ url('/static/images/404-image.png') }}" src="{{ url('/images/empty.gif') }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('cms-ui::widgets.feedback-form')
@endsection
