@php
    /**
     * @var $lightHeader = boolean|null
     */

    $lightHeader = $lightHeader ?? false;
@endphp

@extends('cms-ui::layouts.base', ['lightHeader' => $lightHeader])

@section('main')
    @widget('creeping-line')
    <div class="content">
        @yield('content', '')
    </div>
    @includeWhen(trim(strip_tags(SEO::getSeoText())), 'cms-ui::partials.seo-text', ['hideH1' => $__env->getSection('hideH1')])
    @yield('after_content', '')
@endsection
