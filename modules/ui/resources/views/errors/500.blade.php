@extends('cms-ui::layouts.error')

@section('code', '500')

@section('message', __('cms-ui::site.something wrong'))
