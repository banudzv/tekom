@php
    $items = [
          [
              'numb' => '5',
              'txt' => '<span>филиалов</span> на территории Украины'
          ],
          [
              'numb' => '23',
              'txt' => '<span>года опыта</span> в сфере страхования'
          ],
          [
              'numb' => '5200',
              'txt' => '<span>действующих договоров</span>  страхования'
          ],
          [
              'numb' => '37%',
              'txt' => '<span>Уровень выплат</span> нашим клиентам'
          ],
      ];
@endphp
<div class="title title--size-h2 _text-center">
    Почему Теком
</div>
<div class="_grid _flex-wrap">
    @foreach($items as $item)
        @include('cms-ui::widgets.why-item',[
           'to' => $item['numb'],
            'text' => $item['txt'],
      ])
    @endforeach
</div>

