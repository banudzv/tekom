<header class="header js-import header--mobile {{ $lightHeader ? 'header--light' : '' }} {{ $staticHeader ? 'header--static' : '' }} {{ isset($class) && $class ? $class : '' }}" data-sticky-header>
    <div class="container">
        <div class="_flex _justify-between _items-center">
            <div class="_flex _items-center">
                @if(Request::is('/'))
                    <div class="logo logo--header">
                        <svg>
                            <use xlink:href="#logo-symbol"></use>
                        </svg>
                    </div>
                @else
                    <a class="logo logo--header" href="{{ route('home') }}">
                        <svg>
                            <use xlink:href="#logo-symbol"></use>
                        </svg>
                    </a>
                @endif
            </div>
            @widget('favorites:header-button', [], 'cms-favorites::site.widgets.mobile-header-button')
            @widget('orders:cart:header-mobile-button')
            <button type="button" class="mobile-burger" data-mmenu-opener-mobile-nav>
                @svg('menu', null, null, 'mobile-burger__icon')
            </button>
        </div>
    </div>
</header>
