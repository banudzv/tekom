@component('cms-ui::components.form.form-ajax', [
    'url' => route('search'),
    'method' => 'POST',
    'id' => uniqid('form-')
    ])
    <div class="search" data-search-block>
        <label for="search" class="search__title">
            @lang('cms-ui::site.search by site')
        </label>
        <div class="search__field-wrapper">
            <input id="search"
                   name="search"
                   placeholder="{{ __('cms-ui::site.enter what you want to find')}}"
                   type="search"
                   class="search__field">
            <button type="submit" class="search__button">
                @svg('search')
            </button>
        </div>
    </div>
@endcomponent
