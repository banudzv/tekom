@php
    $items = [
          [
              'img' => '/static/images/partn1.png'
          ],
          [
              'img' => '/static/images/partn2.png'
          ],
          [
              'img' => '/static/images/partn2.png'
          ],
          [
              'img' => '/static/images/partn3.png'
          ],
            [
                'img' => '/static/images/partn3.png'
            ],
          [
              'img' => '/static/images/partn4.png'
          ]
      ];
@endphp
<div class="title title--size-h2 _text-center">
    Блог
</div>
<div class="tabs tabs--blog">
    <div class="_grid _mb-df _df:mb-hg _mt-lg">
        @widget('ui:button', [
            'component' => 'button',
            'attrs' => ['data-wstabs-ns' => 'accordion-example-single-2', 'data-wstabs-button' => '4'],
            'text' => 'Перестрахование',
            'modificators' => ['button button--tab button--size-sm _mr-xs']
        ])
        @widget('ui:button', [
            'component' => 'button',
            'attrs' => ['data-wstabs-ns' => 'accordion-example-single-2', 'data-wstabs-button' => '5'],
            'text' => 'Брокеры',
            'modificators' => ['button button--tab button--size-sm _mr-xs']
        ])
        @widget('ui:button', [
            'component' => 'button',
            'attrs' => ['data-wstabs-ns' => 'accordion-example-single-2', 'data-wstabs-button' => '6'],
            'text' => 'Финансы',
            'modificators' => ['button button--tab button--size-sm']
        ])
    </div>
    <div class="tabs__body">
        <div class="wstabs-block" data-wstabs-ns="accordion-example-single-2" data-wstabs-block="4">
            <div class="_flex align-items-center _flex-wrap">
                @foreach($items as $item)
                    @include('cms-ui::widgets.partner-item',[
                           'img' => $item['img']
                      ])
                @endforeach
            </div>
        </div>
        <div class="wstabs-block" data-wstabs-ns="accordion-example-single-2" data-wstabs-block="5">
            <div class="_flex align-items-center _flex-wrap">
                @foreach($items as $item)
                    @include('cms-ui::widgets.partner-item',[
                           'img' => $item['img']
                      ])
                @endforeach
            </div>
        </div>
        <div class="wstabs-block" data-wstabs-ns="accordion-example-single-2" data-wstabs-block="6">
            <div class="_flex align-items-center _flex-wrap">
                @foreach($items as $item)
                    @include('cms-ui::widgets.partner-item',[
                           'img' => $item['img']
                      ])
                @endforeach
            </div>
        </div>
    </div>
</div>

