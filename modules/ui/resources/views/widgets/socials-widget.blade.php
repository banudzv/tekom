@php
    /**
     * @var $fb_messager null|string
     * @var $viber null|string
     */
@endphp
<div class="socials-widget js-import" data-fixed-button data-toggle="socials-widget">
    @if($fb_messenger)
        <noindex>
            <a href="{{ \WezomCms\Contacts\Models\Contact::socialLink('fb_messenger') }}" rel="nofollow noopener" class="socials-widget__link socials-widget__link--theme-fb" target="_blank">
                @svg('fb-messenger')
            </a>
        </noindex>
    @endif
    @if($viber)
        <noindex>
            <a href="{{ \WezomCms\Contacts\Models\Contact::socialLink('viber') }}" rel="nofollow noopener" class="socials-widget__link socials-widget__link--theme-viber" target="_blank">
                @svg('viber')
            </a>
        </noindex>
    @endif
    <button type="button" class="socials-widget__button" data-toggle-trigger="socials-widget">
        @svg('chat')
    </button>
</div>
