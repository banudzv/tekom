<div class="why-item {{$class ?? ''}}">
  <div class="why-item__numb">
     <span class="js-import" data-from="0" data-to="{{$to}}" data-counter data-duration="1">
        0
     </span>
  </div>
    <div class="why-item__text">
        {!! $text ?? '' !!}
    </div>
</div>
