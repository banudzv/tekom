<div class="section section--bg-lighter _py-hg" id="quality-control">
    <div class="container">
        <div class="quality-control">
            <div class="_grid _justify-center">
                <div class="_cell _cell--12 _md:cell--10 _df:cell--8">
                    <div class="quality-control__content">
                        <div class="quality-control__title">
                            @lang('cms-contacts::site.Want to ask a question')
                        </div>
                        <div class="quality-control__description">
                            @lang('cms-contacts::site.If you got questions')
                        </div>

                        @component('cms-ui::components.form.form-ajax', [
                            'class' => 'js-import',
                            'method' => 'POST',
                            'autocomplete' => 'off',
                            'url' => route('contacts.form'),
                            'id' => uniqid('question-'),
                            'attrs' => [
                              'enctype' => 'multipart/form-data'
                            ],
                        ])
                            @csrf
                            <div class="_grid _spacer _spacer--md _mb-md">
                                <div class="_mb-xs _pb-xs _cell--12 _md:cell--6">
                                    @include('cms-ui::components.form.input', [
                                        'name' => 'name',
                                        'attributes' => [ 'placeholder' => __('cms-callbacks::site.Name') ],
                                        'classes' => 'form-item--theme-default',
                                        'mode' => 'text'
                                    ])
                                </div>
                                <div class="_mb-xs _pb-xs _cell--12 _md:cell--6">
                                    @include('cms-ui::components.form.input', [
                                        'name' => 'email',
                                        'attributes' => [ 'placeholder' => __('cms-callbacks::site.Email')],
                                        'classes' => 'form-item--theme-default',
                                        'mode' => 'email'
                                    ])
                                </div>
                                <div class="_mb-xs _pb-xs _cell--12 _md:cell--6">
                                    @php
                                        $inputmaskConfig = json_encode((object)[
                                            'mask' => '+38(999)-99-99-999',
                                            'androidMask' => '+38(999)-99-99-999'
                                        ])
                                    @endphp
                                    @include('cms-ui::components.form.input', [
                                        'name' => 'phone',
                                            'attributes' => [
                                            'placeholder' => __('cms-callbacks::site.Phone'),
                                            'data-mask' => $inputmaskConfig,
                                            'class' => 'js-import form-item__control',
                                            'required',
                                            'data-rule-phone'
                                         ],
                                        'modificators' => [],
                                        'component' => 'input',
                                        'classes' => 'form-item--theme-default',
                                        'mode' => 'tel'
                                    ])
                                </div>
                                <div class="_mb-xs _pb-xs _cell--12 _md:cell--6">
                                    @include('cms-ui::components.form.input', [
                                        'name' => 'contract_number',
                                        'attributes' => [ 'placeholder' => __('cms-callbacks::site.Contract number'), 'required' ],
                                        'component' => 'input',
                                        'classes' => 'form-item--theme-default',
                                        'mode' => 'text'
                                    ])
                                </div>
                                <div class="_mb-xs _cell _cell--12">
                                    @include('cms-ui::components.form.input', [
                                        'name' => 'message',
                                        'attributes' => [ 'placeholder' => __('cms-callbacks::site.Message') ],
                                        'modificators' => [],
                                        'component' => 'textarea',
                                        'classes' => 'form-item--theme-default',
                                        'mode' => 'text',
                                    ])
                                </div>
                            </div>
                            <div class="_flex _justify-center">
                                @widget('ui:button', [
                                'component' => 'button',
                                'text' => __('cms-callbacks::site.Send'),
                                'classes' => '_text-center',
                                'attrs' => ['type' => 'submit'],
                                'modificators' => ['color-accent', 'size-def', 'uppercase', 'ls-def', 'bold']
                                ])
                            </div>
                        @endcomponent
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
