@php
/**
 * @var array $links
 * @var array $current
 */
@endphp
<span class="language-switcher {{ \WezomAgency\Browserizr::detect()->isDesktop() ? '' : 'language-switcher--theme-mobile' }}">
    <span class="language-switcher__current">
        <img src="{{ asset('/static/images/flag-' . str_replace('uk', 'ua', $current['locale']) . '.png') }}" alt="{{ mb_strtoupper(str_replace('uk', 'ua', $current['locale'])) }}">
    </span>
    <span class="language-switcher__dropdown">
        @if(count($links) > 1)
            <ul class="language-switcher__list">
                @foreach($links as $locale => $item)
                    @continue($item['active'])
                    <li class="language-switcher__item">
                        <a href="{{ $item['url'] }}" class="language-switcher__link">
                            <img src="{{ asset('/static/images/flag-' . str_replace('uk', 'ua', $locale)) . '.png' }}" alt="{{ mb_strtoupper(str_replace('uk', 'ua', $locale)) }}">
                        </a>
                    </li>
                @endforeach
            </ul>
        @endif
    </span>
</span>
