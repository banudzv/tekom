@php
    /**
     * @var $hideH1 bool|null
     */
@endphp
{{--@dd(SEO::getSeoText())--}}

@if(SEO::getSeoText())
    <div class="section section--wysiwyg _pt-xxl _pb-xxl _mt-xxl">
        <div class="container">
            <div class="wysiwyg wysiwyg--main js-import {{$class ?? ''}}" data-wrap-media data-draggable-table>
                <div class="wysiwyg__body">
                    {!! SEO::getSeoText() !!}
                </div>
            </div>
        </div>
    </div>
@endif

