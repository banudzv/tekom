<link rel="apple-touch-icon" sizes="180x180" href="{{ url('static/favicons/apple-touch-icon.png') }}">
<link rel="icon" type="image/png" sizes="192x192" href="{{ url('static/favicons/android-chrome-192x192.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ url('static/favicons/android-chrome-36x36.png') }}">
<link rel="manifest" href="{{ url('static/favicons/site.webmanifest') }}">
<meta name="application-name" content="{{ config('app.name') }}">
<meta name="msapplication-config" content="{{ url('static/favicons/browserconfig.xml') }}">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="theme-color" content="#ffffff">
