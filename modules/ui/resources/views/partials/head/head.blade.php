<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">

{!! SEO::generate() !!}
{{--@dd(\Artesaos\SEOTools\Facades\SEOMeta::generate())--}}
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="format-detection" content="telephone=no">
<meta name="format-detection" content="address=no">
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="preload" as="image" href="{{ url('/static/images/placeholders/no-image.png') }}">

@include('cms-ui::partials.head.favicons')
@include('cms-ui::partials.styles')

@foreach($assetManager->getJs(\WezomCms\Core\Contracts\Assets\AssetManagerInterface::POSITION_HEAD) as $script)
    {!! $script !!}
@endforeach
