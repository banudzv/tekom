@php

/**
 * @var string $content
 * @var string $source
 */

    $symbols = [];
    if ($content) {
        preg_match_all('/<symbol\s((?!<).)*>/', $content, $_symbols);
        if ($_symbols) {
            foreach ($_symbols[0] as $_symbol) {
                 preg_match_all('/id="(((?!").)*)"/', $_symbol, $_ids);
                 if ($_ids) {
                    array_push($symbols, $_ids[1][0]);
                 }
            }
        }
    }
    $sprite = $sprite ?? 'sprite';
@endphp
@if (count($symbols))
    <div class="_grid _grid--3 _xs:grid--4 _md:grid--6 _def:grid--10 _lg:grid--12 _nml-xs _svg-spritemap">
    @foreach($symbols as $symbol)
        <div class="_gcell _svg-spritemap__cell _pl-xs _mb-xs">
            <div class="_svg-spritemap__item" title="{{ $symbol }}">
                <div class="_svg-spritemap__head">
					@svg($symbol, 50, 50, '_svg-spritemap__icon', $sprite)
                </div>
                <div class="_text-center">
                    <code id="__icon-{{ $symbol }}"
                            class="_svg-spritemap__name _ellipsis">
                        {{ $symbol }}
                    </code>
                    <code data-clipboard-target="#__icon-{{ $symbol }}"
                            class="_svg-spritemap__copy js-spritemap-clipboard">
                        copy
                    </code>
                </div>
            </div>
        </div>
    @endforeach
    </div>
@endif

