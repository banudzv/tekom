
import { zeroBefore } from '../utils/zero-before';

// ----------------------------------------
// Helper class
// ----------------------------------------

export class CountOf {
	/**
	 * @param {jQuery} $container
	 * @param {Number} [zeroBefore=10]
	 */
	constructor ($container, zeroBefore = 10) {
		this.zeroBefore = zeroBefore;
		this.$current = $container.find('[data-current]');
		this.$amount = $container.find('[data-amount]');
	}

	updateCurrent (number) {
		this.$current.text(zeroBefore(number, this.zeroBefore));
	}

	updateAmount (number) {
		this.$amount.text(zeroBefore(number, this.zeroBefore));
	}
}
