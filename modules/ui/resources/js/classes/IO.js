class IO {
	constructor () {
		this.options = {
			rootMargin: '0px',
			threshold: 0
		};
		this.observer = null;
		this.callbackBefore = function (element, observer) {
			observer.observe(element);
		};
		this.callback = function (entry) {};
		this.callbackAfter = function (element, observer) {
			observer.unobserve(element);
		};
	}

	init ($elements) {
		if (window.IntersectionObserver) {
			this.observer = new window.IntersectionObserver((entries) => {
				entries.forEach(entry => {
					this.callback(entry);
				});
			}, this.options);
			$elements.each((index, element) => {
				this.callbackBefore(element, this.observer);
			});
		}
	}

	setOptions (options) {
		this.options = Object.assign(this.options, options);
	}

	setBefore (callback) {
		this.callbackBefore = callback;
	}

	setCallback (callback) {
		this.callback = callback;
	}

	setAfter (callback) {
		this.callbackAfter = callback;
	}

	observe ($elements, callback) {
		if (window.IntersectionObserver) {
			this.init($elements, callback);
		} else {
			import('intersection-observer').then(() => {
				this.init($elements, callback);
			});
		}
	}

	unobserve ($elements) {
		if (this.observer) {
			$elements.each((index, element) => {
				this.callbackAfter(element);
			});
		}
	}
}

export default IO;
