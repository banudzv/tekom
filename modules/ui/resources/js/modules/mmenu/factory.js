'use strict';

/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import { AbstractMobileMenu } from './abstract';

// ----------------------------------------
// Private
// ----------------------------------------

// ----------------------------------------
// Public
// ----------------------------------------

export class MobileNav extends AbstractMobileMenu {
	get defaults () {
		return {
			options: {
				extensions: [
					'shadow-page',
					'position-right',
					'position-front',
					'pagedim-white',
					'theme-white'
				]
			},
			configuration: {
				offCanvas: {
					page: {
						selector: ''
					}
				}
			},
			customData: {}
		};
	}

	/**
	 * @protected
	 */
	_setup () {
		super._setup();

		if (this.customData.networksCloneSelector) {
			const $networks = $(this.customData.networksCloneSelector).clone();
			this.$container.addClass('mm-custom-networks');
			this.options.navbars = [{
				position: 'bottom',
				height: 1,
				content: $networks
			}];
		}
	}

	/**
	 * @protected
	 */
	_afterInitialize () {
		const iosFIx = function (evt) {
			evt.preventDefault();
			evt.stopPropagation();
		};

		const IOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);

		this.$openButtons.on('click', () => {
			this.api.open();
		});

		if (IOS) {
			this.api.bind('open:before', function () {
				window.addEventListener('touchmove', iosFIx, { passive: false });
			});

			this.api.bind('close:before', function () {
				window.removeEventListener('touchmove', iosFIx, { passive: false });
			});
		}

		this.$container.find('.mm-listview').each((i, list) => {
			const $list = $(list);
			const $tileView = $list.children('[data-js-tile-view]');

			if ($list.children('[data-js-tile-view]').length) {
				$list.addClass($tileView.attr('data-js-tile-view'));
			}
		});
	}
}
