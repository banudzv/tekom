'use strict';

/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import $ from 'jquery';
import 'custom-jquery-methods/fn/get-my-elements';
import { WebPluginInterface } from 'web-plugin-interface';

// ----------------------------------------
// Public
// ----------------------------------------

export class AbstractMobileMenu extends WebPluginInterface {
	/**
	 * @param {jQuery} $container
	 * @param {Object} clientSettings
	 * @param {MobileMenuCustomData} clientSettings.customData
	 * @param {MobileMenuOptions} clientSettings.options
	 * @param {MobileMenuConfiguration} clientSettings.configuration
	 */
	constructor ($container, clientSettings = {}) {
		super();
		this.api = {};
		this.clientSettings = clientSettings;
		/** @type {jQuery} */
		this.$container = $container;
		/** @type {MobileMenuOptions} */
		this.options = {};
		/** @type {MobileMenuOptions} */
		this.configuration = {};
		/** @type {MobileMenuCustomData} */
		this.customData = {};
	}

	/** @protected */
	_setup () {
		this.options = $.extend(true, {}, this.defaults.options, this.clientSettings.options);
		this.configuration = $.extend(true, {}, this.defaults.configuration, this.clientSettings.configuration);
		this.customData = $.extend(true, {}, this.defaults.customData, this.clientSettings.customData);
		super._setup();
	}

	/** @protected */
	_beforeInitialize () {
		this.$openButtons.addClass('is-ready');
	}

	initialize () {
		this._setup();
		this._beforeInitialize();
		this.menu = this.$container.mmenu(this.options, this.configuration);
		this.api = this.menu.data('mmenu');
		this._afterInitialize();
	}

	/** @protected */
	_afterInitialize () {
		super._afterInitialize();
	}

	/**
	 * @type {{options: MobileMenuOptions, configuration: MobileMenuConfiguration, customData: MobileMenuCustomData}}
	 */
	get defaults () {
		return {
			options: {},
			configuration: {},
			customData: {}
		};
	}

	/**
	 * @return {jQuery}
	 */
	get $openButtons () {
		return this.$container.getMyElements('$openButton', this.customData.openButtonSelector);
	}

	/**
	 * @return {jQuery}
	 */
	get $currentLink () {
		return this.$container.getMyElements('$currentLink', this.customData.currentLinkSelector, 'find');
	}

	/**
	 * @return {jQuery}
	 */
	get $currentPanel () {
		if (this.$currentLink.length) {
			return this.$currentLink.last().getMyElements('$currentPanel', '.mm-panel', 'closest');
		}
		return $([]);
	}
}

// ----------------------------------------
// Definitions
// ----------------------------------------

/**
 * @typedef {Object} MobileMenuCustomData
 * @property {string} [currentLinkSelector]
 * @property {string} [networksCloneSelector]
 * @property {string} [networksCloneClasses]
 * @property {string} [openButtonSelector]
 */

/**
 * @typedef {Object} MobileMenuOptions
 *
 * @property {string[]} [extensions] []
 * @property {Object} [hooks] {}
 *
 * @property {Object} [navbar] {}
 * @property {boolean} [navbar.add] true
 * @property {string|null} [navbar.title] null
 * @property {string} [navbar.titleLink] "parent" / "anchor" / "none"
 *
 * @property {Object} [onClick] {}
 * @property {boolean|Function} [onClick.close] false
 * @property {null|boolean|Function} [onClick.preventDefault] null
 * @property {boolean|Function} [onClick.setSelected] true
 *
 * @property {boolean} [slidingSubmenus] true
 *
 * @property {MobileMenuAddonNavbars[]} [navbars]
 *
 * @property {Object|boolean} [offCanvas] {}
 * @property {boolean|string} [offCanvas.blockUI] true
 * @property {boolean} [offCanvas.moveBackground] true
 *
 * @property {Object} [scrollBugFix] {}
 * @property {boolean} [fix] true
 */

/**
 * @typedef {Object} MobileMenuAddonNavbars
 * @property {string|string[]|jQuery} [content] []
 * @property {number} [height] 1
 * @property {string} [position] "top" / "bottom"
 * @property {string} [type] null / "tabs"
 */

/**
 * @typedef {Object} MobileMenuConfiguration
 * @property {Object|boolean} [offCanvas]
 * @property {Object} [offCanvas.menu] {}
 * @property {string} [offCanvas.menu.insertMethod] "prependTo"
 * @property {string} [offCanvas.menu.insertSelector] "body"
 * @property {Object} [offCanvas.page] {}
 * @property {string} [offCanvas.page.nodetype] "div"
 * @property {string} [offCanvas.page.selector] "body > " + this.nodetype
 * @property {string[]} [offCanvas.page.noSelector] []
 * @property {boolean} [offCanvas.page.wrapIfNeed] true
 */
