'use strict';

/**
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import 'custom-jquery-methods/fn/has-inited-key';
import 'custom-jquery-methods/fn/get-my-elements';
import 'jquery.mmenu/dist/jquery.mmenu.js';
import { AbstractMobileMenu } from './abstract';
import * as factory from './factory';

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @param {jQuery} $element
 * @param {string} FactoryName
 * @param {object} factoryExtend
 * @return {AbstractMobileMenu}
 * @private
 */
function _create ($element, FactoryName, factoryExtend) {
	if (FactoryName in factory) {
		return new factory[FactoryName]($element, factoryExtend);
	}
	return new AbstractMobileMenu($element, factoryExtend);
}

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {jQuery} $elements
 */
export default function ($elements) {
	$elements.each((i, element) => {
		const $element = $(element);
		if ($element.hasInitedKey('mMenuIsInitialized')) {
			return true;
		}

		const { factory: FactoryName, factoryExtend } = $element.data('mmenu-init') || {};
		const mmenuInstance = _create($element, FactoryName, factoryExtend);
		mmenuInstance.initialize();
	});
}

// ----------------------------------------
// Definitions
// ----------------------------------------

// описание кастомных типов jsdoc
