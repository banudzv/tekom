import getInnerHeight from 'js#/utils/get-inner-height';

const $document = $(document);
const $window = $(window);

window.toggleInstance = null;

class Toggle {
	constructor ($root) {
		let defaults = {
			closeOnClickOutSide: true,
			closeOnScroll: false,
			onlyOneOpened: false,
			focusInput: false,
			slideTime: 300,
			isSlide: false,
			css: {
				active: 'is-active'
			}
		};

		this.$root = $root;
		this.type = $root.data('toggle');
		this.$trigger = $root.find('[data-toggle-trigger="' + this.type + '"]');
		this.$prevent = $root.find('[data-toggle-prevent="' + this.type + '"]');
		this.$toggleText = this.$trigger.find('[data-toggle-text]').add(this.$trigger.filter('[data-toggle-text]'));
		this.toggleText = this.$toggleText.length ? this.$toggleText.data('toggle-text') : '';
		this.text = this.$toggleText.length ? this.$toggleText.text() : '';
		this.params = $.extend(true, {}, defaults, $root.data('user-config') || {});

		this.ns = 'toggle';
		this.toggleEvent = `click.${this.ns}}`;

		this._isOpen = false;

		this.init();
	}

	init () {
		this.$trigger.on(this.toggleEvent, (e) => {
			let allowToggle =
				!/^label$|^input$/.test(e.target.tagName.toLowerCase()) &&
				!$(e.target).closest('[data-toggle-prevent="' + this.type + '"]', this.$trigger).length;

			if (allowToggle) {
				if (this.params.isSlide) {
					this.$prevent.stop(false, true).slideToggle(this.params.slideTime, () => {
						this.$root.toggleClass(this.params.css.active);
						this._isOpen = true;
						this.params.focusInput && this.focusInput();
						this.params.onlyOneOpened && this.switchInstance();
						this.checkNeedToggle();
						this.replaceText();

						this.$root.find('[data-toggle]').each((index, el) => {
							$(el).trigger('check-toggle');
						});
					});
				} else {
					this.$root.toggleClass(this.params.css.active);
					this._isOpen = true;
					this.params.focusInput && this.focusInput();
					this.params.onlyOneOpened && this.switchInstance();
					this.checkNeedToggle();
					this.replaceText();

					this.$root.find('[data-toggle]').each((index, el) => {
						$(el).trigger('check-toggle');
					});
				}
			}
		});

		if (this.params.closeOnClickOutSide) {
			$document.on(this.toggleEvent, (e) => {
				if (!$(e.target).closest(this.$root).length) {
					this.close();
				}
			});
		}

		this.$root.on('check-toggle', (e) => {
			this.checkNeedToggle();
		});

		this.$root.find('[data-toggle-close]').on('click', () => {
			this.close();
		});

		this.params.closeOnScroll && this.closeOnScroll();

		$(window).on('resize', () => {
			this.checkNeedToggle();
		});

		this.checkNeedToggle();
	}

	close () {
		if (this.params.isSlide) {
			this.$prevent.stop(false, true).slideUp(this.params.slideTime, () => {
				this.$root.removeClass(this.params.css.active);
				this._isOpen = false;
			});
		} else {
			this.$root.removeClass(this.params.css.active);
			this._isOpen = false;
		}
	}

	focusInput () {
		window.setTimeout(() => {
			// Chrome BUG => https://fellowtuts.com/jquery/jquery-focus-not-working-in-chrome/
			this.$root
				.find('input')
				.first()
				.focus();
		}, 50);
	}

	closeOnScroll () {
		$window.on(
			'scroll',
			window.setTimeout(() => {
				this._isOpen && this.close();
			}, 500)
		);
	}

	switchInstance () {
		if (window.toggleInstance && window.toggleInstance !== this) {
			window.toggleInstance.close();
		}
		window.toggleInstance = this;
	}

	replaceText () {
		if (this.toggleText) {
			let text = this.$toggleText.text();
			if (text === this.toggleText) {
				this.$toggleText.html(this.text);
			} else {
				this.$toggleText.html(this.toggleText);
			}
		}
	}

	checkNeedToggle () {
		if (this.$prevent.length && this.params.lineCount) {
			let computedStyle = window.getComputedStyle(this.$prevent.get(0));
			let lineHeight = parseFloat(computedStyle.lineHeight);
			let maxHeight = lineHeight * this.params.lineCount;
			let height = getInnerHeight(this.$prevent);

			this.$prevent.css('max-height', maxHeight + 'px');

			if (height > maxHeight) {
				this.$trigger.addClass(this.params.css.show);
			} else {
				this.$trigger.removeClass(this.params.css.show);
			}
		}
	}
}

export default Toggle;
