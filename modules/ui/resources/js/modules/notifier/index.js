'use strict';

/**
 * @module
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @example
 * import {notifier} from 'assetsSite#/js/modules/notifier';
 * notifier({}).then(noty => noty.show())
 * @param {Object} [options={}]
 * @return {Promise}
 */
export function notifier (options = {}) {
	return new Promise((resolve, reject) => {
		import('./noty-extend.js')
			.then(({ default: Noty }) => {
				resolve(new Noty(options));
			})
			.catch(reject);
	});
}

/**
 * @param {string} [text]
 * @return {Promise}
 */
notifier.success = function (text = 'Successful!') {
	return notifier({
		text,
		type: 'success'
	});
};

/**
 * @param {string} [text]
 * @return {Promise}
 */
notifier.warn = function (text = 'Something went wrong! See DevTools!') {
	console.warn(text);
	return notifier({
		text,
		type: 'error'
	});
};

/**
 * @param {string} [text]
 * @return {Promise}
 */
notifier.error = function (text = 'Something went wrong! See DevTools!') {
	if (text.charAt(0) !== '<') {
		console.error(text);
	}
	return notifier({
		text,
		type: 'error'
	});
};

/**
 * @param {Object} [error={}]
 * @param {string} [text]
 * @return {Promise}
 */
notifier.errorAjax = function (error = {}, text = 'Something went wrong! See DevTools!') {
	const { status = '000', statusText = 'unknown' } = error;

	console.error(text);

	text = [`<div style="font-size: 1.25em;">${status}, ${statusText}!</div>`, `<em>${text}</em>`].join('');
	return notifier.error(text);
};

// ----------------------------------------
// Exports
// ----------------------------------------

export default notifier;

// ----------------------------------------
// Definitions
// ----------------------------------------

// описание кастомных типов jsdoc
