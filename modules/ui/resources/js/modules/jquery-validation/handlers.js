import $ from 'jquery';
// import Modernizr from 'modernizr';

let timeoutId = null;

function checkForm($form, $buttons) {
	const validator = $form.data('validator');
	if (validator.checkForm()) {
		$form.removeClass('form-error');
		$buttons.each(function () {
			const $button = $(this);
			const action = $button.data('ready-action');
			$button.removeClass('is-disabled');

			if (action === 'submit') {
				clearTimeout(timeoutId);
				timeoutId = setTimeout(function () {
					$form.trigger('submit');
				}, 500);
			}
		});
	} else {
		$form.addClass('form-error');
		$buttons.each(function () {
			const $button = $(this);
			$button.addClass('is-disabled');
		});
	}
}

/**
 * @param {JQuery} $form
 * @param {Validator} Validator
 */
function handlers($form, Validator) {
	const method = $form.attr('method') || 'post';

	$form.prop('method', method);
	if ($form.hasClass('ajax-form')) {
		$form.on('submit', (event) => event.preventDefault());
	}

	// if (Modernizr.mobiledevice) {
	// 	// crutch for mobile device for mask caret position with jquery validation
	// 	$form.on('invalid-form', (event, data) => {
	// 		const nativeElement = data.currentElements.filter('[data-mask]').get(0);
	// 		if (nativeElement) {
	// 			setTimeout(() => {
	// 				nativeElement.inputmask.mask(nativeElement);
	// 			}, 100);
	// 		}
	// 	});
	// }

	$form.on('reset', () => {
		$form.trigger('before-validator-reset');
		Validator.resetForm();
	});

	const $buttons = $form.find('[data-ready-action]');
	if ($buttons.length) {
		$form.find('input,textarea,select').on('input paste change', () => {
			checkForm($form, $buttons);
		});
	}

	$form.on('change', 'input[type="file"]', function () {
		const $element = $(this);
		const Validator = $form.data('validator');
		Validator.element(this);

		let $span = $element.closest('[data-upload-file]');
		const $content = $span.find('[data-content]');
		let fileText = $span.data('file-text');
		let emptyText = $span.data('empty-text');
		const maxSize = $span.data('max');
		let fileList = this.files;

		if (fileList.length) {
			$span.removeClass('is-placeholder');

			if (fileList.length === 1) {
				if (maxSize > 10) {
					$element.val('');
					return true;
				}
				let fileName = fileList[0].name;
				let fileSize = (fileList[0].size / 1024 / 1024).toFixed(2);
				$content.html(fileText.replace('%1', $element.attr('multiple') ? fileList.length : fileName).replace('%2', fileSize) + 'MB');
			} else {
				let filesSize = 0;
				for (let i = 0; i < fileList.length; i++) {
					if (fileList[i].size > maxSize) {
						$element.val('');
						return true;
					}
					filesSize += fileList[i].size;
				}
				filesSize = (filesSize / 1024 / 1024).toFixed(2);
				$content.html(fileText.replace('%1', fileList.length).replace('%2', filesSize) + 'MB');
			}
		} else {
			$content.html(emptyText).addClass('is-placeholder');
		}
	});
}

export default handlers;
