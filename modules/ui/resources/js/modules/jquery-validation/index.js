import $ from 'jquery';

import 'jquery-validation';
import 'custom-jquery-methods/fn/has-inited-key';
import './extend/validate-extend';
import handlers from './handlers';
import actions from '../actions';

/**
 * @const {Object}
 */
const configDefault = {
	ignore: ':hidden, .js-ignore',

	get classes() {
		return {
			error: 'has-error',
			isError: 'is-error',
			valid: 'is-valid',
			labelError: 'label-error',
			formError: 'form--error',
			formValid: 'form--valid',
			formPending: 'form--pending'
		};
	},

	get errorClass() {
		return this.classes.error;
	},

	get validClass() {
		return this.classes.error;
	},

	/**
	 * Валидировать элементы при потере фокуса.
	 * Или false или функция
	 * @type {Boolean|Function}
	 * @prop {HTMLElement} element
	 * @prop {Event} event
	 * @see {@link https://jqueryvalidation.org/validate/#onfocusout}
	 */
	onfocusout(element) {
		if (
			element.value.length ||
			element.classList.contains(this.settings.classes.error)
		) {
			this.element(element);
		}
	},

	/**
	 * Валидировать элементы при keyup.
	 * Или false или функция
	 * @type {Boolean|Function}
	 * @prop {HTMLElement} element
	 * @prop {Event} event
	 * @see {@link https://jqueryvalidation.org/validate/#onkeyup}
	 */
	onkeyup(element) {
		if (element.classList.contains(this.settings.classes.error)) {
			this.element(element);
		}
	},

	/**
	 * Подсветка элементов с ошибками
	 * @param {HTMLElement} element
	 */
	highlight(element) {
		element.classList.remove(this.settings.classes.valid);
		element.classList.add(this.settings.classes.error);
		$(element).parent().addClass(this.settings.classes.isError);
	},

	/**
	 * Удаление подсветки элементов с ошибками
	 * @param {HTMLElement} element
	 */
	unhighlight(element) {
		const doClass =
			typeof element.value === 'string' && element.value.length ? 'add' : 'remove';

		element.classList.remove(this.settings.classes.error);
		$(element).parent().removeClass(this.settings.classes.isError);
		element.classList[doClass](this.settings.classes.valid);
	},

	/**
	 * Обработчик сабмита формы
	 * @param {HTMLFormElement} form
	 * @returns {boolean}
	 */
	submitHandler(form) {
		const $form = $(form);

		const ajaxForm = $form.hasClass('ajax-form');
		if (!ajaxForm) {
			return true;
		}

		const url = $form.attr('action');
		const data = new window.FormData($form[0]);
		const pendingClass = $form.data('validator').settings.classes.formPending;

		$form.addClass(pendingClass);

		if ($form.data('files')) {
			const files = $form.data('files');
			for (let i = 0; i < files.length; i++) {
				data.append(files[i].name, files[i].file);
			}
		}

		$.ajax({
			url,
			data,
			processData: false,
			contentType: false,
			method: $form.attr('method') || 'post'
		})
			.done(function (data) {
				if (typeof data === 'string') {
					data = JSON.parse(data);
				}
				actions($form, data).then(() => {
					$form.removeClass(pendingClass);
				});
			})
			.fail((err) => {
				console.log(err);
			})
			.always(function () {
				$form.removeClass(pendingClass);
			});

		return false;
	}
};

/**
 * @param response
 * @returns {any}
 */
// eslint-disable-next-line no-unused-vars
function checkResponse(response) {
	if (typeof response === 'string' && response.length) {
		return JSON.parse(response);
	} else if (Object.prototype.toString.call(response) === '[object Object]') {
		return response;
	}

	throw new SyntaxError('Bad JSON');
}

/**
 * @param {JQuery} $elements
 */
function validate($elements) {
	$elements.each((i, el) => {
		const $form = $(el);

		if ($form.hasInitedKey('validate-inited')) {
			return true;
		}

		const config = $.extend(true, {}, configDefault);

		$form.validate(config);
		handlers($form, $form.data('validator'));
	});
}

export default validate;
