import $ from 'jquery';

export default function ($elements) {
	$elements.each((i, el) => {
		const $el = $(el);

		if ($el.hasInitedKey('branchesIsInitialized')) {
			return true;
		}

		init($el);
	});
}

function init ($container) {
	const $buttons = $container.find('[data-branches-button]');
	const $blocks = $container.find('[data-branches-tooltip]');
	const setActive = (index) => {
		$blocks.each((i, el) => {
			if ($(el).data('branches-tooltip') === index) {
				$(el).addClass('is-active');
				el._tippy.show();
			} else {
				$(el).removeClass('is-active');
				el._tippy.hide();
			}
		}) ;
	};

	const activeIndex = $buttons.filter((i, el) => $(el).hasClass('is-active')).data('branches-button');
	setActive(activeIndex);

	$buttons.on('click', (e) => {
		const $this = $(e.currentTarget);
		const index = $this.data('branches-button');
		$buttons.removeClass('is-active');
		$this.addClass('is-active');
		setActive(index);
	});

	$blocks.on('click', (e) => {
		const $this = $(e.currentTarget);
		const index = $this.data('branches-tooltip');
		$buttons.filter((i, el) => $(el).data('branches-button') === index).trigger('click');
	});
}
