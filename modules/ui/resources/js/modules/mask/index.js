import Inputmask from 'inputmask';
import Browserizr from 'browserizr';
import 'custom-jquery-methods/fn/has-inited-key';

export default function ($elements) {
	$elements.each((i, el) => {
		const $el = $(el);

		if ($el.hasInitedKey('inputmaskInitialized')) {
			return true;
		}

		_init($el);
	});
}

function _init ($element) {
	const {
		mask,
		androidMask
	} = $element.data('mask');

	const instance = new Inputmask({
		'mask': Browserizr.detect().isAndroid() ? androidMask : mask,
		'showMaskOnHover': false
	});

	instance.mask('[data-mask]');
}
