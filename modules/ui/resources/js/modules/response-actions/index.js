/** @see https://sweetalert2.github.io/ */

// import { ModulesImports } from '@/modules-imports';
import { isObject } from 'js#/utils/is-object';
import { isObjectEmpty } from 'js#/utils/is-object-empty';

class ResponseActions {
	constructor(response) {
		this.response = isObject(response) && !isObjectEmpty(response) ? response : null;
	}

	/**
	 * Run process constructor data
	 * @public
	 * */
	process() {
		if (!this.response) {
			return Promise.resolve();
		}

		const { notifications, errors, message } = this.response;

		if (notifications && notifications.length) {
			return ResponseActions.showNotices(notifications).then(() => {
				this.additionalActions(this.response);
			});
		} else {
			this.additionalActions(this.response);
		}

		if (errors && !isObjectEmpty(errors)) {
			return ResponseActions.showNotices([
				{
					title: message,
					icon: 'error',
					toast: true,
					html: this.groupErrorList(errors),
					position: 'top-right',
					customClass: 'swal2-column'
				}
			]);
		} else if (message) {
			return ResponseActions.showNotice(message);
		}

		return Promise.resolve();
	}

	/**
	 * @param {Object} errors
	 */
	groupErrorList(errors) {
		let html = '';

		html += '<ul class="swal2-error-list">';

		for (const k in errors) {
			if (errors.hasOwnProperty(k)) {
				const errorsList = errors[k];

				for (let i = 0; i < errorsList.length; i++) {
					html +=
						'<li class="swal2-error-list__item">' + errorsList[i] + '</li>';
				}
			}
		}

		html += '</ul>';

		return html;
	}

	additionalActions(response) {
		const { reload, redirect } = response;

		if (reload) {
			window.location.reload();
		}

		if (typeof redirect === 'string' && redirect.length) {
			window.location.href = redirect;
		}
	}

	/**
	 * @param {string|Object} title
	 * @param {string} text
	 * @param {string} icon
	 * @return {promise|boolean}
	 * @static
	 * */
	static showNotice(title, text = '', icon = '') {
		const firstArgument = arguments[0];

		if (isObject(firstArgument)) {
			if (isObjectEmpty(firstArgument)) {
				return Promise.resolve();
			}

			return ResponseActions.importPlugin().then((Plugin) => {
				Plugin.close();
				Plugin = new Plugin(firstArgument);

				return Plugin;
			});
		}

		if (typeof title !== 'string') {
			return false;
		}

		return ResponseActions.showNotices([
			{
				title,
				text,
				icon,
				toast: true,
				position: 'top-end',
				timer: 3500
			}
		]);
	}

	/**
	 * Showed queue messages.
	 *
	 * @param {array|object} notices - array or object setting messages
	 * @return {promise}
	 * @static
	 * */
	static showNotices(notices = []) {
		if (!isObject(notices)) {
			throw new Error('The argument must be an array or object');
		}

		if (!Array.isArray(notices)) {
			notices = [notices];
		}

		if (!notices.length) {
			console.warn('Notices is empty');
		}

		return ResponseActions.importPlugin().then((Plugin) => {
			Plugin.close();

			async function show() {
				for (let i = 0; i < notices.length; i++) {
					await new Plugin(ResponseActions.modifyParams(notices[i]));
				}
			}

			return show();
		});
	}

	/**
	 * Modify params from server depending on the plugin
	 *
	 * @param {object} setting - object params for plugin
	 * @return {object}
	 * @static
	 * */
	static modifyParams(setting) {
		for (const k in setting) {
			if (setting.hasOwnProperty(k)) {
				if (k === 'time') {
					setting.timer = setting[k];

					delete setting[k];
				}
			}
		}

		return setting;
	}

	/**
	 * Webpack dynamic import plugins
	 *
	 * @return {promise} with function plugin
	 * @static
	 * */
	static importPlugin() {
		return import(
			/* webpackChunkName: 'sweetalert-styles' */ './styles.scss'
		).then(() =>
			import(
				/* webpackChunkName: 'sweetalert2' */ 'sweetalert2/dist/sweetalert2.js'
			).then((plugin) => plugin.default)
		);
	}
}

export { ResponseActions };
