'use strict';

/**
 * Расширение дефолтных параметров плагина
 * @module
 */

import $ from 'jquery';

// ----------------------------------------
// Public
// ----------------------------------------

export function extend (translations) {
	const classes = ['mfp-animate-zoom-in'];
	const $html = $('html');

	$.extend(true, $.magnificPopup.defaults, {
		mainClass: classes.join(' '),
		autoFocusLast: false,
		removalDelay: 300,
		// closeBtnInside: true,
		fixedBgPos: true,
		fixedContentPos: true,
		callbacks: {
			open: function () {
				$html.addClass('mfp-is-opened');

				this.contentContainer.find('[data-mfp-close]').on('click', () => {
					this.close();
				});
			},
			close: function () {
				$html.removeClass('mfp-is-opened');
				$(document).trigger('mfp-close');
			}
		}
	});

	if (translations) {
		$.extend(true, $.magnificPopup.defaults, {
			closeMarkup: '<div title="%title%" class="mfp-close"></div>',
			tClose: translations.tClose,
			tLoading: translations.tLoading,
			gallery: {
				tPrev: translations.tPrev,
				tNext: translations.tNext,
				tCounter: translations.tCounter
			},
			image: {
				tError: translations.tErrorImage
			},
			ajax: {
				tError: translations.tError
			},
			inline: {
				tNotFound: translations.tNotFound
			}
		});
	}
}
