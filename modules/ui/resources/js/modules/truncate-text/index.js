/**
 * @param {jQuery} $elements
 */

export default function ($elements) {
	function _truncate (el, len) {
		const str = el.innerHTML;
		const wrapper = document.createElement('span');
		let newStr = str;
		const words = str.split(' ');

		el.innerHTML = '';
		el.appendChild(wrapper);

		function test (elHeight) {
			return wrapper.offsetHeight > elHeight;
		}

		const elHeight = el.offsetHeight;

		wrapper.innerHTML = str;

		if (test(elHeight)) {
			do {
				newStr = newStr.slice(0, newStr.length - 1);
				wrapper.innerHTML = newStr;
			} while (test(elHeight));
			let newWords = newStr.split(' ');
			let lastWordIndex = newWords.length - 1;
			if (words[lastWordIndex].length > newWords[lastWordIndex].length) {
				newWords.pop();
				newStr = newWords.join(' ');
			}
			newStr = newStr + ' ...';
			wrapper.innerHTML = newStr;
		}
	}

	function init ($els) {
		$els.each(function () {
			const $el = $(this);

			if (!$el.hasClass('truncate-inited')) {
				_truncate($el[0]);
				$el.addClass('truncate-inited');
			}
		});
	}

	init($elements);
}
