import $ from 'jquery';

export default function ($elements) {
	$elements.each((i, el) => {
		const $el = $(el);

		if ($el.hasInitedKey('uploadFileInitialized')) {
			return true;
		}

		init($el);
	});
}

function init ($element) {
	const $control = $element.find('[data-upload-file-control]');
	const $area = $element.find('[data-upload-file-area]');
	const sizeErrorMessage = $control.data('size-exceeded-message');
	const maxSize = $control.data('max-size') || 15;

	$control.on('change', (e) => {
		const file = e.target.files[0];

		if (file.size > maxSize * Math.pow(1024,2)) {
			$element.addClass('has-error');
			$area.html(`<span class="file-upload__error">${sizeErrorMessage}</span>`);
			$control.val('');
		} else {
			$area.html(`<span class="file-upload-preview__name">${file.name}</span>`);
		}
	});
}
