import $ from 'jquery';
import throttle from 'lodash.throttle';

export default function ($elements) {
	$elements.each((i, el) => {
		const $el = $(el);

		if ($el.hasInitedKey('navTabsInitialized')) {
			return true;
		}

		init($el);
	});
}

function init ($element) {
	const $buttons = $element.find('[data-nav-tab]');

	const calculateActiveTab = () => {
		$buttons.each((i, el) => {
			const $this = $(el);
			const $block = $($this.data('scroll-window'));

			if ($block.length) {
				const scrollTop = $(window).scrollTop();
				const blockOffset = $block.offset().top;
				const windowHeight = window.innerHeight;

				if (scrollTop + windowHeight / 2 > blockOffset) {
					$buttons.removeClass('is-active');
					$this.addClass('is-active');
				}
			}
		});
	};
	calculateActiveTab();
	$(window).on('scroll', throttle(calculateActiveTab, 100));
}
