import $ from 'jquery';
import 'custom-jquery-methods/fn/has-inited-key';
import throttle from 'lodash.throttle';

export default function ($elements) {
	$elements.each((i, el) => {
		const $el = $(el);

		if ($el.hasInitedKey('headerInitialized')) {
			return true;
		}

		_init($el);
	});
}

function _init ($element) {
	const $links = $element.find('[data-dropdown-link]');
	const $blocks = $element.find('[data-dropdown-menu]');
	const $burger = $element.find('[data-burger]');
	let isActive = false;

	const setActive = (e) => {
		isActive = true;
		const $this = $(e.currentTarget);
		const selector = $this.data('dropdown-selector');

		const burgerActive = $burger.hasClass('is-active');
		if (selector === 'burger' && burgerActive) {
			setClosed();
		} else {
			$links.removeClass('is-active');
			$burger.removeClass('is-active');
			$this.addClass('is-active');
			$element.addClass('is-active');
			$blocks.removeClass('is-active')
				.filter((i, el) => $(el).data('dropdown-menu') === selector)
				.addClass('is-active');
		}
	};

	$burger.on('click', setActive);

	$links.on('mouseenter', setActive);

	$element.on('mouseleave', () => {
		setClosed();
	});

	const setClosed = () => {
		isActive = false;
		$burger.removeClass('is-active');
		$links.removeClass('is-active');
		$blocks.removeClass('is-active');
		$element.removeClass('is-active');
	};

	$(window).on('scroll', throttle(() => {
		if (isActive && $element.hasClass('is-page-scrolled')) {
			setClosed();
		}
	}, 100));
}
