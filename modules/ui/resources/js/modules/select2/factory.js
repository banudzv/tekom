'use strict';

/**
 * @module
 * @author Oleg Dutchenko <dutchenko.o.dev@gmail.com>
 * @version 1.0.0
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import { AbstractSelect2 } from './abstract';

// ----------------------------------------
// Public
// ----------------------------------------

export class DefaultSelect2 extends AbstractSelect2 {
	/**
	 * @type {Select2Props}
	 */
	get defaultProps () {
		return super.defaultProps;
	}

	/**
	 * @type {Select2Settings}
	 */
	get defaultSettings () {
		let settings = {
			containerCssClass: 'custom-select2__select-container',
			dropdownCssClass: 'custom-select2__select-dropdown',
			minimumResultsForSearch: 1,
			width: '100%'
		};

		if (this.props.url) {
			const options = this.props.options || {};

			settings.ajax = {
				url: this.props.url,
				method: 'post',
				dataType: 'json',
				delay: 250,
				data: function (params) {
					let param = {
						query: params.term
					};
					for (let key in options) {
						param[key] = options[key];
					}
					return param;
				},
				processResults: function (data, params) {
					return {
						results: data
					};
				}
			};
		}

		return Object.assign(settings, this.props);
	}

	/**
	 * @protected
	 */
	_afterInitialize () {
		const $control = this.$container.closest('[data-control]');

		this.$select.on('select2:opening', () => {
			this.$container.addClass('custom-select2--focused');
			$control.addClass('in-focus');
		});
		this.$select.on('select2:close', () => {
			this.$container.removeClass('custom-select2--focused');
			$control.removeClass('in-focus');
		});
		this.$select.on('select2:select', () => {
			this.$container.addClass('custom-select2--selected');
			$control.addClass('has-value');
		});
		this.$select.on('select2:unselect', () => {
			this.$container.removeClass('custom-select2--selected');
			$control.removeClass('has-value');
		});

		if (this.$select.val().length) {
			this.$container.addClass('custom-select2--selected');
			$control.addClass('has-value');
		}
	}
}

export class SelectWithLinks extends AbstractSelect2 {
	_afterInitialize () {
		this.$select.on('select2:select', () => {
			window.location = this.$select.find('option:selected').data('url-to-go');
		});
	}
}
