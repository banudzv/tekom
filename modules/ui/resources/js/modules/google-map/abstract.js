// ----------------------------------------
// Imports
// ----------------------------------------

import $ from 'jquery';
import { WebPluginInterface } from 'web-plugin-interface';
import { getExternalJsFile } from 'js#/utils/get-external-js-file';

// ----------------------------------------
// Public
// ----------------------------------------

export class GoogleMapPluginAbstract extends WebPluginInterface {
	constructor($container, clientSettings = {}, clientProps = {}) {
		super();
		this.$container = $container;
		this.mapElement = $container[0];
		this.map = null;
		this.markers = [];
		this.markersData = clientProps.markersData || [];
		this.settings = {};
		this.clientSettings = clientSettings;
		this.props = {};
		this.clientProps = clientProps;
	}

	get defaultSettings() {
		return {
			zoom: 10,
			streetViewControl: false,
			mapTypeControl: false
		};
	}

	get defaultProps() {
		return {};
	}

	_setup() {
		this.props = $.extend({}, this.defaultProps, this.clientProps);
		this.settings = $.extend({}, this.defaultSettings, this.clientSettings);

		delete this.clientProps;
		delete this.clientSettings;
	}

	_beforeInitialize() {
		super._beforeInitialize();
	}

	_afterInitialize() {
		super._afterInitialize();
	}

	_initialize() {
		this._buildMap();
		this._buildMarkers();
	}

	initialize() {
		this._loadApi().then(() => {
			this._setup();
			this._loadMarkers().then(() => {
				this._beforeInitialize();
				this._initialize();
				this._afterInitialize();
			});
		});
	}

	_loadMarkers() {
		if (this.props.hasOwnProperty('markersJson')) {
			return $.ajax(this.props.markersJson, {})
				.then((data) => {
					this.markersData = data;
				})
				.catch(() => {
					// notifier.errorAjax(error).then((noty) => noty.show());
				});
		}
		return Promise.resolve({});
	}

	_loadApi() {
		const { source, query } = window.env.maps;
		return getExternalJsFile(source, query);
	}

	_buildMap() {
		const { Map } = window.google.maps;
		this.map = new Map(this.mapElement, this.settings);
	}

	_buildMarkers() {
		const { Marker, InfoWindow, Size, LatLngBounds, LatLng } = window.google.maps;
		// const infowindow = new InfoWindow();
		const { markerUrl } = this.props;
		const bounds = new LatLngBounds();
		this.markersData.forEach((data) => {
			const { _infoWindowContent, icon = {}, position, ...rest } = data;

			if (this.markersData.length > 1) {
				bounds.extend(new LatLng(position.lat, position.lng));
			}

			const marker = new Marker({
				icon: {
					url: markerUrl,
					size: new Size(28, 36),
					scaledSize: new Size(28, 36),
					...icon
				},
				position,
				...rest
			});

			marker.setMap(this.map);
			if (this.markersData.length > 1) {
				this.map.fitBounds(bounds);
			} else {
				this.map.setCenter(position);
			}

			// marker.addListener('click', () => {
			// 	infowindow.close();
			// 	infowindow.setContent(_infoWindowContent);
			// 	infowindow.open(this.map, marker);
			// });

			this.markers.push(marker);
		});
	}
}
