/**
 * Дофелтный конфиг для всех экземпляров прелоадера
 * @type {Object}
 * @prop {string} mainClass - основной CSS класс, который добавляется к контейнеру прелодара
 * @prop {string} showClass - CSS класс, который добавляется к контейнеру прелодара, при его показе
 * @prop {string} hideClass - CSS класс, который добавляется к контейнеру прелодара, перед его закрытием
 * @prop {number} removeDelay - время задержки, после которой будут удалены разметка прелоадре и CSS классы у контейнера
 * @prop {string} markup - Разметка прелодера
 * @private
 */
let defaultConfig = {
	mainClass: 'preloader',
	showClass: 'preloader--show',
	hideClass: 'preloader--hide',
	removeDelay: 300,
	markup: '<div class="preloader__block"></div>'
};

/**
 * Создание прелодаров
 * @param {JQuery} $element - контейнер для прелоадера
 * @param {Object} [userConfig={}] - Пользвательский конфиг для создваемого экземпляра
 * @prop {JQuery} [$element] - контейнер для прелоадера
 * @prop {Object} [config] - прелоадера
 * @prop {Object|number} timer - id таймера
 * @prop {boolean} empty
 * @example
 * let preloader = new Preloader($myElement);
 * preloader.show();
 * window.setTimeout(() => preloader.hide(), 1000);
 */
class Preloader {
	constructor ($element, userConfig = {}) {
		if (!($element && $element.length)) {
			this.empty = true;
			return this;
		}
		if ($element.data('preloader') instanceof Preloader) {
			$element.data('preloader', null);
		}
		this.$element = $element;
		this.config = $.extend(true, {}, Preloader.config, userConfig);
		this.$element.data('preloader', this);
		this.empty = false;
		this.timer = null;
	}

	/**
	 * Показ прелодера
	 * @param {string} [place="append"] - append / prepend / before / after
	 */
	show (place = 'append') {
		if (this.empty) {
			console.warn('Empty preloader cannot be shown!');
			return false;
		}

		const { mainClass, showClass, markup } = this.config;

		this.$element.each((i, el) => {
			const $el = $(el);

			$el.addClass(mainClass);
			$el.data('$preloaderMarkup', $(markup));
			$el[place]($el.data('$preloaderMarkup'));
			window.setTimeout(() => $el.addClass(showClass), 10);
		});
	}

	/**
	 * Скрытие прелодера
	 * @param {boolean} [removeMarkup=true] - удалить разметку прелодера после скрытия
	 */
	hide (removeMarkup = true) {
		if (this.empty) {
			console.warn('Empty preloader cannot be hidden!');
			return false;
		}

		const { mainClass, showClass, hideClass } = this.config;

		this.$element.addClass(hideClass);

		clearTimeout(this.timer);

		this.timer = setTimeout(() => {
			if (removeMarkup) {
				this.$element.each((i, el) => {
					const markup = $(el).data('$preloaderMarkup');
					if (markup) {
						markup.remove();
					}
				});
			}

			this.$element.removeClass([mainClass, showClass, hideClass]);
		}, this.config.removeDelay);
	}

	/**
	 * Получение глобального конфига для всех прелодаров
	 * @returns {Object}
	 */
	static get config () {
		return defaultConfig;
	}

	/**
	 * Установка глобального конфига
	 * @param {Object} value
	 * @example
	 * Preloader.config = {mainClass: 'my-preloader'}
	 */
	static set config (value) {
		$.extend(true, defaultConfig, value);
	}
}

export default Preloader;
