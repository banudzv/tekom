'use strict';

/**
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import $ from 'jquery';
import 'custom-jquery-methods/fn/has-inited-key';
import 'custom-jquery-methods/fn/get-my-elements';
import { AbstractSlickCarousel } from 'web-plugin-abstract-slick-carousel';
import * as factory from './factory';

// ----------------------------------------
// Private
// ----------------------------------------

/**
 * @param {jQuery} $element
 * @param {string} FactoryName
 * @param {object} factoryExtend
 * @return {AbstractSlickCarousel}
 * @private
 */
function _create ($element, FactoryName, factoryExtend) {
	if (FactoryName in factory) {
		return new factory[FactoryName]($element, factoryExtend);
	}
	return new AbstractSlickCarousel($element, factoryExtend);
}

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {jQuery} $elements
 */
export default function ($elements) {
	if (typeof window.jQuery.type !== 'function') {
		window.jQuery.type = window.jqueryType;
	}

	const each = (i, element) => {
		const $element = $(element);
		if ($element.hasInitedKey('slickCarouselIsInitialized')) {
			return true;
		}

		const { factory: FactoryName, factoryExtend } = $element.data('slick-carousel') || {};
		const carousel = _create($element, FactoryName, factoryExtend);
		carousel.initialize();
	};
	$elements.each(each);
}

// ----------------------------------------
// Definitions
// ----------------------------------------

// описание кастомных типов jsdoc
