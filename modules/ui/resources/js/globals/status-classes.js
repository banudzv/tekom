'use strict';

/**
 * @module
 */

// ----------------------------------------
// Public
// ----------------------------------------

export const statusClasses = {
	documentIsReady: 'document-is-ready',
	windowIsLoaded: 'window-is-loaded',
	windowUnderScroll: 'window-under-scroll',
	isActive: 'is-active',
	isReady: 'is-ready',
	isOpen: 'is-open',
	isShown: 'is-shown'
};
