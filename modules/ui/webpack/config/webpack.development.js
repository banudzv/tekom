/**
 * @fileOverview Конфигурация webpack под dev режим разработки
 */

const { join } = require('path');
const { merge } = require('webpack-merge');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const paths = require('./paths');
const browsersEnv = require('./browsers-env');
const commonWebpackConfig = require('../../../../webpack.config');
const { getPostcssPlugins } = require('../utils/get-postcss-options');
const { getBrowserslistQueries } = require('../utils/get-browserslist');

// Должно совпадать со значением в `.browserslistrc`
const env = browsersEnv.development;

/**
 * Составляем webpack конфиг заточенный под инкрементальную сборку в dev режиме
 * @type {Configuration}
 */
module.exports = merge(commonWebpackConfig, {
	mode: 'development',
	devtool: 'inline-source-map',
	watch: false,
	entry: {
		app: join(paths.src.js, 'app.js'),
		generic: join(paths.src.sass, 'generic/_all-generic.scss'),
		elements: join(paths.src.sass, 'elements/_all-elements.scss'),
		objects: join(paths.src.sass, 'objects/_all-objects.scss'),
		components: join(paths.src.sass, 'components/_all-components.scss'),
		utilities: join(paths.src.sass, 'utilities/_all-utilities.scss')
	},
	output: {
		path: paths.dist.dev,
		publicPath: paths.public.dev,
		filename: '[name].bundle.js'
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				use: [
					{
						loader: 'babel-loader',
						options: {
							cacheDirectory: true,
							presets: [
								[
									'@babel/preset-env',
									{
										targets: getBrowserslistQueries({ env })
									}
								]
							]
						}
					}
				]
			},
			{
				test: /\.scss$/,
				use: [
					MiniCssExtractPlugin.loader,
					{
						loader: 'css-loader',
						options: {
							sourceMap: true,
							importLoaders: 1,
							url: false
						}
					},
					{
						loader: 'postcss-loader',
						options: {
							sourceMap: true,
							postcssOptions: {
								plugins: getPostcssPlugins(env)
							}
						}
					},
					{
						loader: 'sass-loader',
						options: {
							sourceMap: true,
							implementation: require('sass'),
							additionalData: [
								`$ENV: '${env}';`,
								`$STATIC_PATH: '/';`,
								`$PUBLIC_PATH: '${paths.public.base}';`
							].join('\n')
						}
					}
				]
			}
		]
	},
	plugins: [
		new CleanWebpackPlugin(),
		new MiniCssExtractPlugin({
			filename: '[name].bundle.css'
		}),
		new ManifestPlugin({
			basePath: paths.public.base
		})
	],
	stats: {
		all: undefined,
		chunks: false,
		chunkGroups: false,
		modules: false,
		assets: true,
		errors: true,
		warnings: true
	}
});
