/**
 * @fileOverview Конфигурация webpack под dev режим разработки
 */

const { join } = require('path');
const { merge } = require('webpack-merge');
const { config: dotEnvConfig } = require('dotenv');
const paths = require('./paths');
const devWebpackConfig = require('./webpack.development');

/**
 * @typedef DotenvParseOutput
 * @property {string} APP_URL
 * @property {string} UI_DEV_SERVER_HOST
 * @property {string} UI_DEV_SERVER_PORT
 * @property {string} UI_DEV_SERVER_OPEN
 */

/**
 * @type {DotenvParseOutput}
 */
const dotEnv = dotEnvConfig().parsed;
const clientURL = `http://${dotEnv.UI_DEV_SERVER_HOST}:${dotEnv.UI_DEV_SERVER_PORT}`;

/**
 * @param file
 * @return {[string, string, string]}
 */
const devEntryJS = (file) => [
	`webpack-dev-server/client?${clientURL}`,
	'webpack/hot/only-dev-server',
	join(paths.src.js, file)
];

/**
 * @param file
 * @return {[string, string, string]}
 */
const devEntrySASS = (file) => [
	`webpack-dev-server/client?${clientURL}`,
	'webpack/hot/only-dev-server',
	join(paths.src.sass, file)
];

console.log(devEntryJS, devEntrySASS);

/**
 * Составляем webpack конфиг заточенный под сборку c dev-server'ом
 * @type {Configuration}
 */
module.exports = merge(devWebpackConfig, {
	watch: false,
	devServer: {
		hot: true,
		inline: true,
		quiet: false,
		overlay: false,
		host: dotEnv.UI_DEV_SERVER_HOST,
		open: JSON.parse(dotEnv.UI_DEV_SERVER_OPEN),
		port: JSON.parse(dotEnv.UI_DEV_SERVER_PORT),
		contentBase: paths.dist.base,
		proxy: {
			'*': {
				target: dotEnv.APP_URL,
				changeOrigin: true
			}
		},
		disableHostCheck: true,
		headers: {
			'Access-Control-Allow-Origin': '*'
		},
		historyApiFallback: true,
		writeToDisk: true
	}
});
