<?php

namespace WezomCms\Sitemap;

use Illuminate\Support\Traits\Localizable;
use Mcamara\LaravelLocalization\LaravelLocalization;
use WezomCms\Core\Contracts\SitemapXmlGeneratorInterface;
use XMLWriter;

class SitemapXmlGenerator implements SitemapXmlGeneratorInterface
{
    use Localizable;

    /**
     * @var XMLWriter
     */
    protected $xml;

    private $file;

    /**
     * @var array
     */
    private $locales;

    /**
     * Sitemap constructor.
     * @param  string  $file
     * @param  array  $locales
     * @param  LaravelLocalization  $laravelLocalization
     */
    public function __construct(string $file, array $locales, LaravelLocalization $laravelLocalization)
    {
        $this->file = $file;

        // Set default locale first
        $defaultLocale = $laravelLocalization->getDefaultLocale();
        uksort($locales, function ($item) use ($defaultLocale) {
            return $item === $defaultLocale ? -1 : 1;
        });

        $this->locales = $locales;
    }

    /**
     * @return $this
     */
    public function start(): SitemapXmlGeneratorInterface
    {
        $this->xml = new XMLWriter();
        $this->xml->openUri($this->file);

        $this->xml->startDocument('1.0', 'utf-8');
        $this->xml->startElement('urlset');
        $this->xml->writeAttribute('xmlns', 'https://www.sitemaps.org/schemas/sitemap/0.9');
        $this->xml->writeAttributeNs('xmlns', 'xhtml', null, 'https://www.w3.org/1999/xhtml');

        return $this;
    }

    /**
     * @param  string|array|mixed|callable  $url
     * @return SitemapXmlGeneratorInterface
     */
    public function add($url): SitemapXmlGeneratorInterface
    {
        $url = is_iterable($url) ? $url : func_get_args();

        foreach ($url as $item) {
            if (is_callable($item)) {
                $rows = [];
                foreach ($this->locales as $locale => $language) {
                    $this->withLocale($locale, function () use (&$rows, $locale, $item) {
                        $result = call_user_func($item, $locale);

                        foreach (is_iterable($result) ? $result : [$result] as $key => $value) {
                            $rows[$key][$locale] = $value;
                        }
                    });
                }

                foreach ($rows as $rowItem) {
                    $this->xml->startElement('url');

                    $this->xml->writeElement('loc', array_first($rowItem));

                    foreach ($rowItem as $locale => $href) {
                        $this->xml->startElementNs('xhtml', 'link', null);
                        $this->xml->writeAttribute('rel', 'alternate');
                        $this->xml->writeAttribute('hreflang', $locale);
                        $this->xml->writeAttribute('href', $href);
                        $this->xml->endElement();
                    }

                    $this->xml->endElement();
                }
            } else {
                $this->xml->startElement('url');
                $this->xml->writeElement('loc', $item);
                $this->xml->endElement();
            }
        }

        return $this;
    }

    /**
     * @param  array|string  $name
     * @param  mixed  $parameters
     * @param  bool  $absolute
     * @return SitemapXmlGeneratorInterface
     */
    public function addLocalizedRoute($name, $parameters = [], $absolute = true): SitemapXmlGeneratorInterface
    {
        return $this->add(function () use ($name, $parameters, $absolute) {
            return route_localized($name, $parameters, $absolute);
        });
    }

    /**
     * @return void
     */
    public function save()
    {
        $this->xml->endElement();
        $this->xml->endDocument();
        $this->xml->flush();
    }
}
