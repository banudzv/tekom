## Installation

#### Use console
```
composer require wezom-cms/sitemap
```
#### Or edit composer.json
```
"require": {
    ...
    "wezom-cms/sitemap": "^7.0"
}
```
#### Install dependencies:
```
composer update
```
#### Package discover
```
php artisan package:discover
```

## Publish
#### Views
```
php artisan vendor:publish --provider="WezomCms\Sitemap\SitemapServiceProvider" --tag="views"
```
#### Config
```
php artisan vendor:publish --provider="WezomCms\Sitemap\SitemapServiceProvider" --tag="config"
```
#### Lang
```
php artisan vendor:publish --provider="WezomCms\Sitemap\SitemapServiceProvider" --tag="lang"
```
