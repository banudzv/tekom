<?php

namespace WezomCms\Slider\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use WezomCms\Core\Http\Requests\ChangeStatus\RequiredIfMessageTrait;
use WezomCms\Core\Traits\LocalizedRequestTrait;

class SlideLinkRequest extends FormRequest
{
    use LocalizedRequestTrait;
    use RequiredIfMessageTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->localizeRules(
            [
                'link' => 'required|string|max:255',
                'name' => 'required|string|max:255',
            ],
            [
                'published' => 'bool',
                'icon' => 'nullable|string',
            ]
        );
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return $this->localizeAttributes(
            [
                'link' => __('cms-slider::admin.Link'),
                'name' => __('cms-slider::admin.Name'),
            ],
            [
                'icon' => __('cms-core::admin.layout.Icon'),
                'published' => __('cms-core::admin.layout.Published')
            ]
        );
    }
}
