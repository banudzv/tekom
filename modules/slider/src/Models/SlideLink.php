<?php

namespace WezomCms\Slider\Models;

use Illuminate\Database\Eloquent\Model;
use WezomCms\Core\ExtendPackage\Translatable;
use WezomCms\Core\Traits\Model\Filterable;
use WezomCms\Core\Traits\Model\ImageAttachable;
use WezomCms\Core\Traits\Model\PublishedTrait;
use WezomCms\Services\Models\Service;

/**
 * \WezomCms\Slider\Models\Slide
 *
 * @property int $id
 * @property int $sort
 * @property bool $open_in_new_tab
 * @property bool $published
 * @property int|null $service_id
 * @property string $slider
 * @property string $image
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\WezomCms\Slider\Models\SlideTranslation[] $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Slider\Models\Slide filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Slider\Models\Slide listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Slider\Models\Slide newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Slider\Models\Slide newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Slider\Models\Slide notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Slider\Models\Slide orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Slider\Models\Slide orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Slider\Models\Slide orderByTranslation($key, $sortmethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Slider\Models\Slide paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Slider\Models\Slide published()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Slider\Models\Slide publishedWithSlug($slug, $slugField = 'slug')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Slider\Models\Slide query()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Slider\Models\Slide simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Slider\Models\Slide translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Slider\Models\Slide translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Slider\Models\Slide whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Slider\Models\Slide whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Slider\Models\Slide whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Slider\Models\Slide whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Slider\Models\Slide whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Slider\Models\Slide whereOpenInNewTab($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Slider\Models\Slide whereSlider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Slider\Models\Slide whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Slider\Models\Slide whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Slider\Models\Slide whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Slider\Models\Slide whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Slider\Models\Slide withTranslation()
 * @mixin \Eloquent
 * @mixin SlideLinkTranslation
 */
class SlideLink extends Model
{
    use Translatable;
    use PublishedTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['icon', 'published'];

    /**
     * Names of the fields being translated in the "Translation" model.
     *
     * @var array
     */
    protected $translatedAttributes = ['name', 'link'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'published' => 'bool'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];
}

