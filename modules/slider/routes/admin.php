<?php

Route::adminResource('slides', 'WezomCms\\Slider\\Http\\Controllers\\Admin\\SlideController');
Route::adminResource('slide-links', 'WezomCms\\Slider\\Http\\Controllers\\Admin\\SlideLinkController');
