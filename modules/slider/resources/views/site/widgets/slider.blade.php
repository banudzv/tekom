@php
    /**
     * @var $result \Illuminate\Database\Eloquent\Collection|\WezomCms\Slider\Models\Slide[]
     * @var $size null|string
     */
	$sliderConfig = config('cms.ui.sliderConfigs.heroSlider');
    // todo рекомендованный размер изображений - 1920x900
@endphp
<div class="section">
    <div class="container container--size-hd">
        <link rel="preload" as="image" href="{{ $result[0]['image'] }}">
        <div class="js-import hero-slider" data-slick-carousel='{!! json_encode($sliderConfig) !!}'>
            <div class="hero-slider__list" data-slick-carousel-list>
                @foreach($result as $obj)
                    <div class="hero-slider__slide">
                        <div class="hero-slider__image">
                            @if(\WezomAgency\Browserizr::detect()->isDesktop())
                                <img class="lozad fade-in js-import"
                                    data-src="{{ $obj['image'] }}"
                                     src="{{ url('/images/empty.gif') }}"
                                     alt="slider-image">
                            @else
                                <img data-lazy="{{ $obj['image'] }}"
                                     src="{{ url('/static/images/placeholders/no-image.png') }}"
                                     alt="slider-image">
                            @endif

                        </div>
                        <div class="hero-slider__content">
                            <div class="title title--size-h1 title--color-white _text-center">
                                {{ $obj['name'] }}
                            </div>
                            <div class="hero-slider__text">
                                {!! $obj['text'] !!}
                            </div>
                            @if($obj['service']['name'] && $obj['service']['slug'])
                                <div class="_flex _justify-center">
                                    @widget('ui:button', ['component' => 'a', 'classes' => '_text-center', 'attrs' => ['href' => $obj['service']['slug']], 'text' => $obj['service']['name'], 'modificators' => ['color-accent', 'size-def', 'uppercase', 'ls-def', 'bold']])
                                </div>
                            @endif
                            @if(count($obj['links'] ?? []))
                                <div class="hero-slider__links _spacer _spacer--md">
                                    @foreach($obj['links'] ?? [] as $link)
                                        @if($loop->index < 4)
                                            <a href="{{ $link['link'] }}" class="link link--theme-service">
                                                <span class="link__icon">
                                                    @svg($link['icon'])
                                                </span>
                                                    <span class="link__text">
                                                    {!! $link['name'] !!}
                                                </span>
                                            </a>
                                        @endif
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="hero-slider__controls">
                <button class="hero-slider__control hero-slider__control--prev" data-slick-carousel-prev-arrow>
                    @svg('arrow-top')
                </button>
                <div class="hero-slider__dots" data-slick-carousel-dots></div>
                <button class="hero-slider__control hero-slider__control--next" data-slick-carousel-next-arrow>
                    @svg('arrow-down')
                </button>
            </div>
        </div>
    </div>
</div>
