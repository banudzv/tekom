<?php

use WezomCms\Core\Enums\TranslationSide;

return [
    TranslationSide::ADMIN => [
        'FAQ' => 'FAQ',
        'Questions' => 'Вопросы',
        'FAQ questions' => 'FAQ: Вопросы',
        'FAQ groups' => 'FAQ: Групы',
        'Groups' => 'Групы FAQ',
        'Questions list' => 'Список вопросов',
        'Question' => 'Вопрос',
        'Answer' => 'Ответ',
        'Groups list' => 'Список',
        'Name' => 'Название',
        'Group' => 'Группа',
        'Publish questions' => 'Опубликованные вопросы',
    ],
];
