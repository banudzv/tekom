@extends('cms-ui::layouts.main')

@php
    /**
     * @var $result \Illuminate\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection|\WezomCms\News\Models\News[]
     * @var $currentTag null|string
     */
@endphp

@section('content')
    <div class="container">
        <h1>{{ SEO::getH1() }}</h1>
        <div class="grid">
            <div class="gcell">
                @widget('news:tags', ['currentTag' => $currentTag ?? null])
                @widget('news:most-viewed')
            </div>
            <div class="gcell">
                <div class="grid">
                    @forelse($result as $item)
                        <div class="gcell">
                            <div>
                                <a href="{{ $item->getFrontUrl() }}">
                                    <img class="lozad js-import" src="{{ url('assets/images/empty.gif') }}"
                                         data-lozad="{{ $item->getImageUrl() }}" alt="{{ $item->name }}">
                                </a>
                                <a href="{{ $item->getFrontUrl() }}">{{ $item->name }}</a>
                                <div>{{ $item->published_at->format('d.m.Y') }}</div>
                                <div>{{ str_limit(strip_tags($item->text)) }}</div>
                                <a href="{{ $item->getFrontUrl() }}">@lang('cms-news::site.Читать')</a>
                            </div>
                        </div>
                    @empty
                        <div class="gcell">
                            @emptyResult
                        </div>
                    @endforelse
                </div>
                @if($result->hasPages())
                    <div>{!! $result->links() !!}</div>
                @endif
            </div>
        </div>
    </div>
@endsection
