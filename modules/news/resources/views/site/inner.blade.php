@extends('cms-ui::layouts.main')

@php
    /**
     * @var $obj \WezomCms\News\Models\News
     */
@endphp

@section('content')
    <div class="container">
        <h1>{{ SEO::getH1() }}</h1>
        <div class="grid">
            <div class="gcell">
                @widget('news:tags')
                @widget('news:most-viewed')
            </div>
            <div class="gcell">
                {{ $obj->published_at->format('d.m.Y') }}
                <div class="wysiwyg js-import" data-wrap-media data-draggable-table>{!! $obj->text !!}</div>
                @widget('ui:share')
                <a href="{{ route('news') }}">@lang('cms-news::site.назад к списку')</a>
                @if($next = $obj->getNext())
                    <a href="{{ $next->getFrontUrl() }}">@lang('cms-sales::site.Предыдущая новость')</a>
                @endif
                @if($prev = $obj->getPrev())
                    <a href="{{ $prev->getFrontUrl() }}">@lang('cms-sales::site.Следующая новость')</a>
                @endif
            </div>
        </div>
    </div>
@endsection
