<?php

Route::namespace('WezomCms\\News\\Http\\Controllers\\Admin')
    ->group(function () {
        Route::adminResource('news', 'NewsController')->settings();

        if (config('cms.news.news.use_tags')) {
            Route::adminResource('news-tags', 'NewsTagsController');
        }
    });
