## Installation

#### Use console
```
composer require wezom-cms/news
```
#### Or edit composer.json
```
"require": {
    ...
    "wezom-cms/news": "^7.0"
}
```
#### Install dependencies:
```
composer update
```
#### Package discover
```
php artisan package:discover
```
#### Run migrations
```
php artisan migrate
```

## Publish
#### Views
```
php artisan vendor:publish --provider="WezomCms\News\NewsServiceProvider" --tag="views"
```
#### Config
```
php artisan vendor:publish --provider="WezomCms\News\NewsServiceProvider" --tag="config"
```
#### Lang
```
php artisan vendor:publish --provider="WezomCms\News\NewsServiceProvider" --tag="lang"
```
#### Migrations
```
php artisan vendor:publish --provider="WezomCms\News\NewsServiceProvider" --tag="migrations"
```
