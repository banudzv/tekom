<div class="row">
    <div class="col-lg-7">
        <div class="card mb-3">
            <div class="card-body">
                @langTabs
                    <div class="form-group">
                        {!! Form::label($locale . '[name]', __('cms-branches::admin.Name')) !!}
                        {!! Form::text($locale . '[name]', old($locale . '.name', $obj->translateOrNew($locale)->name)) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label($locale . '[address]', __('cms-branches::admin.Address')) !!}
                        {!! Form::text($locale . '[address]', old($locale . '.address', $obj->translateOrNew($locale)->address)) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label($locale . '[time_working]', __('cms-branches::admin.Time working')) !!}
                        {!! Form::textarea($locale . '[time_working]', old($locale . '.time_working', $obj->translateOrNew($locale)->time_working), ['class' => 'js-wysiwyg', 'data-lang' => $locale]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label($locale . '[date_create]', __('cms-branches::admin.Date create')) !!}
                        {!! Form::textarea($locale . '[date_create]', old($locale . '.date_create', $obj->translateOrNew($locale)->date_create)) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::multipleInputs($locale . '[services][]', old('services', $obj->translateOrNew($locale)->services), __('cms-branches::admin.Services additional')) !!}
                    </div>
                @endLangTabs
                <div class="form-group">
                    {!! Form::label('code', __('cms-branches::admin.Code')) !!}
                    {!! Form::text('code', old('code', $obj->code)) !!}
                </div>
                {{--<div class="form-group">
                    {!! Form::label('zip_code', __('cms-branches::admin.Zip code')) !!}
                    {!! Form::text('zip_code', old('code', $obj->zip_code)) !!}
                </div>--}}
            </div>
        </div>
{{--        <div class="card mb-3">--}}
{{--            <div class="card-header"><h4>@lang('cms-branches::admin.Map')</h4></div>--}}
{{--            <div class="card-body">--}}
{{--                {!! Form::map('map', $obj->map) !!}--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>
    <div class="col-lg-5">
        <div class="card mb-3">
            <div class="card-body">
                <div class="form-group">
                    {!! Form::label(str_slug('published'), __('cms-core::admin.layout.Published')) !!}
                    {!! Form::status('published') !!}
                </div>
                <div class="form-group">
                    {!! Form::label(str_slug('is_main'), __('cms-branches::admin.Is main')) !!}
                    {!! Form::status('is_main') !!}
                </div>
                <div class="form-group">
                    {!! Form::label('type', __('cms-branches::admin.Branch type')) !!}
                    <div class="input-group">
                        {!! Form::select('type', $obj::getTypeBySelect(), old('type', $obj->type), ['class' => 'js-select2']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('city_id', __('cms-regions::admin.City')) !!}
                    <div class="input-group">
                        {!! Form::select('city_id', $cities, old('city_id', $selectedCity), ['class' => 'js-select2']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('email', __('cms-branches::admin.E-mail')) !!}
                    {!! Form::text('email') !!}
                </div>
                <div class="form-group">
                    {!! Form::label('lat', __('cms-regions::admin.latitude')) !!}
                    {!! Form::text('lat') !!}
                </div>
                <div class="form-group">
                    {!! Form::label('lon', __('cms-regions::admin.longitude')) !!}
                    {!! Form::text('lon') !!}
                </div>
                <div class="form-group">
                    {!! Form::multipleInputs('phones[]', old('phones', $obj->phones), __('cms-branches::admin.Phones')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('employee_id', __('cms-branches::admin.Employee')) !!}
                    <div class="input-group">
                        {!! Form::select('employee_id', $employees, old('employee_id', $obj->employee_id), ['class' => 'js-select2']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
