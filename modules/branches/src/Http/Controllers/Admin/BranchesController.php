<?php

namespace WezomCms\Branches\Http\Controllers\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use WezomCms\Branches\Http\Requests\Admin\BranchRequest;
use WezomCms\Branches\Models\Branch;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;
use WezomCms\Core\Settings\AdminLimit;
use WezomCms\Core\Settings\Fields\AbstractField;
use WezomCms\Core\Settings\MultilingualGroup;
use WezomCms\Core\Traits\SettingControllerTrait;
use WezomCms\OurTeam\Repositories\EmployeeRepository;
use WezomCms\Regions\Repositories\CityRepository;
use WezomCms\Services\Repositories\ServiceRepository;

class BranchesController extends AbstractCRUDController
{
    use SettingControllerTrait;

    /**
     * Model name.
     *
     * @var string
     */
    protected $model = Branch::class;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-branches::admin';

    /**
     * Resource route name.
     *
     * @var string
     */

    protected $routeName = 'admin.branches';

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = BranchRequest::class;

    /**
     * @var CityRepository
     */
    private $cityRepository;
    /**
     * @var EmployeeRepository
     */
    private $employeeRepository;
    /**
     * @var ServiceRepository
     */
    private $serviceRepository;

    public function __construct(
        CityRepository $cityRepository,
        EmployeeRepository $employeeRepository,
        ServiceRepository $serviceRepository
    )
    {
        parent::__construct();

        $this->cityRepository = $cityRepository;
        $this->employeeRepository = $employeeRepository;
        $this->serviceRepository = $serviceRepository;
    }

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-branches::admin.Branches');
    }

    /**
     * @param $result
     * @param Request $request
     */
    protected function selectionIndexResult($result, Request $request)
    {
        $result->orderBy('sort');
    }

    /**
     * @param  Branch  $obj
     * @param  FormRequest  $request
     * @return array
     */
    protected function fill($obj, FormRequest $request): array
    {
        $data = parent::fill($obj, $request);

        $data['phones'] = array_filter($request->get('phones', []));
        $data['email'] = $request->get('email');
        $data['map'] = (array)json_decode($request->get('map'));
        foreach (\LaravelLocalization::getLocalesOrder() as $key => $item) {
            $services = $request->get($key, [])['services'] ?? [];
            $data[$key]['services'] = array_filter($services);
        }
        if(isset($data['employee_id']) && $data['employee_id'] == 0){
            $data['employee_id'] = null;
        }

        return $data;
    }

    /**
     * @param  Branch  $model
     * @param  Request  $request
     */
    protected function afterSuccessfulSave($model, Request $request)
    {

    }

    /**
     * @param  Branch $model
     * @param  array  $viewData
     * @return array
     */
    protected function createViewData($model, array $viewData): array
    {

        return [
            'cities' => $this->cityRepository->getBySelect(),
            'selectedCity' => [],
            'employees' => $this->employeeRepository->getBySelect(),
        ];
    }

    /**
     * @param  Branch $model
     * @param  array  $viewData
     * @return array
     */
    protected function editViewData($model, array $viewData): array
    {
        return [
            'cities' => $this->cityRepository->getBySelect(),
            'selectedCity' => [$model->city_id],
            'employees' => $this->employeeRepository->getBySelect(),
        ];
    }

    /**
     * @return array|AbstractField[]|MultilingualGroup[]
     * @throws \Exception
     */
    protected function settings()
    {
        return [AdminLimit::make()];
    }
}
