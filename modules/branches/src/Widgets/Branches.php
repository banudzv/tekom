<?php

namespace WezomCms\Branches\Widgets;

use WezomCms\Branches\Models\Branch;
use WezomCms\Branches\Repositories\BranchRepository;
use WezomCms\Core\Foundation\Widgets\AbstractWidget;

class Branches extends AbstractWidget
{
    /**
     * A list of models that, when changed, will clear the cache of this widget.
     *
     * @var array
     */
//    public static $models = [Branch::class];

    /**
     * @return array|null
     */
    public function execute(): ?array
    {
        /** @var $models Branch[] */
        $models = app(BranchRepository::class)
            ->getByTypeWithRelation(['translations', 'city']);

        if ($models->isEmpty()) {
            return null;
        }

        return compact('models');
    }
}
