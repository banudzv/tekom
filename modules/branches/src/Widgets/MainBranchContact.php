<?php

namespace WezomCms\Branches\Widgets;

use WezomCms\Branches\Models\Branch;
use WezomCms\Branches\Repositories\BranchRepository;
use WezomCms\Core\Foundation\Widgets\AbstractWidget;

class MainBranchContact extends AbstractWidget
{
    /**
     * A list of models that, when changed, will clear the cache of this widget.
     *
     * @var array
     */
//    public static $models = [Branch::class];

    /**
     * @return array|null
     */
    public function execute(): ?array
    {
        /** @var $model Branch */
        $model = app(BranchRepository::class)
            ->getMainBranch();

        if (!$model) {
            return null;
        }

        return [
            'email' => $model->email,
            'phones' => $model->phones,
            'full_address' => $model->full_address
        ];
    }
}

