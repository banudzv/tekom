<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RefactorBranchTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('branch_translations', function (Blueprint $table) {
            $table->string('city')->nullable();
            $table->string('time_working', 1000)->nullable();
            $table->dropColumn('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('branch_translations', function (Blueprint $table) {
            $table->string('name')->nullable();
            $table->dropColumn('city');
            $table->dropColumn('time_working');
        });
    }
}

