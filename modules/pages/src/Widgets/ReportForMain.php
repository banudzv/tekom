<?php

namespace WezomCms\Pages\Widgets;

use WezomCms\Core\Foundation\Widgets\AbstractWidget;
use WezomCms\Pages\Models\Page;
use WezomCms\Pages\Repositories\PageRepository;

class ReportForMain extends AbstractWidget
{
    /**
     * @return array|null
     */
    public function execute(): ?array
    {
        /** @var $models Page[] */
        $models = resolve(PageRepository::class)->getByMain();

        if($models->isEmpty()){
            return null;
        }

        return compact('models');
    }
}

