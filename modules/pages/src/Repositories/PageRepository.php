<?php

namespace WezomCms\Pages\Repositories;

use WezomCms\Core\Repositories\AbstractRepository;
use WezomCms\Pages\Models\Page;

class PageRepository extends AbstractRepository
{
    protected function model()
    {
        return Page::class;
    }

    public function getByMain()
    {
        return $this->getModelQuery()
            ->published()
            ->with(['translations'])
            ->where('for_main_report', true)
            ->get();
    }
}
