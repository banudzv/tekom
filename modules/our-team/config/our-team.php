<?php

use WezomCms\OurTeam\Dashboards;

return [
    'images' => [
        'directory' => 'our-team',
        'placeholder' => 'no-avatar.png',
        'default' => 'medium', // For admin image preview
        'sizes' => [
            'medium' => [
                'width' => 335,
                'height' => 365,
                'mode' => 'fit',
            ],
        ],
    ],
    'dashboards' => [
        Dashboards\EmployeeDashboard::class,
    ],
];
