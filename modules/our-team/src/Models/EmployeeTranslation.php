<?php

namespace WezomCms\OurTeam\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * \WezomCms\OurTeam\Models\EmployeeTranslation
 *
 * @property int $id
 * @property int $employee_id
 * @property string $name
 * @property string|null $position
 * @property string|null $address
 * @property string|null $experience_description
 * @property string|null $education
 * @property string|null $work_description
 * @property string $locale
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\EmployeeTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\EmployeeTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\EmployeeTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\EmployeeTranslation whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\EmployeeTranslation whereEmployeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\EmployeeTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\EmployeeTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\EmployeeTranslation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\EmployeeTranslation wherePosition($value)
 * @mixin \Eloquent
 */
class EmployeeTranslation extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'position', 'address', 'experience_description', 'education', 'work_description'];
}
