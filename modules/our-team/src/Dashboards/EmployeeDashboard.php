<?php

namespace WezomCms\OurTeam\Dashboards;

use WezomCms\Core\Foundation\Dashboard\AbstractValueDashboard;
use WezomCms\OurTeam\Models\Employee;

class EmployeeDashboard extends AbstractValueDashboard
{
    /**
     * @var null|int - cache time in minutes.
     */
    protected $cacheTime = 5;

    /**
     * @var null|string - permission for link
     */
    protected $ability = 'employees.view';

    /**
     * @return int
     */
    public function value(): int
    {
        return Employee::count();
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return __('cms-our-team::admin.Employees');
    }

    /**
     * @return string
     */
    public function icon(): string
    {
        return 'fa-users';
    }

    /**
     * @return string
     */
    public function iconColorClass(): string
    {
        return 'color-warning';
    }

    /**
     * @return null|string
     */
    public function url(): ?string
    {
        return route('admin.employees.index');
    }
}

