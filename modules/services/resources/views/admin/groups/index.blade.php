@extends('cms-core::admin.crud.index')

@php
    //$result = $pagination;

@endphp

@section('content')
    @if(!empty($paginatedResult))
        <div class="m-3">
            <div class="dd js-nestable" data-settings-depth="{{ config('cms.services.services.max_depth') }}"
                 data-control-list
                 data-params="{{ json_encode(['model' => encrypt($model)]) }}">
                @massControl($routeName)
                <ol class="dd-list">
                    @foreach($paginatedResult as $item)
                        @include($viewPath.'.index-list-item', ['result' => $result, 'item' => $item])
                    @endforeach
                </ol>
            </div>
        </div>
    @endif
@endsection

