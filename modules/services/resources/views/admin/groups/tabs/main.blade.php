@langTabs
    <div class="form-group">
        {!! Form::label($locale . '[name]', __('cms-services::admin.Name')) !!}
        {!! Form::text($locale . '[name]', old($locale . '.name', $obj->translateOrNew($locale)->name)) !!}
    </div>
    <div class="form-group">
        {!! Form::label($locale . '[slug]', __('cms-core::admin.layout.Slug')) !!}
        {!! Form::slugInput($locale . '[slug]', old($locale . '.slug', $obj->translateOrNew($locale)->slug), ['source' => 'input[name="' . $locale . '[name]"']) !!}
    </div>
    <div class="form-group">
        {!! Form::label($locale . '[sub_title]', __('cms-services::admin.Sub title')) !!}
        {!! Form::text($locale . '[sub_title]', old($locale . '.sub_title', $obj->translateOrNew($locale)->sub_title)) !!}
    </div>
    <div class="form-group">
        {!! Form::label($locale . '[title_for_risk]', __('cms-services::admin.Title for risk')) !!}
        {!! Form::text($locale . '[title_for_risk]', old($locale . '.title_for_risk', $obj->translateOrNew($locale)->title_for_risk)) !!}
    </div>

    @if($obj->template == 'new-index')
        <div class="form-group">
            {!! Form::label($locale . '[text_top]', __('cms-services::admin.Text') . ' TOP') !!}
            {!! Form::textarea($locale . '[text_top]', old($locale . '.text', $obj->translateOrNew($locale)->text_top), ['class' => 'js-wysiwyg', 'data-lang' => $locale]) !!}
        </div>
    @endif
    <div class="form-group">
        {!! Form::label($locale . '[text]', __('cms-services::admin.Text')) !!}
        {!! Form::textarea($locale . '[text]', old($locale . '.text', $obj->translateOrNew($locale)->text), ['class' => 'js-wysiwyg', 'data-lang' => $locale]) !!}
    </div>
    <div class="form-group">
        {!! Form::label($locale . '[short_description]', __('cms-services::admin.Short description')) !!}
        {!! Form::textarea($locale . '[short_description]', old($locale . '.short_description', $obj->translateOrNew($locale)->short_description)) !!}
    </div>
@endLangTabs
