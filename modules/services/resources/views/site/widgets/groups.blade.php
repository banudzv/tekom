@php
    /**
     * @var $result \Illuminate\Database\Eloquent\Collection|\WezomCms\Services\Models\ServiceGroup[]
     */
@endphp
<ul>
    @foreach($result as $group)
        <li>
            @if($group->id == $currentGroupId)
                <span>{{ $group->name }}</span>
            @else
                <a href="{{ $group->getFrontUrl() }}">{{ $group->name }}</a>
            @endif
        </li>
    @endforeach
</ul>
