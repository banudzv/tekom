{{--@dd($childs)--}}
@foreach($childs ?? [] as $child)
    @include('cms-services::site.group.partials.service-card', [
    'child' => $child,
    'groupSlug' => $groupSlug
])
@endforeach
