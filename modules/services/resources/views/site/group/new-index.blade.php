@extends('cms-ui::layouts.base', ['lightHeader' => true])

@php
use WezomCms\Services\Models\ServiceGroup;
    /**
     * @var $group \WezomCms\Services\Models\ServiceGroup
     * @var $child \WezomCms\Services\Models\ServiceGroup
     * @var $service \WezomCms\Services\Models\Service
     */

    $load_more_config = [
        'url' => route('service-group.more'),
        'method' => 'GET'
    ];
@endphp

@section('main')
    @widget('ui:heading-section', [
        'bannerImage' => $group->getImageBanner(),
        'title' => SEO::getH1(),
        'text' => $group->sub_title,
        'button' => (object)[
            'text' => __('cms-callbacks::site.get consultation'),
            'attrs' => [
                'type' => 'button',
                'data-mfp' => 'ajax',
                'data-mfp-src' => route('callbacks.popup')
            ],
            'component' => 'button',
            'classes' => 'js-import'
        ]
    ])
    @if($group->text_top)
        <div class="section section--description">
        <div class="container">
            <div class="text-block">
                <div class="_grid _justify-center _spacer _spacer--md">
                    <div class="_cell _cell--12">
                        <div class="text-block__text">
                            <div class="text-block__image">
                                <div class="image image--decorated">
                                    <div class="image__decor"></div>
                                    <img src="{{ url('/images/empty.gif') }}" class="lozad js-import" data-src="{{ $group->getImageTextTop() }}" alt="">
                                </div>
                            </div>
                            {!! $group->text_top !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="section section--programs js-import" data-load-more='{!! json_encode($load_more_config) !!}'>
        <div class="container">
            <div class="title title--size-h2 _text-center">@lang('cms-services::site.Insurance programs')</div>
            <div class="_grid _justify-center _spacer _spacer--md">
                <div class="_cell _cell--12 _sm:cell--10 _md:cell--12 _df:cell--10">
                    <div class="programs" data-load-more-container>
                        {{-- @todo ограничить количество выводимых до 4 (тз) --}}
                        {{-- @todo ожидаемый формат ответа - {
                            current_count // int - текущее количество карточек с учетом присланных
                            more // boolean - есть ли еще карточки для вывода
                            html // разметка с карточками по примеру вывода ниже
                        } --}}
                        {{-- @todo сюда в value выводить начальное кол-во выведенных карточек --}}
                        <input type="hidden" name="current-items-count" value="{{ ServiceGroup::DEFAULT_ON_PAGE }}" data-current-count>
                        <input type="hidden" name="service-group-id" value="{{ $group->id }}" data-service-group-id>
                        @include('cms-services::site.group.partials.service-groups', [
                            'childs' => $group->children->take(ServiceGroup::DEFAULT_ON_PAGE),
                            'groupSlug' => $group->slug
                        ])
                    </div>
                </div>
            </div>
            {{-- @todo не выводить если карточек <=4 --}}
            @if($group->children->count() > ServiceGroup::DEFAULT_ON_PAGE)
                <div class="_flex _justify-center">
                    <button class="load-more" type="button" data-load-more-trigger>
                        @svg('load', null, null, 'load-more__icon')
                        <span class="load-more__text">@lang('cms-core::site.load more')</span>
                    </button>
                </div>
            @endif
        </div>
    </div>

    <div class="section section--description">
        <div class="container">
            <div class="text-block">
                <div class="_grid _justify-center _spacer _spacer--md">
                    <div class="_cell _cell--12">
                        <div class="text-block__text">
                            <div class="text-block__image">
                                <div class="image image--decorated">
                                    <div class="image__decor"></div>
                                    <img src="{{ url('/images/empty.gif') }}" class="lozad js-import" data-src="{{ $group->getImage() }}" alt="">
                                </div>
                            </div>
                            {!! $group->text !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @widget('counter', ['classes' => 'counter--lined', 'modification' => 'counter'])
    @widget('benefits')
    <div class="section section--questions">
        <div class="section__decor" data-scroll-window="#consultation">
            <span>@lang('cms-core::site.or')</span>
            @svg('arrow-bottom-lg')
        </div>
        <div class="container">
            <div class="questions">
                <div class="_grid _justify-center">
                    <div class="_cell _cell--12 _md:cell--10 _df:cell-8 _lg:cell--6 _flex _flex-column _items-center">
                        <div class="questions__title">
                            @lang('cms-services::site.Hotline title')
                        </div>
                        <a href="tel:{{ $hotline }}" class="questions__link">
                            {{ $hotline }}
                        </a>
                        <div class="questions__button">
                            @widget('ui:button', [
                                'component' => 'button',
                                'classes' => 'js-import',
                                'attrs' => [
                                    'type' => 'button',
                                    'data-mfp' => 'ajax',
                                    'data-mfp-src' => route('callbacks.request-popup')
                                ],
                                'text' => __('cms-callbacks::site.send order by contract'),
                                'modificators' => [
                                    'color-accent',
                                    'size-def',
                                    'uppercase',
                                    'ls-def',
                                    'bold'
                                ]
                            ])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section section--consultation" id="consultation">
        <div class="consultation-block">
            <div class="container">
                <div class="_grid _spacer _spacer--md _justify-center">
                    <div class="_cell--12 _lg:cell--10">
                        <div class="_flex _flex-column _items-center">
                            <div class="consultation-block__title">
                                @lang('cms-callbacks::site.Consultation send button')
                            </div>
                            <div class="consultation-block__text">
                                @lang('cms-callbacks::site.Questions description with br')
                            </div>
                        </div>
                        @component('cms-ui::components.form.form-ajax', [
                            'class' => 'js-import',
                            'method' => 'POST',
                            'autocomplete' => 'off',
                            'url' => route('callbacks.consultation'),
                            'id' => uniqid('consultation-')])
                            @csrf
                            <div class="_grid _spacer _spacer--md _df:mb-none _mb-xs">
                                <div class="_cell--12 _df:cell--4">
                                    @include('cms-ui::components.form.input', [
                                        'name' => 'name',
                                        'attributes' => [ 'placeholder' => __('cms-callbacks::site.Name') ],
                                        'modificators' => ['inactive'],
                                        'classes' => 'form-item--theme-default',
                                        'mode' => 'text'
                                    ])
                                </div>
                                <div class="_cell--12 _df:cell--4">
                                    @php
                                        $inputmaskConfig = json_encode((object)[
                                            'mask' => '+38(999)-99-99-999',
                                            'androidMask' => '+38(999)-99-99-999'
                                        ])
                                    @endphp
                                    @include('cms-ui::components.form.input', [
                                        'name' => 'phone',
                                        'attributes' => [
                                            'placeholder' => __('cms-callbacks::site.Phone'),
                                            'data-mask' => $inputmaskConfig,
                                            'class' => 'js-import form-item__control',
                                            'required',
                                            'data-rule-phone'
                                            ],
                                        'modificators' => [],
                                        'component' => 'input',
                                        'classes' => 'form-item--theme-default',
                                        'mode' => 'tel'
                                    ])
                                </div>
                                <div class="_cell--12 _df:cell--4">
                                    @include('cms-ui::components.form.input', [
                                        'name' => 'text',
                                        'attributes' => [ 'placeholder' => __('cms-callbacks::site.Time') ],
                                        'modificators' => [],
                                        'component' => 'input',
                                        'classes' => 'form-item--theme-default',
                                        'mode' => 'text'
                                    ])
                                </div>
                            </div>
                            <div class="_flex _flex-column _items-center _df:mt-sm _df:pt-xxs">
                                <button type="submit"
                                        class="button button--bold button--ls-def button--uppercase button--color-accent button--size-def">
                                    @lang('cms-callbacks::site.Consultation send button')
                                </button>
                                <div class="consultation-block__description">@lang('cms-callbacks::site.Consultation send text')</div>
                            </div>
                        @endcomponent
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="_mb-md">
        @widget('branches:list')
    </div>
@endsection

