@extends('cms-ui::layouts.base', ['lightHeader' => true, 'staticHeader' => true])

@php
    /**
     * @var $service \WezomCms\Services\Models\Service
     * @var $risk \WezomCms\Services\Models\Risk
     * @var $document \WezomCms\Services\Models\Document
     * @var $step \WezomCms\Services\Models\Step
     * @var $additional \WezomCms\Services\Models\Additional
     * @var $related \WezomCms\Services\Models\Service
     * @var $group \WezomCms\Services\Models\ServiceGroup
     * @var $faq \WezomCms\Faq\Models\FaqQuestion
     * @var $objectInsurance \WezomCms\Services\Models\ObjectInsurance
     * @var $hotline string
     */
    $anchors = [
        (object)[
            'name' => __('cms-services::site.anchor.about'),
            'anchor' => '#about',
            'active' => $service->name ? true : false,
            'offset' => '100'
        ],
        (object)[
            'name' => __('cms-services::site.anchor.risks'),
            'anchor' => '#risks',
            'active' => $service->risks->isNotEmpty() ? true : false,
            'offset' => '100'

        ],
        (object)[
            'name' => __('cms-services::site.anchor.benefits'),
            'anchor' => '#additional',
            'active' => $service->steps->isNotEmpty() ? true : false,
            'offset' => '0'
        ],
        (object)[
            'name' => __('cms-services::site.anchor.faqs'),
            'anchor' => '#faq',
            'active' => $service->faqs->isNotEmpty() ? true : false,
            'offset' => '100'
        ],
    ];
@endphp

@section('main')
    @if($service->calculate_link)
        <?php
            $button = (object)[
                'text' => __('cms-services::site.calculate service', ['name' => $service->name]),
                'attrs' => [
                    'href' => $service->calculate_link,
                ],
                'component' => 'a'
            ]
        ?>
    @endif
    @widget('ui:heading-section', [
        'bannerImage' => $service->getImageBanner(),
        'title' => SEO::getH1(),
        'text' => $service->sub_title,
        'button' => $button ?? null
    ])
    <div class="nav-block nav-block--theme-anchors">
        {{-- @todo выводить с переводами но такими же якорями --}}
        <div class="nav-block__anchors js-import" data-nav-tabs>
            @foreach($anchors as $item)
                @if($item->active)
                    <div class="nav-block__anchor" data-nav-tab data-offset="{{ $item->offset }}" data-scroll-window="{{ $item->anchor }}">
                        {{ $item->name }}
                    </div>
                @endif
            @endforeach
        </div>
    </div>
    @if($service->name)
        <div class="section section--about-service" id="about">
            <div class="container">
                <div class="title title--size-h2 _text-center _df:pb-xxl _pb-lg">
                    @lang('cms-services::site.service.About service', ['name' => $service->name])
                </div>
                <div class="text-block">
                    <div class="_grid _justify-center _spacer _spacer--md">
                        <div class="_cell _cell--12 _sm:cell--10 _md:cell--8 _lg:cell--6">
                            <div class="text-block__text">
                                {!! $service->text !!}
                            </div>
                        </div>
                        <div class="_cell _cell--12 _sm:cell--10 _md:cell--8 _df:cell--4 _flex _justify-center _df:justify-start">
                            <div class="text-block__image">
                                <div class="image image--decorated">
                                    <div class="image__decor"></div>
                                    <img src="{{ url('/images/empty.gif') }}" class="lozad js-import" data-src="{{ $service->getImage() }}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if($service->risks->isNotEmpty())
        <div class="section section--risks" id="risks">
            <div class="container">
                <div class="risks risks--theme-inner">
                    <div class="risks__title">
                        @lang('cms-services::site.service.What risks does it cover')
                    </div>
                    <div class="_grid _spacer _spacer--md _justify-center">
                        <div class="_cell _cell--12 _df:cell--10">
                            <div class="risks__content-wrapper">
                                <div class="risks__content">
                                    @foreach($service->risks ?? [] as $risk)
                                        <div class="risk">
                                            <div class="risk__icon">
{{--                                                {{ svg($risk->icon) }}--}}
                                                @svg($risk->icon)
                                            </div>
                                            <div class="risk__title">
                                                {{ $risk->name }}
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                {{-- @todo выводить блок ниже на Внутр. странице услуги для основной категории Корпоративным клиентам--}}
                                <div>
                                    <div class="title title--size-h2 _text-center _pt-lg _pb-xxl _mb-xs">
                                        @lang('cms-services::site.service.Object insurance')
                                    </div>
                                    <div class="_grid _justify-center _spacer _spacer--md _pb-md">
                                        @foreach($service->objectInsurances ?? [] as $objectInsurance)
                                            <div class="_cell _cell--12 _md:cell--8 _df:cell--6">
                                                <div class="insurance">
                                                    <div class="insurance__title">
                                                        {{ $objectInsurance->name }}
                                                    </div>
                                                    <div class="insurance__text">
                                                        {{ $objectInsurance->text }}
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if($service->documents->isNotEmpty())
        <div class="section section--documents" id="documents">
            <div class="container">
                <div class="documents">
                    <div class="_grid _justify-center _spacer _spacer--md">
                        <div class="_cell _cell--12 _md:cell--8 _df:cell--6 _lg:cell--5">
                            <div class="documents__title">
                                @lang('cms-services::site.service.Documents')
                            </div>
                            <div class="documents__text wysiwyg wysiwyg--main">
                                {!! $service->text_for_document !!}
                            </div>
                        </div>
                        <div class="_cell _cell--12 _md:cell--8 _df:cell--6 _lg:cell--5">
                            <div class="documents__items">
                                @foreach($service->documents ?? [] as $document)
                                    <div class="document">
                                        <div class="document__count-wrapper">
                                            <div class="document__count">
                                                {{ $loop->iteration }}
                                            </div>
                                        </div>
                                        <div class="document__content">
                                            {{--<div>{{ $document->icon }}</div>--}}
                                            <div class="document__title">{{ $document->name }}</div>
                                            <div class="document__text">{{ $document->text }}</div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if($service->steps->isNotEmpty())
        <div class="section section--steps" id="benefits">
            <div class="container">
                <div class="steps">
                    <div class="steps__title">
                        @lang('cms-services::site.service.Three steps')
                    </div>
                    <div class="steps__content">
                        <div class="_grid _justify-center">
                            <div class="_cell _cell--12 _lg:cell--8 _flex _flex-wrap _lg:flex-nowrap _lg:justify-start _justify-center">
                                @foreach($service->steps ?? [] as $step)
                                    <div class="step {{ $loop->last ? '' : 'step--separated' }}">
                                        <div class="step__icon-wrapper">
                                            <div class="step__icon">
                                                @svg($step->icon)
                                                <div class="step__count">
                                                    {{ $loop->index < 10 ? 0 : '' }}{{ $loop->iteration }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="step__title">{{ $step->name }}</div>
                                        <div class="step__text">{!! $step->text !!}</div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if($service->additionals->isNotEmpty())
        <div class="section section--provided-services" id="additional">
            <div class="container">
                <div class="title title--size-h2 _text-center _pb-sm _lg:pb-hg _mb-df">
                    @lang('cms-services::site.service.Additional services')
                </div>
                <div class="_grid _spacer _spacer--md _justify-center">
                    @foreach($service->additionals ?? [] as $additional)
                        <div class="_cell _cell--12 _sm:cell--8 _md:cell--6 _df:cell--4 _lg:cell--3">
                            <div class="service-card service-card--theme-additional">
                                <div class="service-card__body">
                                    <div class="service-card__top">
                                        <div class="service-card__icon">
                                            @svg($additional->icon)
                                        </div>
                                        <div class="service-card__title">
                                            {{ $additional->name }}
                                        </div>
                                    </div>
                                    <div class="service-card__text">
                                        {{ $additional->text }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

    @endif

    @widget('counter', ['modification' => 'counter'])

    @widget('benefits')
    @if($service->relatedServices->isNotEmpty())
        <div class="section section--related-services">
            <div class="container">
                <div class="title title--size-h2 _text-center _pb-hg _mb-xs">
                    @lang('cms-services::site.service.Relation service', ['name' => $service->name])
                </div>
                <div class="_grid _spacer _spacer--md _justify-center">
                    @foreach($service->relatedServices ?? [] as $related)
                        <div class="_cell _cell--12 _sm:cell--8 _md:cell--6 _df:cell--4 _lg:cell--3">
                            <div class="service-card service-card--theme-related">
                                <div class="service-card__body">
                                    <div class="service-card__top">
                                        <div class="service-card__icon">
                                            @svg($related->icon)
                                        </div>
                                        <div class="service-card__title">
                                            {{ $related->name }}
                                        </div>
                                    </div>
                                    <div class="service-card__text">
                                        {{ $related->text_for_relation }}
                                    </div>
                                </div>
                                <div class="_flex _justify-start">
                                    @widget('ui:button', [
                                        'component' => 'a',
                                            'attrs' => [
                                            'href' => route('services.inner', ['slug' => $related->slug])
                                        ],
                                        'text' => __('cms-services::site.more details'),
                                        'modificators' => [
                                            'color-accent',
                                            'size-md',
                                            'uppercase',
                                            'ls-def',
                                            'bold'
                                        ]
                                    ])
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif

    @if($service->groups->isNotEmpty())
        <div class="section section--recommended-services">
            <div class="container">
                <div class="title title--size-h2 _text-center _pb-lg _mb-sm">
                    @lang('cms-services::site.service.Recommend')
                </div>
                <div class="_grid _spacer _spacer--md _justify-center">
                    @foreach($service->groups ?? [] as $group)
                        <div class="_cell _cell--12 _sm:cell--8 _md:cell--6 _df:cell--4 _lg:cell--3">
                            <div class="service-card service-card--theme-additional service-card--theme-recommended">
                                <div class="service-card__body">
                                    <div class="service-card__top">
                                        <div class="service-card__icon">
                                            @svg($group->icon)
                                        </div>
                                        <div class="service-card__title">
                                            {{ $group->name }}
                                        </div>
                                    </div>
                                    <div class="service-card__text">
                                        {{ $group->short_description }}
                                    </div>
                                </div>
                                <div class="_flex _justify-start">
                                    @widget('ui:button', [
                                        'component' => 'a',
                                        'attrs' => [
                                            'href' => route('service-group.inner', ['baseSlug' => $group->parent->slug, 'slug' => $group->slug])
                                        ],
                                        'text' => __('cms-services::site.more details'),
                                        'modificators' => [
                                            'color-accent',
                                            'size-md',
                                            'uppercase',
                                            'ls-def',
                                            'bold'
                                        ]
                                    ])
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif

    @if($service->faqs)
        <div class="section section--faq" id="faq">
            <div class="section__decor" data-scroll-window="#consultation">
                <span>@lang('cms-core::site.or')</span>
                @svg('arrow-bottom-lg')
            </div>
            <div class="container">
                <div class="title title--size-h2 _text-center _pb-df">
                    @lang('cms-services::site.service.Faq title')
                </div>
                <div class="_grid _justify-center">
                    <div class="_cell _cell--12 _md:cell--10 _df:cell--8">
                        @foreach($service->faqs ?? [] as $faq)
                            <div class="accordion accordion--theme-faq js-import"
                                 data-accordion='{"ns": "faq"}'>
                                <div class="accordion__head"
                                     data-accordion-ns="faq"
                                     data-accordion-button="{{ 'faq-' . $loop->index }}">
                                    <div class="accordion__title">
                                        {{ $faq->question }}
                                    </div>
                                    @svg('plus', null, null, 'accordion__icon')
                                </div>
                                <div class="accordion__body"
                                     style="display: none;"
                                     data-accordion-ns="faq"
                                     data-accordion-block="{{ 'faq-' . $loop->index }}">
                                    {!! $faq->answer !!}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="container _pt-lg">
                <div class="questions">
                    <div class="_grid _justify-center">
                        <div class="_cell _cell--12 _md:cell--10 _df:cell-8 _lg:cell--6 _flex _flex-column _items-center">
                            <div class="questions__title">
                                @lang('cms-services::site.Hotline title', ['service' => $service->getParentalCaseName()])
                            </div>
                            <a href="tel:{{ $hotline }}" class="questions__link">
                                {{ $hotline }}
                            </a>
                            <div class="questions__button">
                                @widget('ui:button', [
                                'component' => 'button',
                                'classes' => 'js-import',
                                'attrs' => [
                                'type' => 'button',
                                'data-mfp' => 'ajax',
                                'data-mfp-src' => route('callbacks.request-popup')
                                ],
                                'text' => __('cms-callbacks::site.send order by contract'),
                                'modificators' => [
                                'color-accent',
                                'size-def',
                                'uppercase',
                                'ls-def',
                                'bold'
                                ]
                                ])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="section section--consultation _nmb-xxl" id="consultation">
        <div class="consultation-block">
            <div class="container">
                <div class="_grid _spacer _spacer--md _justify-center">
                    <div class="_cell--12 _lg:cell--10">
                        <div class="_flex _flex-column _items-center">
                            <div class="consultation-block__title">
                                @lang('cms-callbacks::site.Consultation send button')
                            </div>
                            <div class="consultation-block__text">
                                @lang('cms-callbacks::site.Questions description with br')
                            </div>
                        </div>
                        @component('cms-ui::components.form.form-ajax', [
                            'class' => 'js-import',
                            'method' => 'POST',
                            'autocomplete' => 'off',
                            'url' => route('callbacks.consultation'),
                            'id' => uniqid('consultation-')])
                            @csrf
                            <div class="_grid _spacer _spacer--md _df:mb-none _mb-xs">
                                <div class="_cell--12 _df:cell--4">
                                    @include('cms-ui::components.form.input', [
                                        'name' => 'name',
                                        'attributes' => [ 'placeholder' => __('cms-callbacks::site.Name') ],
                                        'modificators' => ['inactive'],
                                        'classes' => 'form-item--theme-default',
                                        'mode' => 'text'
                                    ])
                                </div>
                                <div class="_cell--12 _df:cell--4">
                                    @php
                                        $inputmaskConfig = json_encode((object)[
                                            'mask' => '+38(999)-99-99-999',
                                            'androidMask' => '+38(999)-99-99-999'
                                        ])
                                    @endphp
                                    @include('cms-ui::components.form.input', [
                                        'name' => 'phone',
                                        'attributes' => [
                                            'placeholder' => __('cms-callbacks::site.Phone'),
                                            'data-mask' => $inputmaskConfig,
                                            'class' => 'js-import form-item__control',
                                            'required',
                                            'data-rule-phone'
                                            ],
                                        'modificators' => [],
                                        'component' => 'input',
                                        'classes' => 'form-item--theme-default',
                                        'mode' => 'tel'
                                    ])
                                </div>
                                <div class="_cell--12 _df:cell--4">
                                    @include('cms-ui::components.form.input', [
                                        'name' => 'text',
                                        'attributes' => [ 'placeholder' => __('cms-callbacks::site.Time') ],
                                        'modificators' => [],
                                        'component' => 'input',
                                        'classes' => 'form-item--theme-default',
                                        'mode' => 'text'
                                    ])
                                </div>
                            </div>
                            <div class="_flex _flex-column _items-center _df:mt-sm _df:pt-xxs">
                                <button type="submit"
                                        class="button button--bold button--ls-def button--uppercase button--color-accent button--size-def">
                                    @lang('cms-callbacks::site.Consultation send button')
                                </button>
                                <div class="consultation-block__description">@lang('cms-callbacks::site.Consultation send text')</div>
                            </div>
                        @endcomponent
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('cms-ui::partials.seo-text', ['hideH1' => true])
@endsection
