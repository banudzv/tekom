<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RefactorServiceGroupBuyModuleTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('buy_module_service_group_translations', function (Blueprint $table) {
            $table->text('text_by_online')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('buy_module_service_group_translations', function (Blueprint $table) {
            $table->dropColumn('text_by_online');
        });
    }
}
