<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObjectInsuranceTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('object_insurance_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('object_insurance_id');
            $table->string('locale')->index();
            $table->string('name');
            $table->string('text',1500);

            $table->unique(['object_insurance_id', 'locale']);
            $table->foreign('object_insurance_id')
                ->references('id')
                ->on('object_insurances')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('object_insurance_translations');
    }
}
