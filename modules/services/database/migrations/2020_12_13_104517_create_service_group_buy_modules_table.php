<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceGroupBuyModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buy_module_service_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('service_group_id');
            $table->foreign('service_group_id')
                ->references('id')
                ->on('service_groups')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buy_module_service_groups');
    }
}

