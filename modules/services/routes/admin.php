<?php

Route::namespace('WezomCms\\Services\\Http\\Controllers\\Admin')
    ->group(function () {
        Route::adminResource('services', 'ServicesController')->settings();
        Route::adminResource('service-groups', 'ServiceGroupsController');
        Route::adminResource('risks', 'RiskController');
        Route::adminResource('risk-groups', 'RiskGroupController');
        Route::adminResource('service-additionals', 'AdditionalController');
        Route::adminResource('service-steps', 'StepController');
        Route::adminResource('service-documents', 'DocumentController');
        Route::adminResource('object-insurances', 'ObjectInsuranceController');
    });
