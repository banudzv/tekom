<?php

namespace WezomCms\Services\Widgets;

use Illuminate\Support\Collection;
use WezomCms\Core\Foundation\Widgets\AbstractWidget;
use WezomCms\Services\Models\ServiceGroup;
use WezomCms\Services\Repositories\ServiceGroupRepository;

class GroupsAsMenu extends AbstractWidget
{

    /**
     * @return array|null
     */
    public function execute(): ?array
    {
        $repository = resolve(ServiceGroupRepository::class);

        /** @var  $models ServiceGroup[] */
        $models = $repository->getByFrontAsMenu();

        if ($models->isEmpty()) {
            return null;
        }

        return compact('models');
    }
}
