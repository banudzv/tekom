<?php

namespace WezomCms\Services\Http\Controllers\Admin;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;
use WezomCms\Core\Settings\Fields\AbstractField;
use WezomCms\Core\Settings\MetaFields\SeoFields;
use WezomCms\Core\Settings\MultilingualGroup;
use WezomCms\Core\Settings\SiteLimit;
use WezomCms\Core\Traits\SettingControllerTrait;
use WezomCms\Faq\Repositories\FaqRepository;
use WezomCms\Services\Http\Requests\Admin\ServiceRequest;
use WezomCms\Services\Models\Service;
use WezomCms\Services\Models\ServiceGroup;
use WezomCms\Services\Repositories\AdditionalRepository;
use WezomCms\Services\Repositories\DocumentRepository;
use WezomCms\Services\Repositories\ObjectInsuranceRepository;
use WezomCms\Services\Repositories\RiskGroupRepository;
use WezomCms\Services\Repositories\RiskRepository;
use WezomCms\Services\Repositories\ServiceGroupRepository;
use WezomCms\Services\Repositories\ServiceRepository;
use WezomCms\Services\Repositories\StepRepository;

class ServicesController extends AbstractCRUDController
{
    use SettingControllerTrait;

    /**
     * Model name.
     *
     * @var string
     */
    protected $model = Service::class;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-services::admin.services';

    /**
     * Resource route name.
     *
     * @var string
     */
    protected $routeName = 'admin.services';

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = ServiceRequest::class;

    /**
     * @var bool
     */
    private $useGroups;

    /**
     * @var RiskRepository
     */
    private $riskRepository;
    /**
     * @var ServiceGroupRepository
     */
    private $serviceGroupRepository;
    /**
     * @var ServiceRepository
     */
    private $serviceRepository;
    /**
     * @var FaqRepository
     */
    private $faqRepository;
    /**
     * @var AdditionalRepository
     */
    private $additionalRepository;
    /**
     * @var StepRepository
     */
    private $stepRepository;
    /**
     * @var DocumentRepository
     */
    private $documentRepository;
    /**
     * @var ObjectInsuranceRepository
     */
    private $objectInsuranceRepository;
    /**
     * @var RiskGroupRepository
     */
    private $riskGroupRepository;

    /**
     * ServicesController constructor.
     */
    public function __construct(
        RiskRepository $riskRepository,
        ServiceGroupRepository $serviceGroupRepository,
        ServiceRepository $serviceRepository,
        FaqRepository $faqRepository,
        AdditionalRepository $additionalRepository,
        StepRepository $stepRepository,
        DocumentRepository $documentRepository,
        ObjectInsuranceRepository $objectInsuranceRepository,
        RiskGroupRepository $riskGroupRepository
    )
    {
        parent::__construct();

        $this->useGroups = (bool)config('cms.services.services.use_groups');
        $this->riskRepository = $riskRepository;
        $this->serviceGroupRepository = $serviceGroupRepository;
        $this->serviceRepository = $serviceRepository;
        $this->faqRepository = $faqRepository;
        $this->additionalRepository = $additionalRepository;
        $this->stepRepository = $stepRepository;
        $this->documentRepository = $documentRepository;
        $this->objectInsuranceRepository = $objectInsuranceRepository;
        $this->riskGroupRepository = $riskGroupRepository;
    }

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-services::admin.Services');
    }

    /**
     * @param  Builder  $query
     * @param  Request  $request
     */
    protected function selectionIndexResult($query, Request $request)
    {
        $query->orderBy('sort');
    }

    /**
     * @param  Service  $model
     * @param  array  $viewData
     * @return array
     */
    protected function formData($model, array $viewData): array
    {
        return [
            'groups' => $this->serviceGroupRepository->getBySelect(),
        ];
    }

    /**
     * @param  Service  $model
     * @param  FormRequest  $request
     * @return array
     */
    protected function fill($model, FormRequest $request): array
    {
        $data = parent::fill($model, $request);

        if (!$this->useGroups && isset($data['service_group_id'])) {
            unset($data['service_group_id']);
        }

        return $data;
    }

    /**
     * @param  Service  $model
     * @param  Request  $request
     */
    protected function afterSuccessfulSave($model, Request $request)
    {
        $model->risks()->sync($request->get('risks', []));
        $model->relatedServices()->sync($request->get('services', []));
        $model->groups()->sync($request->get('groups_relation', []));
        $model->faqs()->sync($request->get('faqs', []));
        $model->additionals()->sync($request->get('additionals', []));
        $model->steps()->sync($request->get('steps', []));
        $model->documents()->sync($request->get('documents', []));
        $model->objectInsurances()->sync($request->get('object_insurances', []));
    }

    /**
     * @param  Service $model
     * @param  array  $viewData
     * @return array
     */
    protected function createViewData($model, array $viewData): array
    {
        return [
            'risks' => $this->riskGroupRepository->getBySelectWithRisk(),
            'selectedRisks' => [],
            'groups' => $this->serviceGroupRepository->getBySelect(),
            'groupsRelation' => $this->serviceGroupRepository->getBySelect($model->service_group_id),
            'selectedGroupsRelation' => [],
            'services' => $this->serviceRepository->getBySelect(false, $model->id),
            'selectedServices' => [],
            'faqs' => $this->faqRepository->getBySelect(),
            'selectedFaqs' => [],
            'additionals' => $this->additionalRepository->getBySelect(),
            'selectedAdditionals' => [],
            'steps' => $this->stepRepository->getBySelect(),
            'selectedSteps' => [],
            'documents' => $this->documentRepository->getBySelect(),
            'selectedDocuments' => [],
            'objectInsurances' => $this->objectInsuranceRepository->getBySelect(),
            'selectedObjectInsurances' => [],
        ];
    }

    /**
     * @param  Service $model
     * @param  array  $viewData
     * @return array
     */
    protected function editViewData($model, array $viewData): array
    {
        return [
            'risks' => $this->riskGroupRepository->getBySelectWithRisk(),
            'selectedRisks' => $model->risks()->pluck('risk_id')->toArray(),
            'groups' => $this->serviceGroupRepository->getBySelect(),
            'groupsRelation' => $this->serviceGroupRepository->getBySelect($model->service_group_id),
            'selectedGroupsRelation' => $model->groups()->pluck('group_id')->toArray(),
            'services' => $this->serviceRepository->getBySelect(false, $model->id),
            'selectedServices' => $model->relatedServices()->pluck('other_service_id')->toArray(),
            'faqs' => $this->faqRepository->getBySelect(),
            'selectedFaqs' => $model->faqs()->pluck('faq_id')->toArray(),
            'additionals' => $this->additionalRepository->getBySelect(),
            'selectedAdditionals' => $model->additionals()->pluck('additional_id')->toArray(),
            'steps' => $this->stepRepository->getBySelect(),
            'selectedSteps' => $model->steps()->pluck('step_id')->toArray(),
            'documents' => $this->documentRepository->getBySelect(),
            'selectedDocuments' => $model->documents()->pluck('document_id')->toArray(),
            'objectInsurances' => $this->objectInsuranceRepository->getBySelect(),
            'selectedObjectInsurances' => $model->objectInsurances()->pluck('object_insurance_id')->toArray(),
        ];
    }

    /**
     * @return array|AbstractField[]|MultilingualGroup[]
     * @throws \Exception
     */
    protected function settings(): array
    {
        return [
            SiteLimit::make()->setName(__('cms-services::admin.Site services limit at page')),
            SeoFields::make('Services'),
        ];
    }
}
