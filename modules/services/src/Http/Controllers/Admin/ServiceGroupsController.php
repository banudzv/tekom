<?php

namespace WezomCms\Services\Http\Controllers\Admin;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use WezomCms\Core\Contracts\Filter\FilterFieldInterface;
use WezomCms\Core\Foundation\Helpers;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;
use WezomCms\Services\Http\Requests\Admin\ServiceGroupRequest;
use WezomCms\Services\ModelFilters\ServiceGroupFilter;
use WezomCms\Services\Models\BuyModule;
use WezomCms\Services\Models\ServiceGroup;
use WezomCms\Services\Repositories\RiskRepository;

class ServiceGroupsController extends AbstractCRUDController
{
    /**
     * Model name.
     *
     * @var string
     */
    protected $model = ServiceGroup::class;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-services::admin.groups';

    /**
     * Resource route name.
     *
     * @var string
     */
    protected $routeName = 'admin.service-groups';

    /**
     * Indicates whether to use pagination.
     *
     * @var bool
     */
    protected $paginate = false;

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = ServiceGroupRequest::class;

    /**
     * @var RiskRepository
     */
    private $riskRepository;

    public function __construct(
        RiskRepository $riskRepository
    )
    {
        parent::__construct();
        $this->riskRepository = $riskRepository;
    }

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-services::admin.Service groups');
    }

    /**
     * @param  Builder  $query
     * @param  Request  $request
     */
    protected function selectionIndexResult($query, Request $request)
    {
        $query->orderBy('sort');
    }

    /**
     * @param $result
     * @param  array  $viewData
     * @return array
     */
    protected function indexViewData($result, array $viewData): array
    {
        $request = app('request');

        $presentFilterInputs = array_filter(
            (new ServiceGroupFilter($this->model()::query(), $request->all()))->getFields(),
            function (FilterFieldInterface $filterField) use ($request) {
                $name = $filterField->getName();

                return $name !== 'per_page' && $request->get($name) !== '' && $request->get($name) !== null;
            }
        );

        if (count($presentFilterInputs)) {
            // Render groups as one level
            $firstLevel = $result->all();
            $result = [null => $result->all()];
        } else {
            // Multidimensional tree
            $result = Helpers::groupByParentId($result);
            $firstLevel = $result[null] ?? [];
        }

        $limit = $this->getLimit($request);

        // Paginate only first level items
        $pagination = new LengthAwarePaginator(
            $firstLevel,
            count($firstLevel),
            $limit,
            null,
            ['path' => $request->url(), 'query' => $request->query()]
        );

        $paginatedResult = array_slice($firstLevel, ($pagination->currentPage() - 1) * $limit, $limit);

        return ['result' => $result, 'paginatedResult' => $paginatedResult, 'pagination' => $pagination];
    }

    /**
     * @param  ServiceGroup  $model
     * @param  Request  $request
     */
    protected function afterSuccessfulSave($model, Request $request)
    {
        $model->risks()->sync($request->get('risks', []));

        if(isset($request['buy_module'])){
            if($model->buyModule){
                $model->buyModule->edit($request['buy_module']);
            } else {
                BuyModule::create($request['buy_module'], $model->id);
            }
        }

    }

    /**
     * @param  ServiceGroup $model
     * @param  array  $viewData
     * @return array
     */
    protected function createViewData($model, array $viewData): array
    {
        return [
            'tree' => $this->buildTree(),
            'risks' => $this->riskRepository->getBySelect(),
            'selectedRisks' => [],

        ];
    }

    /**
     * @param  ServiceGroup $model
     * @param  array  $viewData
     * @return array
     */
    protected function editViewData($model, array $viewData): array
    {
        return [
            'tree' => $this->buildTree($model->id),
            'risks' => $this->riskRepository->getBySelect(),
            'selectedRisks' => $model->risks()->pluck('risk_id')->toArray(),
        ];
    }

    /**
     * @param  int|null  $id
     * @return array
     */
    private function buildTree(int $id = null)
    {
        $tree = [
            '' => __('cms-services::admin.Root'),
        ];

        $query = ServiceGroup::select('id', 'parent_id');

        if ($id) {
            $query->where('id', '<>', $id);
        }

        $items = $query->orderBy('sort')
            ->latest('id')
            ->get();

        $groupedItems = Helpers::groupByParentId($items);

        $maxDepth = config('cms.services.services.max_depth', 100);

        $tree += $this->generateTree($groupedItems, $maxDepth);

        return $tree;
    }

    /**
     * @param  array  $groupedItems
     * @param  int  $maxDepth
     * @param  null  $parentId
     * @param  int  $depth
     * @return array
     */
    private function generateTree(array $groupedItems, int $maxDepth, $parentId = null, $depth = 0)
    {
        $result = [];

        if ($depth + 1 >= $maxDepth) {
            return $result;
        }

        $elements = isset($groupedItems[$parentId]) ? $groupedItems[$parentId] : [];

        foreach ($elements as $item) {
            $result[$item->id] = str_repeat('&nbsp;', $depth * 4) . $item->name;

            $result += $this->generateTree($groupedItems, $maxDepth, $item->id, $depth + 1);
        }

        return $result;
    }
}
