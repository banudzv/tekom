<?php

namespace WezomCms\Services\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use WezomCms\Core\Traits\LocalizedRequestTrait;

class ServiceRequest extends FormRequest
{
    use LocalizedRequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'published' => 'required',
            'icon' => 'nullable|string',
            'calculate_link' => 'nullable|string'
        ];
        if (config('cms.services.services.use_groups')) {
            $rules['service_group_id'] = 'required|exists:service_groups,id';
        }

        return $this->localizeRules(
            [
                'name' => 'required|string|max:255',
                'slug' => 'required|string|max:255',
                'text' => 'nullable|string|max:16777215',
                'title' => 'nullable|string|max:255',
                'h1' => 'nullable|string|max:255',
                'keywords' => 'nullable|string|max:255',
                'description' => 'nullable|string|max:255',
                'sub_title' => 'nullable|string|max:255',
                'text_for_document' => 'nullable|string',
                'text_for_relation' => 'nullable|string|max:3000',
                'seo_text' => 'nullable|string',
                'parental_case_name' => 'nullable|string',
            ],
            $rules
        );
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        $attributes = [
            'published' => __('cms-core::admin.layout.Published'),
            'icon' => __('cms-core::admin.layout.Icon'),
            'calculate_link' => __('cms-services::admin.layout.Calculate link')
        ];
        if (config('cms.services.services.use_groups')) {
            $attributes['service_group_id'] = __('cms-services::admin.Group');
        }

        return $this->localizeAttributes(
            [
                'name' => __('cms-services::admin.Name'),
                'slug' => __('cms-core::admin.layout.Slug'),
                'text' => __('cms-services::admin.Text'),
                'title' => __('cms-core::admin.seo.Title'),
                'h1' => __('cms-core::admin.seo.H1'),
                'keywords' => __('cms-core::admin.seo.Keywords'),
                'description' => __('cms-core::admin.seo.Description'),
                'sub_title' => __('cms-services::admin.Sub title'),
                'text_for_document' => __('cms-services::admin.Text for document'),
                'text_for_relation' => __('cms-services::admin.Text for relation'),
                'seo_text' => __('cms-core::admin.seo.Seo text'),
                'parental_case_name' => __('cms-services::admin.Parental case name'),
            ],
            $attributes
        );
    }
}
