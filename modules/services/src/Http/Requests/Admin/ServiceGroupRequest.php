<?php

namespace WezomCms\Services\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use WezomCms\Core\Traits\LocalizedRequestTrait;

class ServiceGroupRequest extends FormRequest
{
    use LocalizedRequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->localizeRules(
            [
                'name' => 'required|string|max:255',
                'text' => 'nullable|string',
                'text_top' => 'nullable|string',
                'slug' => 'required|string|max:255',
                'title' => 'nullable|string|max:255',
                'h1' => 'nullable|string|max:255',
                'keywords' => 'nullable|string|max:255',
                'description' => 'nullable|string|max:255',
                'short_description' => 'nullable|string|max:900',
                'sub_title' => 'nullable|string|max:255',
                'title_for_risk' => 'nullable|string|max:255',
            ],
            [
                'published' => 'required',
                'is_top' => 'required',
                'icon' => 'nullable|string',
                'parent_id' => 'nullable|int|exists:service_groups,id',
                'template' => 'nullable|string',
                'type' => 'nullable',
            ]
        );
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return $this->localizeAttributes(
            [
                'name' => __('cms-services::admin.Name'),
                'slug' => __('cms-core::admin.layout.Slug'),
                'title' => __('cms-core::admin.seo.Title'),
                'h1' => __('cms-core::admin.seo.H1'),
                'keywords' => __('cms-core::admin.seo.Keywords'),
                'description' => __('cms-core::admin.seo.Description'),
                'short_description' => __('cms-services::admin.Short description'),
                'sub_title' => __('cms-services::admin.Sub title'),
                'title_for_risk' => __('cms-services::admin.Title for risk'),
            ],
            [
                'published' => __('cms-core::admin.layout.Published'),
                'parent_id' => __('cms-services::admin.Parent'),
                'is_top' => __('cms-services::admin.Is top'),
                'icon' => __('cms-core::admin.layout.Icon'),
                'type' => __('cms-services::admin.Type'),
            ]
        );
    }
}
