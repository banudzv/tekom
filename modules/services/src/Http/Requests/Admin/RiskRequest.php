<?php

namespace WezomCms\Services\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use WezomCms\Core\Traits\LocalizedRequestTrait;

class RiskRequest extends FormRequest
{
    use LocalizedRequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->localizeRules(
            [
                'name' => 'required|string|max:255',
            ],
            [
                'published' => 'required',
                'icon' => 'nullable|string',
                'group_id' => 'nullable'
            ]
        );
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return $this->localizeAttributes(
            [
                'name' => __('cms-services::admin.risk.Name'),
            ],
            [
                'published' => __('cms-core::admin.layout.Published'),
                'icon' => __('cms-core::admin.layout.Icon'),
                'group_id' => __('cms-core::admin.layout.Icon')
            ]
        );
    }
}

