<?php

namespace WezomCms\Services\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use WezomCms\Core\ExtendPackage\Translatable;
use WezomCms\Core\Traits\Model\Filterable;
use WezomCms\Core\Traits\Model\GetForSelectTrait;
use WezomCms\Core\Traits\Model\ImageAttachable;
use WezomCms\Core\Traits\Model\PublishedTrait;
use WezomCms\Faq\Models\FaqQuestion;

/**
 * \WezomCms\Services\Models\Service
 *
 * @property int $id
 * @property bool $published
 * @property int|null $service_group_id
 * @property int $sort
 * @property string|null $image
 * @property string|null $image_banner
 * @property string|null $icon
 * @property string|null $calculate_link
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \WezomCms\Services\Models\ServiceGroup|null $group
 * @property-read \WezomCms\Services\Models\ServiceTranslation $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\WezomCms\Services\Models\ServiceTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service orWhereTranslation($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service orWhereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service orderByTranslation($translationField, $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service published()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service publishedWithSlug($slug, $slugField = 'slug')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service query()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service whereServiceGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service whereTranslation($translationField, $value, $locale = null, $method = 'whereHas', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service whereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service withTranslation()
 * @mixin \Eloquent
 * @mixin ServiceTranslation
 */
class Service extends Model
{
    use Translatable;
    use ImageAttachable;
    use GetForSelectTrait;
    use Filterable;
    use PublishedTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['published', 'service_group_id', 'icon', 'calculate_link'];

    /**
     * Names of the fields being translated in the "Translation" model.
     *
     * @var array
     */
    protected $translatedAttributes = [
        'name',
        'slug',
        'text',
        'title',
        'h1',
        'keywords',
        'description',
        'text_for_document',
        'text_for_relation',
        'seo_text',
        'sub_title',
        'parental_case_name'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['published' => 'bool'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * @return array
     */
    public function imageSettings(): array
    {
        return [
            'image' => 'cms.services.services.images',
            'image_banner' => 'cms.services.services.images_service_banner'
        ];
    }

    public function getImage()
    {
        return url($this->getImageUrl(null, 'image'));
    }

    public function getImageBanner()
    {
        return url($this->getImageUrl(null, 'image_banner'));
    }

    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getFrontUrl()
    {
        return route_localized('services.inner', $this->slug);
    }

    public function getFrontUrlByLocal($local)
    {
        return route_localized('services.inner', $this->slug, true, $local);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(ServiceGroup::class, 'service_group_id');
    }

    public function shortDescription()
    {
        return Str::words($this->text, 30);
    }

    public function getParentalCaseName()
    {
        return $this->parental_case_name ?? $this->name;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function groups()
    {
        return $this->belongsToMany(
            ServiceGroup::class,
            'services_group_services_relation',
            'service_id',
            'group_id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function relatedServices()
    {
        return $this->belongsToMany(
            Service::class,
            'services_services_relation',
            'service_id',
            'other_service_id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function risks()
    {
        return $this->belongsToMany(
            Risk::class,
            'services_risks_relation',
            'service_id', 'risk_id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function faqs()
    {
        return $this->belongsToMany(
            FaqQuestion::class,
            'services_faqs_relation',
            'service_id', 'faq_id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function additionals()
    {
        return $this->belongsToMany(
            Additional::class,
            'services_additional_relation',
            'service_id', 'additional_id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function steps()
    {
        return $this->belongsToMany(
            Step::class,
            'services_steps_relation',
            'service_id', 'step_id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function documents()
    {
        return $this->belongsToMany(
            Document::class,
            'services_documents_relation',
            'service_id', 'document_id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function objectInsurances()
    {
        return $this->belongsToMany(
            ObjectInsurance::class,
            'services_object_insurance_relation',
            'service_id', 'object_insurance_id'
        );
    }
}
