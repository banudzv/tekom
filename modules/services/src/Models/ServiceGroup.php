<?php

namespace WezomCms\Services\Models;

use Illuminate\Database\Eloquent\Model;
use WezomCms\Core\ExtendPackage\Translatable;
use WezomCms\Core\Traits\Model\Filterable;
use WezomCms\Core\Traits\Model\GetForSelectTrait;
use WezomCms\Core\Traits\Model\ImageAttachable;
use WezomCms\Core\Traits\Model\PublishedTrait;

/**
 * \WezomCms\Services\Models\ServiceGroup
 *
 * @property int $id
 * @property bool $published
 * @property string|null $template
 * @property int $sort
 * @property int|null $parent_id
 * @property bool $is_top
 * @property string|null $image
 * @property string|null $icon
 * @property int $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\WezomCms\Services\Models\Service[] $services
 * @property-read int|null $services_count
 * @property-read \WezomCms\Services\Models\ServiceGroupTranslation $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\WezomCms\Services\Models\ServiceGroupTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup orWhereTranslation($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup orWhereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup orderByTranslation($translationField, $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup published()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup publishedWithSlug($slug, $slugField = 'slug')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup whereTranslation($translationField, $value, $locale = null, $method = 'whereHas', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup whereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup withTranslation()
 * @mixin \Eloquent
 * @mixin ServiceGroupTranslation
 */
class ServiceGroup extends Model
{
    use Translatable;
    use Filterable;
    use ImageAttachable;
    use GetForSelectTrait;
    use PublishedTrait;

    const TYPE_NONE = 0;
    const TYPE_PRIVATE = 1;
    const TYPE_CORPORATE =2;

    const DEFAULT_ON_PAGE = 4;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['published', 'is_top', 'icon', 'parent_id', 'type', 'template'];

    /**
     * Names of the fields being translated in the "Translation" model.
     *
     * @var array
     */
    protected $translatedAttributes = [
        'name',
        'slug',
        'title',
        'h1',
        'keywords',
        'description',
        'text',
        'sub_title',
        'short_description',
        'title_for_risk',
        'text_top',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'published' => 'bool',
        'is_top' => 'bool'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * @return array
     */
    public function imageSettings(): array
    {
        return [
            'image' => 'cms.services.services.images_group',
            'image_banner' => 'cms.services.services.images_group_banner',
            'image_for_block' => 'cms.services.services.images_group_block',
            'image_text_top' => 'cms.services.services.images_text_top'
        ];
    }

    public static function getTypes()
    {
        return [
            self::TYPE_NONE => __('cms-services::admin.Type none'),
            self::TYPE_PRIVATE => __('cms-services::admin.Type private'),
            self::TYPE_CORPORATE => __('cms-services::admin.Type corporate'),
        ];
    }

    public function isBase()
    {
        return $this->parent_id == null;
    }

    public function isPrivatePerson()
    {
        return $this->type === self::TYPE_PRIVATE;
    }

    public function isCorporateClient()
    {
        return $this->type === self::TYPE_CORPORATE;
    }

    public function getImage()
    {
        return url($this->getImageUrl(null, 'image'));
    }

    public function getImageBanner()
    {
        return url($this->getImageUrl(null, 'image_banner'));
    }
    public function getImageTextTop()
    {
        return url($this->getImageUrl(null, 'image_text_top'));
    }

    public function getImageBlock()
    {
        return url($this->getImageUrl(null, 'image_for_block'));
    }

    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getFrontUrl()
    {
        if($this->isBase()){
            return route_localized('service-group-base.inner', ['slug' => $this->slug]);
        }

        return route_localized('service-group.inner', ['baseSlug' => $this->parent->slug, 'slug' => $this->slug]);
    }

    public function getFrontUrlByLocal($local)
    {
        if($this->isBase()){
            return route_localized('service-group-base.inner', ['slug' => $this->slug], true, $local);
        }

        return route_localized('service-group.inner', ['baseSlug' => $this->parent->slug, 'slug' => $this->slug], true, $local);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function services()
    {
        return $this->hasMany(Service::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(ServiceGroup::class, 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(ServiceGroup::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function risks()
    {
        return $this->belongsToMany(
            Risk::class,
            'service_groups_risks_relation',
            'service_group_id', 'risk_id'
        );
    }

    public function buyModule()
    {
        return $this->hasOne(BuyModule::class);
    }
}
