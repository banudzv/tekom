<?php

namespace WezomCms\Services\Repositories;

use WezomCms\Core\Repositories\AbstractRepository;
use WezomCms\Services\Models\Step;

class StepRepository extends AbstractRepository
{
    protected function model()
    {
        return Step::class;
    }
}
