<?php

namespace WezomCms\Services\Repositories;

use WezomCms\Core\Repositories\AbstractRepository;
use WezomCms\Services\Models\Additional;

class AdditionalRepository extends AbstractRepository
{
    protected function model()
    {
        return Additional::class;
    }
}
