<?php

namespace WezomCms\Services\Repositories;

use WezomCms\Core\Repositories\AbstractRepository;
use WezomCms\Services\Models\Service;

class ServiceRepository extends AbstractRepository
{
    protected function model()
    {
        return Service::class;
    }

    public function getBySelect($forChoice = true, $withoutId = false ,$text = 'Выбрать сервис')
    {
        $services = $this->getModelQuery()
            ->published()
            ->with([
                'translations' => function ($query) {
                    $query->orderBy('name');
                }
            ])
        ;

        if($withoutId){
            $services->where('id', '!=', $withoutId);
        }

        $services = $services->orderBy('sort')
            ->get()
            ->pluck('name', 'id')
            ->toArray();

        if($forChoice){
            $choice[0] = $text;
            $services = $choice + $services;
        }

        return $services;
    }

    public function search(string $search)
    {
        return $this->getModelQuery()
            ->published()
            ->whereHas('translations', function ($query) use ($search) {
                $query->where('name', 'like', '%'.$search.'%');
            })
            ->get();
    }
}
