<?php

namespace WezomCms\Services\Repositories;

use Illuminate\Database\Eloquent\Builder;
use WezomCms\Core\Repositories\AbstractRepository;
use WezomCms\Services\Models\ServiceGroup;

class ServiceGroupRepository extends AbstractRepository
{
    protected function model()
    {
        return ServiceGroup::class;
    }

    public function getByFrontAsMenu()
    {
        return $this->getModelQuery()
            ->published()
            ->with([
                'translations',
                'children' => function($query){
                    $query->published();
                }
            ])
            ->where('parent_id', null)
            ->orderBy('sort')
            ->get();
    }

    public function getBySiteMap()
    {
        return $this->getModelQuery()
            ->published()
            ->with([
                'translations',
                'children' => function($query){
                    $query->published()->with([
                        'services' => function($q){
                            $q->published()->orderBy('sort');
                        }
                    ]);
                }
            ])
            ->where('parent_id', null)
            ->orderBy('sort')
            ->get();
    }

    public function getTop($limit = 5)
    {
        return $this->getModelQuery()
            ->published()
            ->with([
                'translations',
            ])
            ->where('is_top', true)
            ->orderBy('sort')
            ->limit($limit)
            ->get();
    }

    public function getBySelect($withoutId = null)
    {
        $services = [];

        $baseServices = $this->getModelQuery()
            ->published()
            ->with([
                'translations' => function ($query) {
                    $query->orderBy('name');
                },
                'children'
            ])
            ->where('parent_id', null)
            ->orderBy('sort')
            ->get();

        foreach ($baseServices ?? [] as $item){
            foreach ($item->children ?? [] as $i){
                if($withoutId && $withoutId == $i->id){
                } else {
                    $services[$item->name][$i->id] = $i->name;
                }
            }
        }

        return $services;
    }

    public function getBySlugForFront($slug, array $relations = ['translations'])
    {
        return $this->getModelQuery()
            ->publishedWithSlug($slug)
            ->with($relations)
            ->firstOrFail();
    }

    public function getById($id, array $relations = ['translations'])
    {
        return $this->getModelQuery()
            ->publishe()
            ->with($relations)
            ->where('id', $id)
            ->first();
    }


    public function getMoreItems($parentId, $offset, array $relations = ['translations'])
    {
        return $this->getModelQuery()
            ->published()
            ->with($relations)
            ->where('parent_id', $parentId)
            ->offset($offset)
            ->limit(ServiceGroup::DEFAULT_ON_PAGE)
            ->get();
    }

    public function countChildren($parentId)
    {
        return $this->getModelQuery()
            ->published()
            ->where('parent_id', $parentId)
            ->count();
    }
}
