<?php

use WezomCms\Services\Dashboards;
use WezomCms\Services\Widgets;

return [
    'use_groups' => true,
    'count_top' => 5,
    'max_depth' => 3,
    'images' => [
        'directory' => 'services',
        'default' => 'small',
        'sizes' => [
            'small' => [
                'width' => 237,
                'height' => 237,
                'mode' => 'fit',
            ],
        ],
    ],
    'templates'=> [
        'base-index' => 'Стандартный',
        'new-index' => 'Новый'
    ],
    'images_service_banner' => [
        'directory' => 'service-banner',
        'default' => 'big',
        'sizes' => [
            'big' => [
                'width' => 1920,
                'height' => 700,
                'mode' => 'fit',
            ],
        ],
    ],
    'images_group' => [
        'directory' => 'service_group',
        'default' => 'small',
        'sizes' => [
            'small' => [
                'width' => 237,
                'height' => 237,
                'mode' => 'fit',
            ],
        ],
    ],
    'images_group_banner' => [
        'directory' => 'service_group-banner',
        'default' => 'medium',
        'sizes' => [
            'medium' => [
                'width' => 1920,
                'height' => 700,
                'mode' => 'fit',
            ],
        ],
    ],
    'images_group_block' => [
        'directory' => 'service_group-block',
        'default' => 'medium',
        'sizes' => [
            'medium' => [
                'width' => 650,
                'height' => 650,
                'mode' => 'fit',
            ],
        ],
    ],
    'widgets' => [
        'services:groups' => Widgets\Groups::class,
        'services:groups-as-menu' => Widgets\GroupsAsMenu::class,
        'services:for-main' => Widgets\GroupsForMain::class,
    ],
    'dashboards' => [
        Dashboards\ServicesDashboard::class,
    ],
    'sitemap' => [
        'services' => true, // Enable/disable render links to all published services in sitemap page.
    ],
];
