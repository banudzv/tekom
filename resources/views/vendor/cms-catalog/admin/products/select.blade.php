@php
/**
 * @var $products \Illuminate\Support\Collection|array|\WezomCms\Catalog\Models\Product[]|null
 * @var $url string|null
 * @var $name string|null
 * @var $id string|null
 */
$name = $name ?? 'product_id';
@endphp
<select name="{{ $name }}" id="{{ $id ?? $name }}" class="js-ajax-select2 form-control {{ $class ?? '' }}" @if($multiple ?? false) multiple="multiple" @endif data-template="product" data-url="{{ $url ?? route('admin.products.search') }}">
    @foreach($products ?? [] as $oneSelectOption)
        <option value="{{ $oneSelectOption->id }}"
                selected="selected" {!! Html::attributes([
                    'data-name' => $oneSelectOption->name,
                    'data-cost' => money($oneSelectOption->cost),
                    'data-currency' => money()->adminCurrencySymbol(),
                    'data-image' => $oneSelectOption->getImageUrl()]) !!}>{{ $oneSelectOption->name }}</option>
    @endforeach
</select>
