@extends('cms-users::site.layouts.cabinet')

@section('content')
    <div style="margin-top: 100px" class="container">
        <h1>{{ SEO::getH1() }}</h1>
        <div>
            <div>
                @widget('cabinet-menu')
            </div>
            <div>
               @if(WezomCms\Orders\Models\Buck::where('user_id', Auth::id())->count())
                <a style="font-size: 24px" href="{{route('checkout.step1')}}">@lang('cms-orders::site.Оформление заказа')</a>
                @endif
                {{-- <div data-cart-display="detail">@lang('cms-orders::site.Загрузка')...</div> --}}
                    @foreach($carts as $prod)
                        <div style="margin: 20px">
                            @lang('cms-orders::site.Название'): {{$prod->product->name}}
                            <br>
                            @lang('cms-orders::site.Цена'): {{$prod->product['cost']}}
                            <br>
                            @lang('cms-orders::site.Количество товаров'): {{$prod->count}}
                            <form action="{{route('cart.remove')}}" method="POST">
                                @csrf
                                <input type="hidden" name="id" value="{{$prod->id}}">
                                <button>@lang('cms-orders::site.Удалить товар')</button>
                            </form>
                            
                        </div>
                    @endforeach
            </div>
        </div>
    </div>
@endsection