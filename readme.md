<p align="center">
    <img src="https://raw.githubusercontent.com/webpack/media/master/logo/logo-on-white-bg.png">
</p>

## Changelog
- **v1.0.1** :
    - Добавлен скролл к сфокусированому элементу Input / Textarea в модальном окне
    
- **v1.0.0** :
    - Изменен алиас в папку js c ```#``` на ```@```
    - Изменен список команд в ```package.json```
    - Добавлен ```browserslist``` для таких плагинов как ```autoprefixer``` и др. в ```package.json```
    - Удаленны лишние зависимости в ```package.json```
    - Фикс багов ```webpack.export.js```
    - Добавлены оптимальные плагины компиляции svg для ```svgo``` в ```webpack/utils/BundleFile/BundleSvgSpriteFile```
    - Добавлен ````.editorconfig````
    - Изменена конфигурация:
        - ```.sass-lint.yml``` 
        - ```.modernizrrc``` 
        - ```.eslintignore```
    - Удалены ```translations/**.js```, перенесы в ```modules/core/resources/views/site/partials/translations.blade.php```
    - Изменено: стуктура, имена директорий
    - Чистка неиспользуемых функций, файлов
    - Рефакторинг

- **v0.0.2** :
    - Фик скролла на IOS в magnific popup

- **v0.0.1** :
    - Оптимизация изображений 
    - Сохранение в формате WebP 
    - Сбор svg в спрайт
    - Очистка от комментариев скомпилированных скриптов 
