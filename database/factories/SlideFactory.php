<?php

use Faker\Generator as Faker;

$factory->define(\WezomCms\Slider\Models\SlideLink::class, function (Faker $faker) {
    return [
        'published' => $faker->boolean(100),
    ];
});

$factory->define(\WezomCms\Slider\Models\SlideLinkTranslation::class, function (Faker $faker) {

    return [
        'name' => $faker->company,
        'link' => $faker->url,
    ];
});

$factory->define(WezomCms\Partners\Models\Partner::class, function (Faker $faker) {
    return [
        'published' => $faker->boolean(80),
    ];
});

$factory->define(WezomCms\Partners\Models\PartnerTranslation::class, function (Faker $faker) {
    return [
        'name' => $faker->realText(50),
        'description' => $faker->realText($faker->numberBetween(100, 250)),
    ];
});
