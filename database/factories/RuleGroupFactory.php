<?php

use Faker\Generator as Faker;

$factory->define(\WezomCms\About\Models\Rules\Group::class, function (Faker $faker) {
    return [
        'published' => true,
    ];
});

$factory->define(\WezomCms\About\Models\Rules\GroupTranslation::class, function (Faker $faker) {
    return [
        'name' => $faker->city,
    ];
});
