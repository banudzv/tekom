<?php

use Faker\Generator as Faker;

$factory->define(\WezomCms\About\Models\Details\Detail::class, function (Faker $faker) {
    return [
        'published' => true,
    ];
});

$factory->define(\WezomCms\About\Models\Details\DetailTranslation::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence,
        'text' => $faker->paragraph,
    ];
});

$factory->define(\WezomCms\About\Models\Details\DetailFile::class, function (Faker $faker) {
    return [
        'published' => true,
    ];
});

$factory->define(\WezomCms\About\Models\Details\DetailFileTranslation::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence,
    ];
});
