<?php

use Faker\Generator as Faker;

$factory->define(\WezomCms\About\Models\Rules\Rule::class, function (Faker $faker) {
    return [
        'published' => true,
    ];
});

$factory->define(\WezomCms\About\Models\Rules\RuleTranslation::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence,
    ];
});
