<?php

use Faker\Generator as Faker;

$factory->define(\WezomCms\Benefits\Models\Benefits::class, function (Faker $faker) {
    return [
        'published' => true,
        'for_main' => true,
    ];
});

$factory->define(\WezomCms\Benefits\Models\BenefitsTranslation::class, function (Faker $faker) {
    $name = $faker->streetName;
    return [
        'name' => $name,
    ];
});
