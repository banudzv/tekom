<?php

use WezomCms\Benefits\Models\Benefits;
use WezomCms\Benefits\Models\BenefitsTranslation;

class BenefitsSeeder extends BaseSeeder
{
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('benefits')->truncate();
        \DB::table('benefits_translations')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $settings = $this->settings();

        try {
            \DB::transaction(function () use($settings) {

                for($i = 1; $i != 5; $i++){

                    factory(Benefits::class)->create([
                        'sort' => $i,
                        'icon' => $this->randomIcon()
                    ]);

                    factory(BenefitsTranslation::class)->create(['locale' => 'ru', 'benefits_id' => $i]);
                    factory(BenefitsTranslation::class)->create(['locale' => 'uk', 'benefits_id' => $i]);
                }

                //settings
                $exist = \WezomCms\Core\Models\Setting::where([
                    'module' => 'benefits',
                    'group' => 'page'
                ])->exists();

                if(!$exist){
                    foreach ($settings as $item){
                        $p = new \WezomCms\Core\Models\Setting();
                        $p->module = $item->module;
                        $p->group = $item->group;
                        $p->key = $item->key;
                        $p->type = $item->type;

                        $p->save();
                        foreach ($item->translates as $lang => $value){
                            $translations = new \WezomCms\Core\Models\SettingTranslation();
                            $translations->setting_id = $p->id;
                            $translations->locale = $lang;
                            $translations->value = $value;

                            $translations->save();
                        }
                    }
                }

            });
        } catch (\Throwable $e) {
            dd($e->getMessage());
        }
    }

    protected function settings()
    {
        $page = new \stdClass();

        $name = new \stdClass();
        $name->module = 'benefits';
        $name->group = 'page';
        $name->key = 'name';
        $name->type = 'text';
        $name->translates = (object)[
            'ru' => 'Заголовк для преимуществ',
            'uk' => 'Заголовки для переваг'
        ];

        $item = new \stdClass();
        $item->module = 'benefits';
        $item->group = 'page';
        $item->key = 'text';
        $item->type = 'wysiwyg';
        $item->translates = (object)[
            'ru' => '<p>' .$this->getFaker()->realText($this->getFaker()->numberBetween(100, 250)) . '</p>',
            'uk' => '<p>' .$this->getFaker()->realText($this->getFaker()->numberBetween(100, 250)) . '</p>'
        ];

        $data[] = $name;
        $data[] = $item;

        return $page->data = $data;
    }
}
