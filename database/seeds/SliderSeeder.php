<?php

class SliderSeeder extends BaseSeeder
{
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('slides')->truncate();
        \DB::table('slide_translations')->truncate();
        \DB::table('slide_links')->truncate();
        \DB::table('slide_link_translations')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        try {
            \DB::transaction(function (){

                for($i = 1; $i != 10; $i++){

                    // slide Links
                    factory(\WezomCms\Slider\Models\SlideLink::class)->create([
                        'sort' => $i,
                        'icon' => $this->randomIcon()
                    ]);

                    factory(\WezomCms\Slider\Models\SlideLinkTranslation::class)->create(['locale' => 'ru', 'slide_link_id' => $i, 'name' => 'Link__'. $i]);
                    factory(\WezomCms\Slider\Models\SlideLinkTranslation::class)->create(['locale' => 'uk', 'slide_link_id' => $i, 'name' => 'Link__'. $i]);
                }

                foreach ($this->getData() as $item){
                    $service = \WezomCms\Services\Models\Service::orderBy(DB::raw('RAND()'))->first();

                    $b = new WezomCms\Slider\Models\Slide();
                    $b->published = $item['published'];
                    $b->sort = $item['sort'];
                    $b->service_id = $service->id;
                    $b->save();

                    // services
                    $someServices = \WezomCms\Services\Models\Service::orderBy(DB::raw('RAND()'))->take(random_int(2, 3))->pluck('id')->toArray();
                    $b->services()->sync($someServices);

                    // links
                    $someLinks = \WezomCms\Slider\Models\SlideLink::orderBy(DB::raw('RAND()'))->take(random_int(2, 3))->pluck('id')->toArray();
                    $b->links()->sync($someLinks);

                    foreach ($item['translates'] as $lang => $tran){
                        $bt = new \WezomCms\Slider\Models\SlideTranslation();
                        $bt->slide_id = $b->id;
                        $bt->locale = $lang;
                        $bt->name = $tran['name'];
                        $bt->text = $tran['text'];
                        $bt->save();
                    }
                }
            });
        } catch (\Throwable $e) {
            dd($e->getMessage());
        }
    }

    public function getData()
    {
        return [
            [
                'published' => true,
                'sort' => 1,
                'translates' => [
                    'ru' => [
                        'name' => 'Слайд__1',
                        'text' => 'Описание к слайду 1'
                    ],
                    'uk' => [
                        'name' => 'Слайд__1',
                        'text' => 'Описание к слайду 1'
                    ]

                ]
            ],[
                'published' => true,
                'sort' => 2,
                'translates' => [
                    'ru' => [
                        'name' => 'Слайд__2',
                        'text' => 'Описание к слайду 2'
                    ],
                    'uk' => [
                        'name' => 'Слайд__2',
                        'text' => 'Описание к слайду 2'
                    ]

                ]
            ],[
                'published' => true,
                'sort' => 3,
                'translates' => [
                    'ru' => [
                        'name' => 'Слайд__3',
                        'text' => 'Описание к слайду 3'
                    ],
                    'uk' => [
                        'name' => 'Слайд__3',
                        'text' => 'Описание к слайду 3'
                    ]

                ]
            ],[
                'published' => true,
                'sort' => 4,
                'translates' => [
                    'ru' => [
                        'name' => 'Слайд__4',
                        'text' => 'Описание к слайду 4'
                    ],
                    'uk' => [
                        'name' => 'Слайд__4',
                        'text' => 'Описание к слайду 4'
                    ]

                ]
            ],[
                'published' => true,
                'sort' => 5,
                'translates' => [
                    'ru' => [
                        'name' => 'Слайд__5',
                        'text' => 'Описание к слайду 5'
                    ],
                    'uk' => [
                        'name' => 'Слайд__5',
                        'text' => 'Описание к слайду 5'
                    ]

                ]
            ],
        ];
    }
}

