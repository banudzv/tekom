<?php

class AdditionalServiceSeeder extends BaseSeeder
{
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('service_additionals')->truncate();
        \DB::table('service_additional_translations')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        try {
            \DB::transaction(function () {

                for($i = 1; $i != 15; $i++){

                    factory(\WezomCms\Services\Models\Additional::class)->create([
                        'sort' => $i,
                        'icon' => $this->randomIcon()
                    ]);

                    factory(\WezomCms\Services\Models\AdditionalTranslation::class)->create(['locale' => 'ru', 'additional_id' => $i]);
                    factory(\WezomCms\Services\Models\AdditionalTranslation::class)->create(['locale' => 'uk', 'additional_id' => $i]);
                }

            });
        } catch (\Throwable $e) {
            dd($e->getMessage());
        }
    }
}
