<?php

use WezomCms\Articles\Models\Article;
use WezomCms\Articles\Models\ArticleTag;
use WezomCms\Articles\Models\ArticleTagTranslation;
use WezomCms\Articles\Models\ArticleTranslation;

class ArticlesSeeder extends BaseSeeder
{
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('article_groups')->truncate();
        \DB::table('article_group_translations')->truncate();
        \DB::table('articles')->truncate();
        \DB::table('article_translations')->truncate();
        \DB::table('article_tags')->truncate();
        \DB::table('article_tag_translations')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        try {
            \DB::transaction(function (){

                // tag
                factory(ArticleTag::class, 20)->create()->each(function(ArticleTag $articleTag){
                    factory(ArticleTagTranslation::class)->create(['locale' => 'ru', 'article_tag_id' => $articleTag->id]);
                    factory(ArticleTagTranslation::class)->create(['locale' => 'uk', 'article_tag_id' => $articleTag->id]);
                });

                foreach ($this->getData() as $item){

                    $b = new \WezomCms\Articles\Models\ArticleGroup();
                    $b->published = $item['published'];
                    $b->sort = $item['sort'];
                    $b->save();

                    factory(Article::class,random_int(5,8))->create([
                        'article_group_id' => $b->id
                    ])
                        ->each(function (Article $article) {
                            factory(ArticleTranslation::class)->create(['locale' => 'uk', 'article_id' => $article->id]);
                            factory(ArticleTranslation::class)->create(['locale' => 'ru', 'article_id' => $article->id]);
                        })
                    ;

                    foreach ($item['translates'] as $lang => $tran){
                        $bt = new \WezomCms\Articles\Models\ArticleGroupTranslation();
                        $bt->article_group_id = $b->id;
                        $bt->locale = $lang;
                        $bt->name = $tran['name'];
                        $bt->slug = $tran['slug'];
                        $bt->save();
                    }
                }

                foreach(Article::query()->get() as $item){
                    $someTags = ArticleTag::orderBy(DB::raw('RAND()'))->take(random_int(2, 4))->pluck('id')->toArray();
                    $item->tags()->sync($someTags);
                }

            });
        } catch (\Throwable $e) {
            dd($e->getMessage());
        }
    }

    public function getData()
    {
        return [
            [
                'published' => true,
                'sort' => 1,
                'translates' => [
                    'ru' => [
                        'name' => 'Статьи',
                        'slug' => 'statii'
                    ],
                    'uk' => [
                        'name' => 'Cтатті',
                        'slug' => 'statii'
                    ]

                ]
            ],[
                'published' => true,
                'sort' => 2,
                'translates' => [
                    'ru' => [
                        'name' => 'Новости',
                        'slug' => 'novosti'
                    ],
                    'uk' => [
                        'name' => 'Новини',
                        'slug' => 'novunu'
                    ]

                ]
            ],[
                'published' => true,
                'sort' => 3,
                'translates' => [
                    'ru' => [
                        'name' => 'Акции',
                        'slug' => 'akcii'
                    ],
                    'uk' => [
                        'name' => 'Акції',
                        'slug' => 'akcii'
                    ]

                ]
            ],
        ];
    }
}


