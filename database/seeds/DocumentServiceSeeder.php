<?php

class DocumentServiceSeeder extends BaseSeeder
{
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('service_documents')->truncate();
        \DB::table('service_document_translations')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        try {
            \DB::transaction(function () {

                for($i = 1; $i != 15; $i++){

                    factory(\WezomCms\Services\Models\Document::class)->create([
                        'sort' => $i,
                        'icon' => $this->randomIcon()
                    ]);

                    factory(\WezomCms\Services\Models\DocumentTranslation::class)->create(['locale' => 'ru', 'document_id' => $i]);
                    factory(\WezomCms\Services\Models\DocumentTranslation::class)->create(['locale' => 'uk', 'document_id' => $i]);
                }

            });
        } catch (\Throwable $e) {
            dd($e->getMessage());
        }
    }
}

