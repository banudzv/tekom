<?php

use WezomCms\Services\Models\Risk;
use WezomCms\Services\Models\RiskTranslation;

class RisksSeeder extends BaseSeeder
{
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('risks')->truncate();
        \DB::table('risk_translations')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        try {
            \DB::transaction(function () {

                for($i = 1; $i != 10; $i++){

                    factory(Risk::class)->create([
                        'sort' => $i,
                        'icon' => $this->randomIcon()
                    ]);

                    factory(RiskTranslation::class)->create(['locale' => 'ru', 'risk_id' => $i]);
                    factory(RiskTranslation::class)->create(['locale' => 'uk', 'risk_id' => $i]);
                }

            });
        } catch (\Throwable $e) {
            dd($e->getMessage());
        }
    }
}
