<?php

class FaqSeeder extends BaseSeeder
{
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('faq_questions')->truncate();
        \DB::table('faq_question_translations')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        try {
            \DB::transaction(function () {

                for($i = 1; $i != 15; $i++){

                    factory(\WezomCms\Faq\Models\FaqQuestion::class)->create([
                        'sort' => $i,
                    ]);

                    factory(\WezomCms\Faq\Models\FaqQuestionTranslation::class)->create(['locale' => 'ru', 'faq_question_id' => $i]);
                    factory(\WezomCms\Faq\Models\FaqQuestionTranslation::class)->create(['locale' => 'uk', 'faq_question_id' => $i]);
                }

            });
        } catch (\Throwable $e) {
            dd($e->getMessage());
        }
    }
}
