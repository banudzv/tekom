<?php

use Faker\Generator as Faker;
use WezomCms\Core\Models\Setting;
use WezomCms\Core\Models\SettingTranslation;

class SettingsSeeder extends BaseSeeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
//        \DB::table('settings')->truncate();
//        \DB::table('setting_translations')->truncate();
//        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $settings = $this->settings();

        try {
            \DB::transaction(function () use ($settings) {

                foreach ($settings as $module => $group_data){
                    foreach ($group_data as $group => $key_data){
                        foreach($key_data as $key => $type_data){
                            foreach ($type_data as $type => $values)

                            $exist = Setting::where([
                                'module' => $module,
                                'group' => $group,
                                'key' => $key,
                                'type' => $type,
                            ])->exists();

                            if(!$exist){
                                $s = new Setting();
                                $s->module = $module;
                                $s->group = $group;
                                $s->key = $key;
                                $s->type = $type;
                                $s->save();

                                foreach ($values as $value => $value_data){
                                    foreach ($value_data as $lang => $val){
                                        $t = new SettingTranslation();
                                        $t->locale = $lang;
                                        $t->value = $val;
                                        $t->setting_id = $s->id;
                                        $t->save();
                                    }
                                }
                            }
                        }
                    }
                }
            });
        } catch (\Throwable $e) {
            dd($e->getMessage());
        }
    }

    /**
     * [
     *    'module' => [
     *      'group' => [
     *        'key' => [
     *          'type' => [
     *            'value' => [
     *              'ru' => 'some value',
     *              'uk' => 'some value'
     *            ]
     *          ]
     *        ]
     *      ]
     *    ],
     * .....
     *  ];
     */
    protected function settings()
    {
        return [
            'contacts' => [
                'page-settings' => [
                    'hotline1_test' => [
                        'input' => [
                            'value' => [
                                'ru' => '099 9878888',
                                'uk' => '099 9878888'
                            ]
                        ]
                    ]
                ]
            ],
            'payment' => [
                'site' => [
                    'name' => [
                        'input' => [
                            'value' => [
                                'ru' => 'Оплата онлайн',
                                'uk' => 'Оплата онлайн'
                            ]
                        ]
                    ],
                    'h1' => [
                        'text' => [
                            'value' => [
                                'ru' => 'Оплатить онлайн',
                                'uk' => 'Оплатити онлайн'
                            ]
                        ]
                    ],
                    'title' => [
                        'text' => [
                            'value' => [
                                'ru' => 'Оплата онлайн',
                                'uk' => 'Оплата онлайн'
                            ]
                        ]
                    ],
                    'sub_title' => [
                        'textarea' => [
                            'value' => [
                                'ru' => 'Экономьте свое время и оплатите полис онлайн за несколько минут',
                                'uk' => 'Економте свій час і оплатіть поліс онлайн за кілька хвилин'
                            ]
                        ]
                    ],
                    'seo_text' => [
                        'wysiwyg' => [
                            'value' => [
                                'ru' => 'Сео текст для страницы оплаты',
                                'uk' => 'Сео текст для сторінки оплати'
                            ]
                        ]
                    ]
                ]
            ],
            'report' => [
                'site' => [
                    'name' => [
                        'input' => [
                            'value' => [
                                'ru' => 'Отчетность',
                                'uk' => 'Звітність'
                            ]
                        ]
                    ],
                    'h1' => [
                        'text' => [
                            'value' => [
                                'ru' => 'Отчетность',
                                'uk' => 'Звітність'
                            ]
                        ]
                    ],
                    'title' => [
                        'text' => [
                            'value' => [
                                'ru' => 'Отчетность',
                                'uk' => 'Звітність'
                            ]
                        ]
                    ],
                    'sub_title' => [
                        'textarea' => [
                            'value' => [
                                'ru' => 'Мы гордимся тем, что финансовая деятельность СК "Теком" являеться публичной и открытой. В этом разделе мы предоставим все клиентам и партнерам убедиться в прозрачности нашей политики. Предлагаем ознакомиться с финансовыми показателями и результатами СК "Теком"',
                                'uk' => 'Ми пишаємося тим, що фінансова діяльність СК "Теком" являється публічною і відкритою. У цьому розділі ми надамо всі клієнтам і партнерам переконатися в прозорості нашої політики. Пропонуємо ознайомитися з фінансовими показниками і результатами СК "Теком"'
                            ]
                        ]
                    ],
                ]
            ],
            'information' => [
                'site' => [
                    'title' => [
                        'text' => [
                            'value' => [
                                'ru' => 'Информация для ознакомления',
                                'uk' => 'Інформація для ознайомлення'
                            ]
                        ]
                    ]
                ],
            ],
            'agent' => [
                'site' => [
                    'title' => [
                        'text' => [
                            'value' => [
                                'ru' => 'Агенты',
                                'uk' => 'Агенти'
                            ]
                        ]
                    ]
                ],
            ],
            'about' => [
                'site' => [
                    'name' => [
                        'input' => [
                            'value' => [
                                'ru' => 'О компании',
                                'uk' => 'Про компанію'
                            ]
                        ]
                    ],
                    'h1' => [
                        'text' => [
                            'value' => [
                                'ru' => 'О компании',
                                'uk' => 'Про компанію'
                            ]
                        ]
                    ],
                    'title' => [
                        'text' => [
                            'value' => [
                                'ru' => 'О компании',
                                'uk' => 'Про компанію'
                            ]
                        ]
                    ],
                    'description' => [
                        'textarea' => [
                            'value' => [
                                'ru' => 'Какое-то описание',
                                'uk' => 'Какое-то описание'
                            ]
                        ]
                    ],
                ],
                'about_us' => [
                    'name' => [
                        'input' => [
                            'value' => [
                                'ru' => 'О нас',
                                'uk' => 'Про нас'
                            ]
                        ]
                    ],
                    'h1' => [
                        'text' => [
                            'value' => [
                                'ru' => 'О нас',
                                'uk' => 'Про нас'
                            ]
                        ]
                    ],
                    'title' => [
                        'text' => [
                            'value' => [
                                'ru' => 'О нас',
                                'uk' => 'Про нас'
                            ]
                        ]
                    ],
                    'text' => [
                        'wysiwyg' => [
                            'value' => [
                                'ru' => '<p>Какое-то описание</p>',
                                'uk' => '<p>Какое-то описание</p>'
                            ]
                        ]
                    ],
                ],
                'about_details' => [
                    'title' => [
                        'text' => [
                            'value' => [
                                'ru' => 'Детально о ЧAO CK TEKOM',
                                'uk' => 'Детально про ЧAO CK TEKOM'
                            ]
                        ]
                    ],
                ],
                'about_rule' => [
                    'title' => [
                        'text' => [
                            'value' => [
                                'ru' => 'Правила страхования',
                                'uk' => 'Правила страхування'
                            ]
                        ]
                    ],
                ],
                'about_contracts' => [
                    'title' => [
                        'text' => [
                            'value' => [
                                'ru' => 'Договора публичной оферты',
                                'uk' => 'Договори публічної оферти'
                            ]
                        ]
                    ],
                ],
                'about_contracts_ownership' => [
                    'title' => [
                        'text' => [
                            'value' => [
                                'ru' => 'Структура собственности',
                                'uk' => 'Структура власності'
                            ]
                        ]
                    ],
                ],
                'about_license' => [
                    'title' => [
                        'text' => [
                            'value' => [
                                'ru' => 'Лицензии',
                                'uk' => 'Ліцензії'
                            ]
                        ]
                    ],
                ],
            ]
        ];
    }
}


